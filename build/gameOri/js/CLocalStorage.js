/**
 * @Author: memo
 * @Date:   2018-06-20T16:02:36-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-12T11:15:36-06:00
 */



var LOCALSTORAGE_SCORE = "score";

function CLocalStorage(szName){

    var _szName = szName;

    this.init = function(){
        console.log("CLocalStorage->init")
        console.log("window.localStorage:", window.localStorage)
        console.log("storage con nombre:" + szName)
        console.log("window.localStorage:", window.localStorage)
        var bFlag = window.localStorage.getItem(_szName);

        if(bFlag === null || bFlag === undefined || s_bStorageAvailable === false){
            this.resetAllData();
        }

    };

    this.setItem = function(szKey, szValue){
      console.log("CLocalStorage->setItem:", szKey, szValue)
        if(s_bStorageAvailable){
            _bDirty = true;
            window.localStorage.setItem(_szName+"_"+szKey, szValue);
        }
    };

    this.getItem = function(szKey){
      console.log("CLocalStorage->getItem:", szKey)
        return window.localStorage.getItem(_szName+"_"+szKey);
    };

    this.setItemJson = function(szKey, jsonObj){
        console.log("CLocalStorage->setItemJson ", szKey, jsonObj)

        // let url =
        //   window.location != window.parent.location
        //     ? document.referrer
        //     : document.location.href;
        //     console.log("parent url:" +url)
        //
        //     const params = new URLSearchParams(window.location.search)
        //     console.log("params:", params)
        //     var userId = "TBD"
        //     if(params.has('userId')){
        //       userId = params.get('userId')
        //     }
        //     console.log("userId:", userId)
        //     console.log("jsonObj:", jsonObj)
            // window.parent.postMessage({
            //             error  : false,
            //             action : "SendScore",
            //             jsonObj
            //           });



        if(s_bStorageAvailable){
            localStorage.setItem(_szName+"_"+szKey, JSON.stringify(jsonObj));
        }
    };

      this.getItemJson = function(szKey){
          var oRet = JSON.parse(localStorage.getItem(_szName+"_"+szKey));

          if(oRet === null){
              var aLevelScore = new Array();
              for(var i=0;i<NUM_HOLES;i++){
                  aLevelScore[i] = 0;
              }
              return aLevelScore;
          }else{
              return oRet;
          }
      };


    this.isDirty = function(){
        console.log("CLocalStorage->isDirty")
        var aLevelScore = s_oLocalStorage.getItemJson(LOCALSTORAGE_SCORE);
        console.log("aLevelScore:", aLevelScore)
        if(!s_bStorageAvailable || aLevelScore === null){
            return false;
        }

        for (var i = 0; i <aLevelScore.length; i++) {
            if(aLevelScore[i] > 0){
                return true;
            }
        }
        return false;
    };

    this.resetAllData = function(){
        console.log("CLocalStorage->resetAllData:" + _szName)
        window.localStorage.setItem(_szName, true);

        var aScore = new Array();
        for(var i=0; i<NUM_HOLES; i++){
            aScore[i] = 0;
        }
        this.setItemJson(LOCALSTORAGE_SCORE,aScore);
    };

}
