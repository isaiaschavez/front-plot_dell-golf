/**
 * @Author: memo
 * @Date:   2020-10-04T19:44:54-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-06T21:47:38-06:00
 */
var TEXT_PRELOADER_CONTINUE = "COMPETIR";
var TEXT_GAMEOVER = "FIN DE JUEGO";
var TEXT_ARE_SURE = "ESTAS SEGURO DE SALIR?";
var TEXT_SAVE_REMOVE = "ESTO ELIMINARÁ TU AVANCE! ESTAS SEGURO?"
var TEXT_TOTAL_SCORE = "PUNTOS DEL JUEGO";
var TEXT_LAUNCH = "LAUNCH";
var TEXT_HOLE = "HOLE";
var TEXT_LOADING = "Cargando...";
var TEXT_ALL_COMPLETE = "TODOS LOS NIVELES COMPLETADOS";
var TEXT_SELECT_A_LEVEL = "SELECCIONA UN HOYO";
var TEXT_HELP1_PC = "HAZ CLIC EN LA BOLA Y ARRASTRA PARA ACTIVAR EL PODER Y LA DIRECCION";
var TEXT_HELP1_MOBILE = "TOCA LA BOLA Y ARRASTRA EL DEDO PARA ACTIVAR EL PODER Y LA DIRECCION";
var TEXT_HELP2 = "HAZ CLIC Y ARRASTRA PARA VISUALIZAR";
var TEXT_ERR_LS = "TU NAVEGADOR NO SOPORTADO ESTE JUEGO.";

var TEXT_SCORE = "MARCADOR";
var TEXT_COMPLETE = "COMPLETO";

var TEXT_DEVELOPED = "";

var TEXT_DOUBLE_BOGEY = "DOUBLE BOGEY";
var TEXT_BOGEY = "BOGEY";
var TEXT_PAR = "PAR";
var TEXT_BIRDIE = "BIRDIE";
var TEXT_EAGLE = "EAGLE";
var TEXT_ALBATROSS = "ALBATROSS";
var TEXT_CONDOR = "CONDOR";
var TEXT_PHOENIX = "PHOENIX";
var TEXT_HOLEINONE = "HOYO EN UNO";


var TEXT_SHARE_IMAGE = "200x200.jpg";
var TEXT_SHARE_TITLE = "Felicidades!";
var TEXT_SHARE_MSG1 = "Tu ganaste <strong>";
var TEXT_SHARE_MSG2 = " puntos </strong>!<br><br>Comparte!";
var TEXT_SHARE_SHARE1 = "Mi marcador del juego es  ";
var TEXT_SHARE_SHARE2 = " puntos! puedes ganarme?";


var HOYO_EN_UNO = 1000
var PHOENIX = 900
var CONDOR = 700
var ALBATROS = 600
var EAGLE = 500
var BIRDIE = 200
var PAR_VALUE = 100
var BOGEY = 50
