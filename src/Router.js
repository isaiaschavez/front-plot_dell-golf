/**
 * @Author: memo
 * @Date:   2020-10-04T13:03:30-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-03T18:32:33-06:00
 */



import React, { useState, useRef } from 'react';
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import store from "./store";
import Layout from "./components/dashboard/Layout";
import Login from "./components/Auth/Login";
import Register from "./components/Auth/Register";
import Process from "./components/Auth/Process";

// import Login from "./components/Auth/OneForm";

// import Login from "./components/Auth/WrapperLogin";

import PrivateRoute from "./components/private-route/PrivateRoute";
import NotFound from "./components/404/404";
import setAuthToken from "./components/Auth/setAuthToken";
import jwt_decode from "jwt-decode";
import axios from "axios";
import { setCurrentUser, logoutUser } from "./actions/authActions";

import './App.css';

if (localStorage.jwtTokenApp) {
  console.log("setting")
  // localStorage.removeItem("jwtTokenApp");
  // setAuthToken(false);


  // Set auth token header auth
  const token = JSON.parse(localStorage.jwtTokenApp);
  setAuthToken(token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(token);

  axios.defaults.headers.common = {
    'Authorization': 'Bearer ' + token
  };

  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());

    // Redirect to login
    window.location.href = "./";
  }
}

function RouterApp() {
  return (<Provider store={store}>
       <Router>
            <Switch>
              {
                //<Route exact path="/" component={Login} />

              }
                <Route exact path="/register" component={Register} />
                <Route exact path="/" component={Login} />
                <Route exact path="/process/:code" component={Process}/>
                <PrivateRoute exact path="/dashboard" component={Layout} />
                <Route
                  component={localStorage.jwtTokenApp ? Layout : NotFound}
                />
            </Switch>
        </Router>
        </Provider>
  );
}

export default RouterApp;
