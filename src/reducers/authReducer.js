/**
 * @Author: memo
 * @Date:   2020-10-04T16:04:46-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-03T20:04:47-06:00
 */



import {
  GET_USERS,
  GET_ERRORS,
  SET_CURRENT_USER,
  USER_REGISTER,
  USER_RESET_PASSWD,
  USER_CONFIRM,
  USER_ACTIVATION,
  USER_SCHEDULER,
  USER_AVATAR,
  GET_SCORE,
  SET_AVATAR_USER,
  GET_SCORE_TOP,
  SET_GIFT_MAIL,SET_GIFT_MAIL_INTERNAL,
  GET_DATES_USER,GET_GIFTS_USER

 } from "../actions/types";

const isEmpty = require("is-empty");

const initialState = {
  isAuthenticated: false,
  user: {},
  loading: false,
  activatePopUp:false,
  avatar:{}
};

 export default function(state = initialState, action) {
   console.log("AuthReducer->action:", action)
   switch (action.type) {
     case SET_CURRENT_USER:
     console.log("action.payload:", action.payload)
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };

     case GET_USERS:
     return {
       ...state,
       users: action.payload
     };

     case GET_SCORE:
        return {
            ...state,
            score:action.payload
      };
     case GET_SCORE_TOP:
        return {
            ...state,
            scoretop:action.payload
      };
     case USER_REGISTER:
       return{
         ...state,
         user: action.payload
       };
     case SET_GIFT_MAIL:
       return{
         ...state,
         giftsended: action.payload
       };
     case SET_GIFT_MAIL_INTERNAL:
       return{
         ...state,
         giftsendedinternal: action.payload
       };
     case GET_DATES_USER:
       return{
         ...state,
         datesuser: action.payload
       };
     case GET_GIFTS_USER:
       return{
         ...state,
         hasagift: action.payload
       };
      case USER_RESET_PASSWD :
         return{
           ...state,
           user: action.payload,
           activatePopUp:true
         };
         case USER_CONFIRM:
         return{
           ...state,
           user: action.payload,
           activatePopUp:true
         };

          case USER_ACTIVATION:{
              return {
                ...state,
                user:action.payload
              }
          };

          case USER_SCHEDULER:{
            return {
              ...state,
              user:action.payload
            }
          }

          case USER_AVATAR:{
            return{
              ...state,
              avatar: action.payload
            }
          }

          case SET_AVATAR_USER:{
            return{
              ...state,
              avatarUser: action.payload
            }

          }

      case GET_ERRORS:
      return {
         ...state,
         error:action.payload?action.payload.message:""
      };



      default:
        return state;
   }
}
