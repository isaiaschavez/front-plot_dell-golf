import { combineReducers } from "redux";
import authReducer from "./authReducer";
//import utilitiesReducer from "./utilitiesReducer"


export default combineReducers({
  //utils  : utilitiesReducer,
  auth   : authReducer,
});
