/**
 * @Author: memo
 * @Date:   2020-10-01T18:36:40-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-03T19:40:52-06:00
 */
export const GET_ERRORS        = "GET_ERRORS";
export const USER_LOADING      = "USER_LOADING";
export const SET_CURRENT_USER  = "SET_CURRENT_USER";
export const USER_REGISTER     = "USER_REGISTER";
export const GET_USERS         = "GET_USERS";
export const USER_RESET_PASSWD = "USER_RESET_PASSWD";
export const USER_CONFIRM      = "USER_CONFIRM";
export const USER_ACTIVATION   = "USER_ACTIVATION";
export const USER_SCHEDULER    = "USER_SCHEDULER";
export const USER_AVATAR       = "USER_AVATAR";
export const UPDATE_USER       = "UPDATE_USER";
export const SET_AVATAR_USER   = "SET_AVATAR_USER";
export const GET_SCORE         = "GET_SCORE";
export const GET_SCORE_TOP     = "GET_SCORE_TOP";
export const SET_GIFT_MAIL = "SET_GIFT_MAIL";
export const SET_GIFT_MAIL_INTERNAL = "SET_GIFT_MAIL_INTERNAL";
export const GET_DATES_USER = "GET_DATES_USER";
export const GET_GIFTS_USER = "GET_GIFT_USER";

