/**
 * @Author: memo
 * @Date:   2020-10-04T18:49:16-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-03T20:06:35-06:00
 */
import axios from "axios";
import setAuthToken from "../components/Auth/setAuthToken";
import jwt_decode from "jwt-decode";

import { GET_ERRORS, SET_CURRENT_USER, USER_LOADING,
      USER_REGISTER, USER_RESET_PASSWD, USER_CONFIRM,
      USER_ACTIVATION, USER_SCHEDULER, USER_AVATAR,
      GET_SCORE, UPDATE_USER, SET_AVATAR_USER,GET_SCORE_TOP,
      SET_GIFT_MAIL,SET_GIFT_MAIL_INTERNAL,GET_DATES_USER,GET_GIFTS_USER
    } from "./types";
import config from "./../config/data"

const URL_LOGIN = config.protocol+config.host+config.port+config.login
const URL_REGISTER = config.protocol+config.host+config.port+config.register
const URL_RESET = config.protocol+config.host+config.port+config.resetPassword
const URL_CONFIRM = config.protocol+config.host+config.port+config.confirmUser
const URL_ACTIVATION = config.protocol+config.host+config.port+config.activationUser
const URL_SCHEDULER = config.protocol+config.host+config.port+config.sendScheduler
const URL_AVATAR = config.protocol+config.host+config.port+config.getAvatar
const URL_SCORE_TOTAL = config.protocol+config.host+config.port+config.getScoreTotal
const URL_USER = config.protocol+config.host+config.port+config.updateUser
//ISAIAS
const URL_SCORE_TOP = config.protocol+config.host+config.port+config.getTop
const URL_SEND_GIFT = config.protocol+config.host+config.port+config.sendEmailGift
const URL_SEND_GIFT_INTERNAL = config.protocol+config.host+config.port+config.sendEmailGiftInternal
const URL_GET_DATES = config.protocol+config.host+config.port+config.getDatesUser
const URL_GET_GIFTS = config.protocol+config.host+config.port+config.getGiftsUser
//ISAIAS

export const getUser = (userId) => dispatch => {
  var URL = URL_USER + "/" + userId
  console.log("URL_USER:" + URL)
   axios
    .get(URL)
    .then(res => {
      console.log("SET_CURRENT_USER res:", res)
           dispatch({
             type : SET_AVATAR_USER,
             payload:res.data.result.user
             })
    })
    .catch(err=>{
      console.log("USER_AVATAR err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
};


export const updateUser = (userId, data) => dispatch => {
  console.log("userData:", userId)
  var URL = URL_USER + "/" + userId
  console.log("URL_USER:" + URL)
   axios
    .patch(URL, data)
    .then(res => {
      console.log("SET_CURRENT_USER res:", res)
           dispatch({
             type : SET_AVATAR_USER,
             payload:res.data.result.user
             })
    })
    .catch(err=>{
      console.log("USER_AVATAR err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
};

export const getScoreTotal = (userId) => dispatch => {
  console.log("userData:", userId)
  var URL = URL_SCORE_TOTAL + "/" + userId
  console.log("URL_SCORE:" + URL)
   axios
    .get(URL)
    .then(res => {
      console.log("GET_SCORE res:", res)
           dispatch({
             type : GET_SCORE,
             payload:res.data.result
             })
    })
    .catch(err=>{
      console.log("USER_AVATAR err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
};
export const getScoreTop = () => dispatch => {
  var URL = URL_SCORE_TOP 
   axios
    .get(URL)
    .then(res => {
      console.log("GET_SCORE_TOP res:", res)
           dispatch({
             type : GET_SCORE_TOP,
             payload:res.data.result
             })
    })
    .catch(err=>{
      console.log("GET_SCORE_TOP err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
};
//----------------
export const getSendMailGift = (userData) => dispatch => {
  var URL = URL_SEND_GIFT 
  axios
    .post(URL,userData)
    .then(res => {
      console.log("SET_GIFT res:", res)
      dispatch({
        type : SET_GIFT_MAIL,
             payload:res.data.result
            })
          })
    .catch(err=>{
      console.log("GET_SCORE_TOP err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
  };
export const postSendMailGiftInternal = (userData) => dispatch => {
  var URL = URL_SEND_GIFT_INTERNAL 
  axios
    .post(URL,userData)
    .then(res => {
      console.log("SET_GIFT_INTERNAL res:", res)
      dispatch({
        type : SET_GIFT_MAIL_INTERNAL,
             payload:res.data.result
            })
          })
    .catch(err=>{
      console.log("GET_SCORE_TOP err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
  };
export const getDatesOfUser = (userData) => dispatch => {
  var URL = URL_GET_DATES 
  axios
    .post(URL,userData)
    .then(res => {
      console.log("URL_GET_DATES res:", res)
      dispatch({
        type : GET_DATES_USER,
             payload:res.data.result
            })
          })
    .catch(err=>{
      console.log("GET_DATES_USER err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
  };
export const getHasAGift = (userData) => dispatch => {
  var URL = URL_GET_GIFTS 
  console.log("GET GIFTSD ")
  axios
    .post(URL,userData)
    .then(res => {
      console.log("GET_GIFTS_USER res:", res)
      dispatch({
        type : GET_GIFTS_USER,
             payload:res.data.result.hasSendedHisGift
            })
          })
    .catch(err=>{
      console.log("GET_GIFTS_USER err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
  };
  
  //----------------


export const saveAvatar = (userData) => dispatch => {
  var URL = URL_AVATAR
  console.log("URL:" + URL)
   axios
    .post(URL_AVATAR, userData)
    .then(res => {
      console.log("USER_AVATAR res:", res)
           dispatch({
             type : USER_AVATAR,
             payload:res.data.result.avatar
             })
    })
    .catch(err=>{
      console.log("USER_AVATAR err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
};

export const getAvatar = (userId) => dispatch => {
  console.log("userData:", userId)
  var URL = URL_AVATAR + userId
  console.log("URL:" + URL)
   axios
    .get(URL)
    .then(res => {
      console.log("USER_AVATAR res:", res)
           dispatch({
             type : USER_AVATAR,
             payload:res.data.result.avatar
             })
    })
    .catch(err=>{
      console.log("USER_AVATAR err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
};


export const sendScheduler = (userData) => dispatch => {
  console.log("userData:", userData)
   axios
    .post(URL_SCHEDULER, userData)
    .then(res => {
      console.log("res:", res)
           dispatch({
             type : USER_SCHEDULER,
             payload:res.data
             })
    })
    .catch(err=>{
      console.log("err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
};

export const activationUser = (userData) => dispatch => {
  console.log("userData:", userData)
   axios
    .post(URL_ACTIVATION, userData)
    .then(res => {
      console.log("res:", res)
      // if (res&& res.data  &&
      //    res.data.status && res.data.status.code == config.SUCCESS) {
           dispatch({
             type : USER_ACTIVATION,
             payload:res.data
             })
      // }else {
      //   dispatch({
      //     type: GET_ERRORS,
      //     payload: res.data
      //   })
      // }
    })
    .catch(err=>{
      console.log("err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
};


export const confirmUser = (userData) => dispatch => {

  console.log("URL_CONFIRM:", URL_CONFIRM)
  console.log("userData:", userData)
   axios
    .post(URL_CONFIRM, userData)
    .then(res => {
      if (res&& res.data  &&
         res.data.status && res.data.status.code == config.SUCCESS) {
           dispatch({
             type : USER_CONFIRM,
             payload:res.data
             })
      }else {
        dispatch({
          type: GET_ERRORS,
          payload: res.data
        })
      }
    })
    .catch(err=>{
      console.log("err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
};

export const resetUser = (userData) => dispatch => {
   axios
    .post(URL_RESET, userData)
    .then(res => {
      if (res&& res.data  &&
         res.data.status && res.data.status.code == config.SUCCESS) {
           dispatch({
             type : USER_RESET_PASSWD,
             payload:res.data
             })
      }else {
        dispatch({
          type: GET_ERRORS,
          payload: res.data
        })
      }
    })
    .catch(err=>{
      console.log("err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    })
};

export const registerUser = (userData, history) => dispatch => {
  axios
    .post(URL_REGISTER  , userData)
    .then(res => {
      console.log("res:" + JSON.stringify(res))
      if (res&& res.data  &&
         res.data.status && res.data.status.code==config.SUCCESS) {
           loginUser(userData)
           // dispatch({
           //   type : USER_REGISTER,
           //   payload:res.data
           //   })
      }else {
        dispatch({
          type: GET_ERRORS,
          payload: res.data
        })
      }
      dispatch({
        type : USER_REGISTER,
        payload:res.data
        })
    })
    .catch(err =>{
      console.log("err:" + JSON.stringify(err))
      dispatch({
        type: GET_ERRORS,
        payload: err.data
      })
    }

    );
};

// Login - get user token
export const loginUser = userData => dispatch => {

  
  axios
    .post(URL_LOGIN, userData)
    .then(res => {
      // Save to localStorage
      // Set token to localStorage
      console.log("res login:", res)
      const { token, user } = res.data.result;
      console.log("res.data.result:", res.data.result)

      localStorage.setItem("jwtTokenApp", JSON.stringify(token));
        // Set token to Auth header
      setAuthToken(token);
        // Decode token to get user data
      const decoded = jwt_decode(token);
        // Set current user
      dispatch(setCurrentUser(decoded));

    })
    .catch(err =>{
      console.log("err:", JSON.stringify(err))
      var  message = "Credenciales inválidas."

      if(err.message.toUpperCase().includes("NETW")){
        message = "Problema en la red"
      }
      dispatch({
        type: GET_ERRORS,
        // payload: err.data.response.result
        payload: {
          message
        }
      })
     }
  );
};

// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

// User loading
export const setUserLoading = () => {
  return {
    type: USER_LOADING
  };
};

// Log user out
export const logoutUser = history => dispatch => {
  // Remove token from local storage
  localStorage.removeItem("jwtTokenApp");
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to empty object {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
  if(history){
    history.push("/dashboard");
  }
};
