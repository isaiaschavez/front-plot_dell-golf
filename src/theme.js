export const theme = {
  primaryDark: '#212121',
  primaryLight: '#EFFFFA',
  primaryHover: '#343078',
  mobile: '576px',
}
