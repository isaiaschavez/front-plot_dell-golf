/**
 * @Author: memo
 * @Date:   2020-10-08T00:50:22-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-24T00:42:12-06:00
 */
module.exports = {
  // protocol             : "http://",
  // host                 : "192.168.1.76:",
  // port                 : "5556/",


  //protocol             : "http://",
  //host                 : "api.golfdell.inmersys.xyz",
  //port                 : "/",

  protocol             : "https://",
  host                 : "dellapi.herokuapp.com",
  port                 : "/",

  //  protocol             : "http://",
  //  host                 : "localhost:5556",
  //  port                 : "/",

  SUCCESS              : "0000",
  login                : "api/users/login",
  register             : "api/users/register",
  resetPassword        : "api/users/resetPassword",
  confirmUser          : "api/users/confirmUser",
  activationUser       : "api/users/activationUser",
  sendScheduler        : "api/users/sendScheduler",
  getAvatar            : "api/users/avatar/",
  getScoreTotal        : "api/utilities/scoreTotal",
  setSuvey             : "api/utilities/survey",
  userFinishGame       : "api/utilities/userFinishGame",
  setScore             : "api/utilities/score/",
  getScore             : "api/utilities/score/",
  setScoreQuiz         : "api/utilities/scoreQuiz/",
  getTop               : "api/utilities/top/",
  updateUser           : "api/users",
  getUser              : "api/users",
  sendEmailGift        : "api/users/sendGiftNotification",
  sendEmailGiftInternal: "api/users/sendGiftNotificationInternal",
  getDatesUser         : "api/users/getDates",
  getGiftsUser         : "api/users/hasGift",
}
