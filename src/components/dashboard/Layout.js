/**
 * @Author: memo
 * @Date:   2020-10-06T11:54:12-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-01T15:56:33-06:00
 */
import React, { Component,  useState, useRef } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Dashboard from "../MainContent/Dashboard/Dashboard";
import Contact from "../MainContent/Contact/Contact";
import Ranking from "../MainContent/Ranking/Ranking";
import Scheduler from "../MainContent/Scheduler/Scheduler";
import Avatar from "../MainContent/Avatar/Avatar";
import Play from "../MainContent/Play/PlayHook";
import Practice from "../MainContent/Practice/Practice";
import Award from "../MainContent/Award/Award"
import Videos from "../MainContent/Videos/Videos"
import VideosCyber from "../MainContent/Videos/Videos0"
import VideosPowerstore from "../MainContent/Videos/Videos00"

import TopNav from "../TopNav/TopNav";
import NotFound from "../404/404";
import './Dashboard.scss'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter
} from "react-router-dom";
import { getAvatar } from "../../actions/authActions";

import { Burger, Menu } from '../../components';
import FocusLock from 'react-focus-lock';
import { GlobalStyles } from './../../global';
import { ThemeProvider } from 'styled-components';
import { theme } from './../../theme';
import Navbar from './../Nav/Navbar';
import {isMobile} from 'react-device-detect';

const menuId = "main-menu";

class Layout extends Component {
  constructor() {
    super();
    this.state = {
      open:false
    };
  }

  // UNSAFE_componentWillMount(){
  //   this.props.getAvatar(this.props.auth.user.id)
  // }

  componentDidMount() {

   }

  setOpen(open){
      this.setState({
        open
      })
  }

  exit(){
    console.log("exit")
  }
  render() {

    let dashboardContent = (
   <>
   {
       <div>
         <Navbar
           user={this.props.auth.user}
           avatar={this.props.auth}
           />
       </div>
   }
      <div className="mainWrapperMobileLayout">
        <Switch>
          <Route
            exact
            path="/dashboard"
            component={Dashboard}
          />
          {
            <Route exact path="/Contact" component={Contact} />
          }
          {
            <Route exact path="/Play" component={Play} />
          }
          {
            <Route exact path="/Ranking" component={Ranking} />
          }
          {
            <Route exact path="/Scheduler" component={Scheduler} />
          }
          {
            <Route exact path="/Avatar" component={Avatar} />
          }
          {
            <Route exact path="/Practice" component={Practice}/>
          }
          {
            <Route exact path="/Award" component={Award}/>
          }
          {
            <Route exact path="/VideosWork" component={Videos}/>
          }
          {
            <Route exact path="/VideosCyber" component={VideosCyber}/>
          }
          {
            <Route exact path="/VideosPowerstore" component={VideosPowerstore}/>
          }
          <Route component={NotFound} />
        </Switch>
      </div>
    </>
  );
    return (
      <Router>
        {dashboardContent}
      </Router>
    );
  }
}

Layout.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  // projects: state.projects,
  // malls: state.malls
});

export default withRouter(
  connect(

    mapStateToProps
    ,
    {
      getAvatar
      // getMalls
     }
  )(Layout)
);
