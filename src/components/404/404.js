import React from "react";
import { Link } from "react-router-dom";

import "./404.scss";

const NotFound = props => {
  return (
    <div className="not-found">
      <Link to="/dashboard">
        <b>404</b>
      </Link>
      <br />
      <p>La página que busca no se encuentra en este servidor.</p>
      <p>
           O bien, la URL que escribió es incorrecta,
           no tiene acceso privilegios a la página,
           o la página que está buscando ha sido eliminada.
      </p>
    </div>
  );
};

export default NotFound;
