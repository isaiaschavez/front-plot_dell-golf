import React, { Component, PureComponent , useState  } from "react";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import "./../styles/base.scss";
import "./OneForm.scss";
import { loginUser, registerUser } from "../../actions/authActions";
import {isMobile} from 'react-device-detect';
const isEmpty = require("is-empty");

class OneForm extends Component  {

  constructor() {
    super();
    this.state = {
      errorEmailLogin : "",
      errorPasswordLogin : "",

      errorNameRegister:null,
      errorPasswordRegister:null,
      errorConfirmPasswordRegister:null,
      errorEmailRegister:null,

      errorMessage:null,
      errorEmailLogin:null,
      checked:false,

      errorLogin:"asaas"
    };
    //this.nameInput = React.createRef();
    // this.errorNameRegisterInput =  React.createRef();
  }

  componentDidMount() {
    console.log("componentDidMount -> this.props.auth:", this.props.auth)
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
    // else if (this.props.auth.isAuthenticated==false) {
    //   this.state.errorLogin = this.props.auth.error
    // }
  }

  componentDidUpdate(prevProps) {
    console.log("componentDidUpdate . this.props:"+ JSON.stringify(this.props))
    if(this.props.auth &&
      (this.props.auth.error != prevProps.auth.error)
    ){
      this.setState({
        errorMessage:this.props.auth.error
      })
    }
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  }


  register(e){
    var nameInput = document.getElementById('nameInput')
    var passInput = document.getElementById('passInput')
    var confirmPassInput = document.getElementById('confirmPassInput')
    var emailInputRegister = document.getElementById('emailInputRegister')

    // console.log("nameInput.value:" + nameInput.value)
    // console.log("passInput.value:" + passInput.value)
    // console.log("confirmPassInput.value:" + confirmPassInput.value)
    // console.log("emailInputRegister.value:" + emailInputRegister.value)

    if(isEmpty(emailInputRegister.value)){
      this.setState({
        errorEmailRegister:"El email es requerido."
      })
    }else{
      if(emailInputRegister.validity.typeMismatch){
        this.setState({
          errorEmailRegister:"Ingrese un email válido."
        })
      }else {
        this.setState({
          errorEmailRegister:null
        })
      }
    }
    if(isEmpty(nameInput.value)){
       // nameInput.setCustomValidity("El nombre es requerido.");
      this.setState({
        errorNameRegister:"El nombre es requerido."
      })
    }else{
      if(nameInput.value.length<5){
        this.setState({
          errorNameRegister:"Ingrese por lo menos 5 caracteres."
        })
      }else{
        this.setState({
          errorNameRegister:null
        })
      }
    }

    if(isEmpty(passInput.value)){
      this.setState({
        errorPasswordRegister:"El password es requerido."
      })
    }else{
      if(passInput.value.length<8){
        this.setState({
          errorPasswordRegister:"Ingrese por lo menos 8 caracteres."
        })
      }else {
        this.setState({
          errorPasswordRegister:null
        })
      }
    }

    if(isEmpty(confirmPassInput.value)){
      this.setState({
        errorConfirmPasswordRegister:"El password es requerido."
      })
    }else{
      if(confirmPassInput.value.length<8){
        this.setState({
          errorConfirmPasswordRegister:"Ingrese por lo menos 8 caracteres."
        })
      }else {
        if (confirmPassInput.value != passInput.value) {
          this.setState({
            errorConfirmPasswordRegister:"Los passwords no coinciden."
          })
        }else {
          this.setState({
            errorConfirmPasswordRegister:null
          })
        }
      }

      if(
        this.state.errorNameRegister == null &&
        this.state.errorPasswordRegister == null &&
        this.state.errorConfirmPasswordRegister == null &&
        this.state.errorEmailRegister == null
      ){
        this.props.registerUser({
            email:emailInputRegister.value,
            password:confirmPassInput.value,
            name:nameInput.value
        })
      }
    }
  }

  onChangePassword(e){
      this.setState({
        password:e.target.value
      },()=>{
        console.log("password:"+this.state.password)
      })
  }

  onChangeConfirmPassword(e){
      this.setState({
        confirmPassword:e.target.value
      },()=>{
        console.log("confirmPassword:"+this.state.confirmPassword)
      })
  }

  login(){
      var emailLoginInput = document.getElementById('emailLoginInput')
      var passLoginInputs = document.getElementById('passLoginInputs')
      if(isEmpty(emailLoginInput.value)){
         // nameInput.setCustomValidity("El nombre es requerido.");
        this.setState({
          errorEmailLogin:"El email es requerido."
        })
      }else{
          this.setState({
            errorEmailLogin:null
          })
      }


      if(isEmpty(passLoginInputs.value)){
        this.setState({
          errorPasswordLogin:"El password es requerido."
        })
      }else{
          this.setState({
            errorPasswordLogin:null
          })
      }

      if(
        this.state.errorEmailLogin == null &&
        this.state.errorPasswordLogin == null
      ) {
        var data = {
          email:emailLoginInput.value,
          password:passLoginInputs.value
        }
        console.log("data:" + data)
        this.props.loginUser(data)
      }


  }

  onChangeNameLogin(e){
    this.setState({
      emailLogin:e.target.value
    },()=>{
      console.log("emailLogin:", this.state.emailLogin)
    })
  }


handleCheck(e){
   const { name, value } = e.target;
   this.setState({
     checked:value
   })
 }

  render(){
    return(
      <div class="body-login">
      <div class="login-wrap">
      	<div class="login-html">
      		<input id="tab-1" type="radio" name="tab" class="sign-in" onClick={this.handleCheck.bind(this)} />
            <label for="tab-1" class="tab">Entrar</label>
      		<input id="tab-2" type="radio" name="tab" class="sign-up" onClick={this.handleCheck.bind(this)}/>
            <label for="tab-2" class="tab">Registrar </label>
      		<div class="login-form">

      			<div class="sign-in-htm">
      				<div class="group">
      					<label for="user"  class="label">Email</label>
      					<input id="emailLoginInput" required type="email" class="input"/>
                <div className="input-error">
                  {this.state.errorEmailLogin}
                </div>
      				</div>
      				<div class="group">
      					<label for="pass" class="label">Password</label>
      					<input id="passLoginInputs" required type="password" class="input" data-type="password"/>
                <div className="input-error">
                  {this.state.errorPasswordLogin}
                </div>
            	</div>
              {
                // <div class="group">
                //   <input id="check" type="checkbox" class="check" checked/>
                //   <label for="check"><span class="icon"></span> Keep me Signed in</label>
                // </div>
              }
            {
              this.props.auth&&
              this.props.auth.error&&
              <div className="input-error">
                {this.props.auth.error}
              </div>
            }
            	<div class="hr"></div>
      				<div class="group">
      					<input type="submit" class="button"
                  onClick = {this.login.bind(this)}
                  value="Entrar"/>
      				</div>

      				<div class="hr"></div>
              {
                // <div class="foot-lnk">
                //   <a href="#forgot">Forgot Password?</a>
                // </div>
              }
      			</div>


      			<div class="sign-up-htm">
      				<div class="group">
      					<label for="user" class="label" >Nombre</label>

      					<input
                  id="nameInput"
                  required
                  minLength={5}
                  type="text" class="input"/>
      				</div>
              {
                this.state.errorNameRegister &&
                <div className="input-error">
                  {this.state.errorNameRegister}
                </div>
              }

      				<div class="group">
      					<label for="pass" class="label">Password</label>
      					<input id="passInput" required type="password" minLength={8} class="input" data-type="password"/>
                {
                  this.state.errorPasswordRegister &&
                  <div className="input-error">
                    {this.state.errorPasswordRegister}
                  </div>
                }
              </div>
      				<div class="group">
      					<label for="pass" class="label">Confirma el password</label>
      					<input id="confirmPassInput" required type="password" minLength={8} class="input" data-type="password"/>
                {
                  this.state.errorConfirmPasswordRegister &&
                  <div className="input-error">
                    {this.state.errorConfirmPasswordRegister}
                  </div>
                }
              </div>
      				<div class="group">
      					<label for="emailInputRegister" class="label">Email</label>
      					<input id="emailInputRegister" required type="email" class="input"/>
                {
                  this.state.errorEmailRegister &&
                  <div className="input-error">
                    {this.state.errorEmailRegister}
                  </div>
                }
            	</div>
              <div class="hr"></div>

      				<div class="group">
      					<input type="submit" class="button" value="Registrar"
                  onClick={this.register.bind(this)}
                  />
      				</div>
              {
                this.props.auth &&
                this.props.auth.user &&
                this.props.auth.user.status &&
                <div className="input-error">
                  {this.props.auth.user.status.message}
                </div>
              }
      			</div>
      		</div>
      	</div>

        {
          !this.state.checked&&
          <div class="login-img">
            <img
              width="70%"
              height="70%"
              src="../LogoGolf.jpeg"
              />
          </div>
        }
      </div>
    </div>
    )
  }

}

OneForm.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  // errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  // errors: state.errors
});

export default connect(
  mapStateToProps,
  {
    loginUser,
    registerUser
   }
)(OneForm);
