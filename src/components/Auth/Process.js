/**
 * @Author: memo
 * @Date:   2020-10-24T23:04:47-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-12T09:44:20-06:00
 */

 import React, { Component } from "react";
 import PropTypes from "prop-types";
 import { connect } from "react-redux";
 import "./Login.scss";
 import { loginUser, resetUser, confirmUser, activationUser }
    from "../../actions/authActions";
 import {isMobile} from 'react-device-detect';
 import Modal from '../Modal/Modal'
 const isEmpty = require("is-empty");


 class Process extends Component {
   constructor() {
     super();
     this.state = {
       password: "",
       confirmPassword:"",
       errorMessage:null,
       errorPassword:null,
       isActivated:false,
       isVisibleModal:false
     };
      this.onChangePassword = this.onChangePassword.bind(this);
   }

   onChangePassword(e){
     this.setState({ [e.target.id]: e.target.value });

   }

   componentDidMount() {
     const { code } = this.props.match.params;
     this.setState({code})
   }

   componentDidUpdate(prevProps){
     if (this.props.auth
          && this.props.auth.user
          && (this.props.auth.user != prevProps.auth.user)
          && this.props.auth.user.status
          && this.props.auth.user.status.code
      ) {
        if(this.props.auth.user.status.code == "0000"){
            this.setState({
              isActivated:true,
              activationMessage: this.props.auth.user.status.message,
              email: this.props.auth.user.status.email
            })
        }else{
            this.setState({
              errorMessage: this.props.auth.user.status.message
            })
        }
     }
   }

   componentWillReceiveProps(nextProps) {
      const { code } = nextProps.match.params;
      if (nextProps.auth.isAuthenticated) {
        this.props.history.push("/dashboard");
      }

      if (nextProps.errors) {
        this.setState({
          errors: nextProps.errors
        });
      }
    }

   validateForm(){
     console.log("validateForm")

     if(isEmpty(this.state.password)){
       this.setState({
         errorPassword:"El password es requerido"
       })
     }else{
       this.setState({
         errorPassword:null
       })
     }

     if(isEmpty(this.state.confirmPassword)){
       this.setState({
         errorConfirmPassword:"El password de confirmación es requerido"
       })
     }else{
       this.setState({
         errorConfirmPassword:null
       })
     }

     if(
       !isEmpty(this.state.confirmPassword) &&
       !isEmpty(this.state.password) &&
       this.state.confirmPassword != this.state.password
     ){
       this.setState({
         errorConfirmPassword:"El password no coincide."
       })
     }else{
       this.setState({
         errorConfirmPassword:null
       })
     }

     if(
       this.state.errorConfirmPassword == null &&
       this.state.errorPassword == null
     ){
        this.props.activationUser({
            password:this.state.password,
            code:this.props.match.params.code
          }
        )
     }

   }

   login(){
     const userData = {
       email: this.state.email,
       password: this.state.password,
     };
     console.log("login")
     this.props.loginUser(userData);
   }

   render() {
     return (
          <div className="contenedor center" >
          <div className="contenido">

              <div className="contenedorbody">
                <div className="columnleft">
                  <center>
                    <img
                        width='80%'
                        src="./../../../images/mainLogo.png"
                    />
                    <img
                        width='80%'
                        src="./../../../images/logos_vertical.png"
                    />
                  </center>
                </div>

                {!this.state.isActivated &&
                  <div className="columnrightProcess">
                      <center>

                      <div className="registerPwd">
                          <div className="title-process">
                            <label>Crea tu nueva contraseña</label>
                          </div>
                          <input
                            onChange={this.onChangePassword}

                            value={this.state.password}
                            error={this.state.errorPassword}
                            id="password"
                            type="password"
                            className="auth-input"
                            min="8"
                            placeholder="Nueva contraseña"
                          />
                          <div className="auth-error">
                            {this.state.errorPassword}
                          </div>


                          <input
                            onChange={this.onChangePassword}
                            value={this.state.confirmPassword}
                            error={this.state.errorConfirmPassword}
                            id="confirmPassword"
                            type="password"
                            className="auth-input"
                            min="8"
                            placeholder="Confirmar contraseña"
                          />
                          <div className="auth-error">
                            {this.state.errorConfirmPassword}
                          </div>

                          {
                            !isMobile &&
                            <div >
                                <img id="plecaProcess" src="./../../../images/pleca.png">
                                </img>
                            </div>
                          }

                          <div>
                            <button type="submit" className="auth-button" onClick={this.validateForm.bind(this)}>
                              Guardar
                            </button>
                            <div className="auth-error">
                              {this.state.errorMessage}
                            </div>
                          </div>
                      </div>


                      </center>
                  </div>

                }

                {this.state.isActivated &&
                  <div className={isMobile?"columnright-mobile":"columnright"}>
                    <div className="registerPwd confirmTitle">
                      <center>
                        <div className="ready-text">
                            ¡Listo!
                        </div>
                        <label className="ready-text">
                            {this.state.activationMessage}
                        </label>

                        {
                          !isMobile &&
                          <div className="fix-pleca">
                            <img id="pleca" src="./../../../images/pleca.png" width="90%">
                            </img>
                          </div>
                        }

                        <div>
                          <button type="submit" className="auth-button" onClick={this.login.bind(this)}>
                            Continuar
                          </button>
                          <div className="auth-error">
                            {this.state.errorMessage}
                          </div>
                        </div>
                      </center>
                    </div>
                  </div>
                }

              </div>
            </div>
          </div>
     )
   }
 }

  Process.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
  };

  const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
  });

  export default connect(
    mapStateToProps,
    {
      loginUser,
      resetUser,
      confirmUser,
      activationUser
     }
  )(Process);
