/**
 * @Author: memo
 * @Date:   2020-10-04T18:56:08-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-12T21:27:34-06:00
 */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "./Login.scss";
import { loginUser, resetUser } from "../../actions/authActions";
import {isMobile} from 'react-device-detect';
import Modal from '../Modal/Modal'

const LOGIN_TITLE = "Log in"
const FORGOT_TITLE = "Olvidé mi contraseña"
const LOGIN_ACTION = "Entrar"
const FORGOT_ACTION = "ENVIAR CORREO"

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {},
      errorMessage:null,
      errorPassword:null,
      errorEmail:null,
      title:LOGIN_TITLE,
      title2:FORGOT_TITLE,
      actionButton: LOGIN_ACTION,
      isVisibleModal:false
    };
     this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    // If logged in and user navigates to Login page, should redirect them to dashboard
    //console.log("this.props.auth:", this.props.auth)
    console.log("this.props.auth.isAuthenticated:", this.props.auth.isAuthenticated)
    if (this.props.auth.isAuthenticated) {
        console.log("to dashboard")
        this.props.history.push("/dashboard");
    }
  }

  componentDidUpdate(prevProps) {
    console.log("this.props:"+ JSON.stringify(this.props))
    if(this.props.auth &&
      (this.props.auth.error != prevProps.auth.error)
    ){
      this.setState({
        errorMessage:this.props.auth.error,
      })
    }
    if(this.props.auth &&
      this.props.auth.activatePopUp  != prevProps.auth.activatePopUp
    ){
      this.setState({
        isVisibleModal :this.props.auth.activatePopUp
      })
    }

  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }

    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  validarEmail(email) {
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (emailRegex.test(email)){
     return true
    } else {
    return false
    }
  }

  alterForm(){
    if(this.state.title === LOGIN_TITLE){
      this.setState({
        title        : FORGOT_TITLE,
        title2       : LOGIN_TITLE,
        actionButton : FORGOT_ACTION
      })
    }else {
      this.setState({
        title        : LOGIN_TITLE,
        title2       : FORGOT_TITLE,
        actionButton : LOGIN_ACTION
      })
    }
  }

  onCloseModal(){
      this.setState({
        isVisibleModal : false
      })
  }

  onChange(e){
    this.setState({ [e.target.id]: e.target.value });
  }

  onSubmit = e => {
 e.preventDefault();
 console.log("onSubmit")
 if(this.state.password == null ){
     this.setState({
       errorPassword:"La contraseña es requerida",
     })
 }else{
   this.setState({
     errorPassword:null
   })
 }

 if(this.state.email == null){
   this.setState({
       errorEmail:"El correo  es requerido.",
   })
 }else if(this.state.email !=null && !this.validarEmail(this.state.email)){
   this.setState({
       errorEmail:"El correo  es inválido.",
   })
 }else{
   this.setState({
     errorEmail:null
   })
 }
 console.log("this.state:", this.state)
 if(
    this.state.errorEmail == null
    && this.state.errorPassword == null
    && this.state.actionButton == LOGIN_ACTION
 ){
   const userData = {
     email: this.state.email,
     password: this.state.password,
   };
   console.log("login")
   this.props.loginUser(userData);
 }else if (
   this.state.errorEmail === null
   && this.state.actionButton === FORGOT_ACTION
 ){
   const userData = {
     email: this.state.email
   };
   this.props.resetUser(userData);
 }
};

displayForm(){
  return (<form
  ref={(el) => {this.loginRef = el}}
  className="auth-form" noValidate onSubmit={this.onSubmit}>
  {
    isMobile &&
      <div className="headerLoginMobile">
          <img
              width='60%'
              src="./../../../images/mainLogo.png"
          />
          <img
              width='40%'
              src="./../../../images/logos_vertical.png"
          />
      </div>
  }
  <div className="auth-group">
    <div className="color-transparent"> . </div>

    {
      <div className="auth-header">{this.state.title}</div>
    }
    <label>
      <input
        onChange={this.onChange}
        value={this.state.email}
        error={this.state.errorEmail}
        id="email"
        type="email"
        className="auth-input"

        placeholder="email"
      />
      <div className="auth-error">
        {this.state.errorEmail}
      </div>
    </label>
  </div>

  {
    this.state.actionButton === LOGIN_ACTION &&
    <div className="auth-group">
      <label>
        <input
          onChange={this.onChange}
          value={this.state.password}
          error={this.state.errorPassword}
          id="password"
          type="password"
          className="auth-input"
          placeholder="contraseña"
        />
        <div className="auth-error">
          {this.state.errorPassword}
        </div>
      </label>
    </div>
  }


    <div className="auth-group" onClick={this.alterForm.bind(this)}>
       <center>
         <label id="forgotPswd">
           {this.state.title2}
         </label>
       </center>
    </div>

    <div >
     <center>
       <img id="pleca" src="./../../../images/pleca.png">
       </img>
     </center>
    </div>


    <div>
      <button type="submit" className="auth-button" >
        {this.state.actionButton}
      </button>


      <div className="auth-error">
        {this.state.errorMessage}
      </div>
    </div>

    <div className="color-transparent"> . </div>
   </form>)
}

 displayFormWeb(){
   return (<center><form
   ref={(el) => {this.loginRef = el}}
   className="auth-form-web" noValidate onSubmit={this.onSubmit}>
   <div className="auth-group">
     <div className="color-transparent"> . </div>

     {
       <div className="auth-header">{this.state.title}</div>
     }
     <label>
       <input
         onChange={this.onChange}
         value={this.state.email}
         error={this.state.errorEmail}
         id="email"
         type="email"
         className="auth-input"

         placeholder="email"
       />
       <div className="auth-error">
         {this.state.errorEmail}
       </div>
     </label>
   </div>

   {
     this.state.actionButton === LOGIN_ACTION &&
     <div className="auth-group">
       <label>
         <input
           onChange={this.onChange}
           value={this.state.password}
           error={this.state.errorPassword}
           id="password"
           type="password"
           className="auth-input"
           placeholder="contraseña"
         />
         <div className="auth-error">
           {this.state.errorPassword}
         </div>
       </label>
     </div>
   }


     <div className="auth-group" onClick={this.alterForm.bind(this)}>
        <center>
          <label id="forgotPswd">
            {this.state.title2}
          </label>
        </center>
     </div>

     <div >
      <center>
        <img id="pleca" src="./../../../images/pleca.png">
        </img>
      </center>
     </div>


     <div>
       <button type="submit" className="auth-button" >
         {this.state.actionButton}
       </button>


       <div className="auth-error">
         {this.state.errorMessage}
       </div>
     </div>

     <div className="color-transparent"> . </div>
    </form></center>)
 }

  render() {
    var containerLogin = isMobile ? "contenedor center":"base-wrapper"
    return (
      <div className={containerLogin}>
      {
        !isMobile &&
        <div className="div1-1">
          <img
              width="80%"
              src="./images/Log_In.png"
            />
        </div>
      }
      {
        // !isMobile &&
        // <div className="div1">
        //   <img
        //       width='200px'
        //       src="./images/logos_vertical.png"
        //     />
        // </div>
      }
      {
      // !isMobile &&
      //   <div className="div1-1">
      //   <img
      //     width='30%'
      //     src="./images/mainLogo.png"
      //   />
      //   </div>
      }
      {
        // !isMobile &&
        // <div className="div1-2">
        //     <img
        //     width='20%'
        //     src="./images/mainLogo.png"
        //     />
        // </div>
      }
      {
        // isMobile &&
        //   <div className="headerLoginMobile">
        //       <img
        //           width='30%'
        //           src="./../../../images/mainLogo.png"
        //       />
        //       <img
        //           width='30%'
        //           src="./../../../images/logos_vertical.png"
        //       />
        //   </div>
      }
      { isMobile &&
        <div className="div2">
              <center>
                {
                  this.displayForm()
                }
              </center>
         </div>
      }


      { !isMobile &&
        <div className="div2-web">
            {this.displayFormWeb()}
        </div>
      }
       <Modal
        isVisibleModal = {this.state.isVisibleModal}
        message = {this.props.auth && this.props.auth.user &&
            this.props.auth.user.status &&
            this.props.auth.user.status.message
        }
        message2 = {this.props.auth && this.props.auth.user &&
            this.props.auth.user.status &&
            this.props.auth.user.status.message2
        }
        onCloseModal = {this.onCloseModal.bind(this)}
       />
      </div>
    );
  }

}

  Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
  };

  const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
  });

  export default connect(
    mapStateToProps,
    {
      loginUser,
      resetUser
     }
  )(Login);
