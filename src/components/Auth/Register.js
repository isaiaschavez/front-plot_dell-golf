/**
 * @Author: memo
 * @Date:   2020-10-04T18:56:08-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-10-24T22:00:26-05:00
 */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "./Login.scss";
import { loginUser, resetUser, confirmUser } from "../../actions/authActions";
import {isMobile} from 'react-device-detect';
import Modal from '../Modal/Modal'

const LOGIN_TITLE = "Ingresa tu mail"
const FORGOT_TITLE = "Ingresa tu mail"
const LOGIN_ACTION = "Enviar correo"
const FORGOT_ACTION = "ENVIAR CORREO"

class Register extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {},
      errorMessage:null,
      errorPassword:null,
      errorEmail:null,
      title:LOGIN_TITLE,
      title2:FORGOT_TITLE,
      actionButton: LOGIN_ACTION,
      isVisibleModal:false
    };
     this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    // If logged in and user navigates to Login page, should redirect them to dashboard
    //console.log("this.props.auth:", this.props.auth)
    console.log("this.props.auth.isAuthenticated:", this.props.auth.isAuthenticated)
    if (this.props.auth.isAuthenticated) {
        console.log("to dashboard")
        this.props.history.push("/dashboard");
    }
  }

  componentDidUpdate(prevProps) {
    // console.log("this.props:"+ JSON.stringify(this.props))
    if(this.props.auth &&
      (this.props.auth.error != prevProps.auth.error)
    ){
      this.setState({
        errorMessage:this.props.auth.error,
      })
    }
    // alert("this.props.auth:", this.props.auth)
    // if(this.props.auth &&
    //   (this.props.auth.activatePopUp != prevProps.auth.activatePopUp)
    // ){
    //   this.setState({
    //     isVisibleModal :this.props.auth.user.activatePopUp
    //   })
    // }

  }

  componentWillReceiveProps(nextProps) {
    // console.log("componentWillReceiveProps:", nextProps)
    // if (nextProps.auth.user.activatePopUp) {
    //   this.setState({
    //     isVisibleModal :this.props.auth.user.activatePopUp
    //   })
    // }

    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  validarEmail(email) {
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (emailRegex.test(email)){
     return true
    } else {
    return false
    }
  }

  alterForm(){
    if(this.state.title === LOGIN_TITLE){
      this.setState({
        title        : FORGOT_TITLE,
        title2       : LOGIN_TITLE,
        actionButton : FORGOT_ACTION
      })
    }else {
      this.setState({
        title        : LOGIN_TITLE,
        title2       : FORGOT_TITLE,
        actionButton : LOGIN_ACTION
      })
    }
  }

  onCloseModal(){
      this.setState({
        isVisibleModal : false
      })
  }

  onChange(e){
    this.setState({ [e.target.id]: e.target.value });
  }

  onSubmit = e => {
 e.preventDefault();
 console.log("onSubmit")

 if(this.state.email == null){
   this.setState({
       errorEmail:"El correo  es requerido.",
   })
 }else if(this.state.email !=null && !this.validarEmail(this.state.email)){
   this.setState({
       errorEmail:"El correo  es inválido.",
   })
 }else{
   this.setState({
     errorEmail:null
   })
 }
 console.log("this.state:", this.state)
 if(
    this.state.errorEmail == null
 ){
   const userData = {
     email: this.state.email,
   };
     console.log("userData:", userData)
     this.setState({
       isVisibleModal : true
     })
    this.props.confirmUser(userData);
 }
};

 displayForm(){
   return (<form
   ref={(el) => {this.loginRef = el}}
   className="auth-form" noValidate onSubmit={this.onSubmit}>

   <div className="auth-group">
     <div className="color-transparent"> . </div>
     {
       <div className="auth-header">{this.state.title}</div>
     }
     <label>
       <input
         onChange={this.onChange}
         value={this.state.email}
         error={this.state.errorEmail}
         id="email"
         type="email"
         className="auth-input"

         placeholder="email"
       />
       <div className="auth-error">
         {this.state.errorEmail}
       </div>
     </label>
   </div>

     <div >
       <img id="pleca" src="./../../../images/pleca.png">
       </img>
     </div>


     <div>
       <button type="submit" className="auth-button" >
         {this.state.actionButton}
       </button>


       <div className="auth-error">
         {this.state.errorMessage}
       </div>
     </div>

     <div className="color-transparent"> . </div>
    </form>)
 }

  render() {
    return (
      <div className="base-wrapper">
      {
        !isMobile &&
        <div className="div1">
          <img
              width='23%'
              src="./images/logos_vertical.png"
            />
        </div>
      }
      {
      !isMobile &&
        <div className="div1-1">
        <img
        width='25%'
        src="./images/mainLogo.png"
        />
        </div>
      }
      {
        !isMobile &&
        <div className="div1-2">
            <img
            width='20%'
            src="./images/mainLogo.png"
            />
        </div>
      }
      <div className="div2">
            { !isMobile &&
              this.displayForm()
            }
            {
            isMobile &&
            <center>
              {
                this.displayForm()
              }
            </center>
          }
       </div>
       <Modal
        isVisibleModal = {this.state.isVisibleModal}
        message = {this.props.auth && this.props.auth.user &&
            this.props.auth.user.status &&
            this.props.auth.user.status.message
        }
        onCloseModal = {this.onCloseModal.bind(this)}
       />
      </div>
    );
  }

}

  Register.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
  };

  const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
  });

  export default connect(
    mapStateToProps,
    {
      loginUser,
      resetUser,
      confirmUser
     }
  )(Register);
