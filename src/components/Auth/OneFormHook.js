import React, { Component, PureComponent , useState  } from "react";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import "./../styles/base.scss";
import "./OneForm.scss";
import { loginUser } from "../../actions/authActions";
import {isMobile} from 'react-device-detect';
const isEmpty = require("is-empty");

function OneForm ()  {

    const [name, setName] = useState('')
    const [errorName, setErrorName] = useState('')
    const [errorPassword, setErrorPassword] = useState('')
    const [checked, setChecked] = useState(false)

    //const handleClick = value => this.setState({ value });
    const [inputs, setInputs] = useState({
        name: '',
        password: ''
    });


    function handleChange(e) {
        const { name, value } = e.target;
        setName(value)
        // setInputs(inputs => ({ ...inputs, [name]: value }));
    }

    function handleCheck(e){
      const { name, value } = e.target;
      console.log("value:", value)
      setChecked(value)
    }

    function register(){
      var nameInput = document.getElementById('nameInput')
      var passInput = document.getElementById('passInput')
      console.log("nameInput:" + nameInput.value)
      console.log("passInput:" + passInput.value)
      if(isEmpty(nameInput.value)){
        setErrorName("El nombre es requerido.")
      }else{
        if(nameInput.value.length<5){
          setErrorName("Ingrese por lo menos 6 caracteres.")
        }else{
          setErrorName(null)
        }
      }

      if(isEmpty(passInput.value)){
        setErrorPassword("El password es requerido.")
      }else{
        if(passInput.value.length<8){
          setErrorPassword("Ingrese por lo menos 8 caracteres.")
        }else {
          setErrorPassword(null)
        }
      }

      // if(nameInput.value==''){
      //
      // }
    }

    return(
      <div class="body-login">
      <div class="login-wrap">
      	<div class="login-html">
      		<input id="tab-1" type="radio" name="tab" class="sign-in" onClick={handleCheck} />
            <label for="tab-1" class="tab">Entrar</label>
      		<input id="tab-2" type="radio" name="tab" class="sign-up" onClick={handleCheck} />
            <label for="tab-2" class="tab">Registrar </label>

      		<div class="login-form">

      			<div class="sign-in-htm">
      				<div class="group">
      					<label for="user" class="label">Email</label>
      					<input id="user" type="email" class="input"
                      />
                <div className="input-error">
                  UN error
                </div>
      				</div>
      				<div class="group">
      					<label for="pass" class="label">Password</label>
      					<input id="pass" type="password" class="input" data-type="password"/>
      				</div>
              {
                // <div class="group">
                //   <input id="check" type="checkbox" class="check" checked/>
                //   <label for="check"><span class="icon"></span> Keep me Signed in</label>
                // </div>
              }
      				<div class="group">
      					<input type="submit" class="button"
                  value="Entrar"/>
      				</div>
      				<div class="hr"></div>
              {
                // <div class="foot-lnk">
                //   <a href="#forgot">Forgot Password?</a>
                // </div>
              }
      			</div>


      			<div class="sign-up-htm">
      				<div class="group">
      					<label for="user" class="label" >Nombre</label>

      					<input
                  id="nameInput"
                  required
                  minlength={5}
                  onChange={handleChange}
                  type="text" class="input"/>
      				</div>
              {
                errorName &&
                <div className="input-error">
                  {errorName}
                </div>
              }

      				<div class="group">
      					<label for="pass" class="label">Password</label>
      					<input id="passInput" required type="password" minlength={8} class="input" data-type="password"/>
                  {
                    errorPassword &&
                    <div className="input-error">
                      {errorPassword}
                    </div>
                  }
            </div>
      				<div class="group">
      					<label for="pass" class="label">Confirma el password</label>
      					<input id="pass" required type="password" minlength={8} class="input" data-type="password"/>
      				</div>
      				<div class="group">
      					<label for="pass" class="label">Email</label>
      					<input id="pass" required type="email" minlength={5} class="input"/>
      				</div>
      				<div class="group">
      					<input type="submit" class="button" value="Registrar"
                  onClick={register}
                  />
      				</div>
      			</div>
      		</div>


      	</div>

        {
          !checked&&
          <div class="login-img">
            <img
              width="60%"
              height="60%"
              src="../LogoGolf.jpeg"
              />
          </div>
        }
      </div>
    </div>
    )


}
export default OneForm
