/**
 * @Author: memo
 * @Date:   2020-10-05T00:11:10-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-24T00:41:04-06:00
 */



import React, { Component } from "react";
import { connect } from "react-redux";
import {Piece} from 'avataaars';
import { setCurrentUser, getAvatar, getScoreTotal, updateUser, getUser } from "../../../actions/authActions";
import MyAvatar from './../../Nav/NavAvatar';
import store from "./../../../store";
import jwt_decode from "jwt-decode";

import AvatarModal from './AvatarModal';
import 'react-widgets/dist/css/react-widgets.css'
import {isMobile} from 'react-device-detect';
import './Avatar.scss'
const pageContainer = isMobile? "page-mobile-content":"page-content"

class AvatarComponent extends Component {
  constructor(props) {
    super(props);
     this.state = {
       isVisibleModal:false,
       isOnEdition:false
     };
     this.patchUser = this.patchUser.bind(this)
     this.onChange = this.onChange.bind(this);
  }

  UNSAFE_componentWillMount(){
    this.props.getAvatar(this.props.auth.user.id)
    this.props.getScoreTotal(this.props.auth.user.id)
    this.props.getUser(this.props.auth.user.id)
  }

  componentDidUpdate(prevProps) {

    console.log("SCORE this.props:", this.props.auth)
    if(
      this.props.auth.avatar
          && prevProps.auth.avatar
          && this.props.auth.avatar != prevProps.auth.avatar
    ){
      this.setState({
         topType:this.props.auth.avatar.topType,
         hairColor:this.props.auth.avatar.hairColor,
         facialHairType:this.props.auth.avatar.facialHairType,
         clotheType:this.props.auth.avatar.clotheType,
         eyeType:this.props.auth.avatar.eyeType,
         eyebrowType:this.props.auth.avatar.eyebrowType,
         mouthType:this.props.auth.avatar.mouthType,
         skinColor:this.props.auth.avatar.skinColor,
         accessoriesType:this.props.auth.avatar.accessoriesType,
      })
    }

    if(
      this.props.score
          && prevProps.score
          && this.props.score != prevProps.score
    ){
      console.log("in")
        this.setState({
          scoreTotal:this.props.score.score.scoreTotal
        })
    }

    if(this.props.auth &&
        this.props.auth.avatarUser &&
        prevProps.auth.avatarUser &&
        this.props.auth.avatarUser !== prevProps.auth.avatarUser
    ){
      console.log("this.props.auth.user>>>>", this.props.auth.user)
      console.log("prevProps.auth.user>>>>", prevProps.auth.user)
      this.setState({
        isOnEdition:false
      },()=>{
        this.forceUpdate()
      })
    }
    // store.dispatch(setCurrentUser(decoded));

  }

  openAvatar(){
    this.setState({
      isVisibleModal:true
    })
  }

  onCloseModal(){
    this.setState({
      isVisibleModal:false
    })
  }

  changeName(){
    this.setState({
      isOnEdition:true
    })
  }

  onChange(e){
    this.setState({ [e.target.id]: e.target.value });
  }

  patchUser(){

    // var data = localStorage.getItem("jwtTokenApp")
    // console.log("data:",  data)
    // const decoded = jwt_decode(data);
    // console.log("decoded:", decoded)
    console.log("this.state.name:" + this.state.name)
    console.log("this.state.lastname:" + this.state.lastname)
      this.props.updateUser(this.props.auth.user.id,
        {
          name:this.state.name,
          lastname:this.state.lastname
        })
  }

  renderWeb(){
    // var pageContainer = isMobile? "page-mobile-content":"page-content-award"

    return(<div className="page-content-avatar">
      <center>
         <div className="center">
            <div className="mainWrapperLayoutAvatar">
              <div className="mainLayoutAvatar" onClick={()=>this.openAvatar.bind(this)}>

                <div className="wrapperRightWeb">
                  <center>
                      {
                        // <img width="70%" src="/images/question.png"/>
                      }
                      {
                        <div className="fixAvatar">
                        <MyAvatar

                        />
                        </div>
                      }
                      <div className="create-button fix-create-button" onClick={this.openAvatar.bind(this)}>
                            Editar avatar
                      </div>
                  </center>
                </div>

                <div className="wrapperLeftWeb">
                    {
                      // !isMobile &&
                      // <div className="title-avatar">
                      // Mi perfil
                      // </div>
                    }
                    <div className="profileData">
                          <div className="titleWebAvatar">
                          Mi perfil
                          </div>
                            {
                                this.state.isOnEdition === false &&
                                <div className="nameProfile">
                                  <label className="inputProfile fixInputAvatarName">
                                      Nombre: {this.props.auth.avatarUser?this.props.auth.avatarUser.name
                                          + " " + this.props.auth.avatarUser.lastname
                                        :""}
                                  </label>
                                  <div className="edit-button inputProfile" onClick={this.changeName.bind(this)}>
                                      <center>
                                        <label className="font-edit-button">
                                          Editar
                                        </label>
                                      </center>
                                  </div>
                                </div>
                            }
                            {this.state.isOnEdition &&
                              <div className="rowButtonsEditionAvatar fixEditInputs">
                                  <label className="fixInputProfile fixInputAvatarName">
                                    Nombre:
                                  </label>
                                  <input className="avatarTextField"
                                    id="name"
                                    onChange={this.onChange}
                                    placeholder={this.props.auth.avatarUser.name}>
                                  </input>
                                  <input className="avatarTextField"
                                    id="lastname"
                                    onChange={this.onChange}
                                    placeholder={this.props.auth.avatarUser.lastname}>
                                  </input>
                              </div>
                            }

                            <label className="inputProfile">
                                Email: {this.props.auth.user?this.props.auth.user.email:""}
                            </label>

                            {
                              this.props.auth.score &&
                                <label className="inputProfile">
                                  Puntaje: {this.props.auth.score.scoreTotal}
                                </label>
                            }

                            {
                              // <button className="points-button">
                              //   {
                              //     this.props.auth.score &&
                              //     <label>
                              //       {this.props.auth.score.scoreTotal} puntos
                              //     </label>
                              //   }
                              // </button>
                            }
                            <div className="inputProfile">
                              <img width="100%" src="/images/pleca.png"/>
                            </div>

                            {
                              this.state.isOnEdition &&
                              <div className="rowButtonsEditionAvatar">
                                  <div>
                                      <button className="edit-button inputProfile" onClick={this.patchUser.bind(this)}>
                                        <label>
                                          Guardar
                                        </label>
                                      </button>
                                  </div>
                                  <div>
                                      <button className="cancel-button inputProfile" onClick={()=>{this.setState({isOnEdition:false})}}>
                                        <label>
                                          Cancelar
                                        </label>
                                      </button>
                                  </div>
                              </div>
                            }
                    </div>
                </div>
              </div>
            </div>
         </div>
       </center>
       <center>
        <AvatarModal
            isVisibleModal={this.state.isVisibleModal}
            onCloseModal={this.onCloseModal.bind(this)}
            avatar={this.props.auth.avatar}
        />
        </center>
      </div>
    )
  }


  renderMobile(){
      return(<div className="page-mobile-content-dashboard">
            <div className="mobile-content-dashboard">
                <center>
                  <div className="title-avatar-mobile">
                      Mi Perfil
                  </div>
                  <div className="fix-avatar-mobile">
                    <div>
                    </div>
                    <div>
                      <MyAvatar

                      />
                    </div>
                    <div>
                    </div>
                  </div>

                  <button className="create-button-mobile" onClick={this.openAvatar.bind(this)}>
                    Editar avatar
                 </button>
                 { !this.state.isOnEdition &&
                   <div className="divs-row-mobile">
                      Nombre:{this.props.auth.avatarUser?" " + this.props.auth.avatarUser.name
                             + " " + this.props.auth.avatarUser.lastname
                           :""}
                   </div>
                  }


                  {
                    this.state.isOnEdition &&
                    <div className="divs-row-mobile div-edition-name">
                      <div className="div-edition-name">
                          <div className="div-edition-name-left">
                              Nombre:
                          </div>
                          <div className="div-edition-name-right">
                              <input className="avatarTextField-mobile"
                                id="name"
                                onChange={this.onChange}
                                placeholder={this.props.auth.avatarUser.name}>
                              </input>
                              <input className="avatarTextField-mobile"
                                id="lastname"
                                onChange={this.onChange}
                                placeholder={this.props.auth.avatarUser.lastname}>
                              </input>
                          </div>
                      </div>
                    </div>
                  }


                  <div className="divs-row-mobile">
                      Email : {this.props.auth.user?this.props.auth.user.email:""}
                  </div>
                  {
                    this.props.auth.score &&
                      <div className="divs-row-mobile">
                      Puntaje:  {this.props.auth.score.scoreTotal}
                      </div>
                  }

                  {
                    !this.state.isOnEdition &&
                    <div className="edit-button fix-edit-button-mobile" onClick={this.changeName.bind(this)}>
                        <label className="font-edit-button">
                          Editar
                        </label>
                    </div>
                  }
                  {
                    this.state.isOnEdition &&
                    <div className="edit-button-zone">
                        <div className="zone-save">
                            <button className="edit-button-mobile-save" onClick={this.patchUser.bind(this)}>
                                Guardar
                            </button>
                        </div>
                        <div className="zone-space">
                        </div>
                        <div className="zone-save">
                            <button className="cancel-button-mobile" onClick={()=>{this.setState({isOnEdition:false})}}>
                                Cancelar
                            </button>
                        </div>
                    </div>
                  }
                </center>
            </div>
            <center>
             <AvatarModal
                 isVisibleModal={this.state.isVisibleModal}
                 onCloseModal={this.onCloseModal.bind(this)}
                 avatar={this.props.auth.avatar}
             />
             </center>
      </div>
      )
  }

  renderMobileOld(){
    var pageContainer = "page-content-avatar"

    return(<div className={pageContainer}>
        <div className="mainWrapperLayoutAvatar-mobile">

        <div className="wrapper-mobile">

              <div className="title-avatar-mobile">
                  Mi Perfil
              </div>
              {
                <div className="fix-avatar-mobile">
                  <MyAvatar

                  />
                </div>
              }
              <button className="create-button-mobile" onClick={this.openAvatar.bind(this)}>
                <center>
                    Crear avatar
                </center>
             </button>

            {
              // !isMobile &&
              // <div className="title-avatar">
              // Mi perfil
              // </div>
            }


                <label className="inputProfileModile">
                 {this.props.auth.user?this.props.auth.user.name
                        // + this.props.auth.user.last
                      :""}
                </label>
                <label className="inputProfileModile">
                    {this.props.auth.user?this.props.auth.user.email:""}
                </label>

                <label className="inputProfileModile">
                  Puntaje
                </label>
              <button className="points-button">
              {
                this.props.auth.score &&
                <label>
                  {this.props.auth.score.scoreTotal} puntos
                </label>
              }
              </button>

              <div className="edit-button inputProfile" onClick={this.changeName.bind(this)}>
                    <label className="font-edit-button">
                      Editar
                    </label>
              </div>
            </div>

        </div>
        <center>
         <AvatarModal
             isVisibleModal={this.state.isVisibleModal}
             onCloseModal={this.onCloseModal.bind(this)}
             avatar={this.props.auth.avatar}
         />
         </center>
      </div>)
  }

  render(){
    if (!isMobile) {
      return this.renderWeb()
    }else{
      return this.renderMobile()
    }
  }
}

const mapStateToProps = state => ({
  utils : state.utils,
  auth  : state.auth
});
export default connect(
  mapStateToProps,
  {
    getAvatar,
    getScoreTotal,
    updateUser,
    getUser
  }
)(AvatarComponent);
