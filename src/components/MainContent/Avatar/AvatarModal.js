/**
 * @Author: memo
 * @Date:   2020-10-18T16:35:14-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-24T15:14:35-06:00
 */
 import React, { Component,  useState, useRef, useEffect } from "react";
 import {isMobile} from 'react-device-detect';
 import { getAvatar, saveAvatar } from "../../../actions/authActions";
 import { connect } from "react-redux";

 import Avatar from 'avataaars'
 import "./../../styles/modal.scss";
 import Combobox from 'react-widgets/lib/Combobox'

 class AvatarModal extends Component {
   constructor(props) {
     super(props);
      this.state = {
        topType:null,
        hairColor:null,
        facialHairType:null,
        clotheColor:null,
        eyeType:null,
        eyebrowType:null,
        mouthType:null,
        skinColor:null
      };
   }

   onChangeStatus(e){
   console.log("e:", e)
   this.setState({
     top : e,
   })
 }

   onSelected(e){
       console.log("e:", e)
       this.setState({
         top:e.target.value
       })
   }

   onSelectedClotheType(e){
     this.setState({
       clotheType:e.target.value
     })
   }

   onChangeSelect(e){
      console.log("e:", e.target.value, e.target.id)
      var newValue = {}
      newValue[e.target.id] = e.target.value
      this.setState(newValue,()=>{
        console.log("this.state:", this.state)
      })
   }

   saveAvatarData(){
     console.log("saveAvatarData")
     var data = {
       userId:this.props.auth.user.id,
       avatar:{
         // "topType":this.state.topType,
         // "hairColor":this.state.hairColor,
         // "facialHairType":this.state.facialHairType,
         // "clotheColor":this.state.clotheColor,
         // "eyeType":this.state.eyeType,
         // "eyebrowType":this.state.eyebrowType,
         // "mouthType":this.state.mouthType,
         // "skinColor":this.state.skinColor


         topType:this.state.topType?this.state.topType:this.props.avatar.topType,
         accessoriesType:this.state.accessoriesType?this.state.accessoriesType:this.props.avatar.accessoriesType,
         hairColor:this.state.hairColor?this.state.hairColor:this.props.avatar.hairColor,
         facialHairType:this.state.facialHairType?this.state.facialHairType:this.props.avatar.facialHairType,
         clotheType:this.state.clotheType?this.state.clotheType:this.props.avatar.clotheType,
         clotheColor:this.state.clotheColor?this.state.clotheColor:this.props.avatar.clotheColor,
         eyeType:this.state.eyeType?this.state.eyeType:this.props.avatar.eyeType,
         eyebrowType:this.state.eyebrowType?this.state.eyebrowType:this.props.avatar.eyebrowType,
         mouthType:this.state.mouthType?this.state.mouthType:this.props.avatar.mouthType,
         skinColor:this.state.skinColor?this.state.skinColor:this.props.avatar.skinColor



       }
     }

     console.log("data:", data)
     this.props.saveAvatar(data)
     this.props.onCloseModal()
   }

   avatar(){
     var styleModal = isMobile ? {width: '100px', height: '100px'} : null
      return(<div className="mainBoxAvatar">
      <span className="close" onClick={this.props.onCloseModal}>&times;</span>

      <div>
            <Avatar
              style={styleModal}
              avatarStyle='Circle'
              topType={this.state.topType?this.state.topType:this.props.avatar.topType}
              accessoriesType={this.state.accessoriesType?this.state.accessoriesType:this.props.avatar.accessoriesType}
              hairColor={this.state.hairColor?this.state.hairColor:this.props.avatar.hairColor}
              facialHairType={this.state.facialHairType?this.state.facialHairType:this.props.avatar.facialHairType}
              clotheType={this.state.clotheType?this.state.clotheType:this.props.avatar.clotheType}
              clotheColor={this.state.clotheColor?this.state.clotheColor:this.props.avatar.clotheColor}
              eyeType={this.state.eyeType?this.state.eyeType:this.props.avatar.eyeType}
              eyebrowType={this.state.eyebrowType?this.state.eyebrowType:this.props.avatar.eyebrowType}
              mouthType={this.state.mouthType?this.state.mouthType:this.props.avatar.mouthType}
              skinColor={this.state.skinColor?this.state.skinColor:this.props.avatar.skinColor}
            />
      </div>

<div className="container">
   <form className="form-horizontal">
  <div className="row form-group">
      {
        // <label for="topType" className="col-sm-3 control-label">Top</label>
      }
        <div className="col-sm-9">
            <select id="topType" className="form-control" onChange={this.onChangeSelect.bind(this)}>
            <option value="">Parte superior</option>
            <option value="NoHair">Sin cabello</option>
            <option value="Eyepatch">Parche</option>
            <option value="Hat">Sombrero</option>
            <option value="Hijab">Hijab</option>
            <option value="Turban">Turban</option>
            <option value="WinterHat1">Invierno 1</option>
            <option value="WinterHat2">Invierno 2</option>
            <option value="WinterHat3">Invierno 3</option>
            <option value="WinterHat4">Invierno 4</option>
            <option value="LongHairBigHair">Cabello Largo 1</option>
            <option value="LongHairBob">Cabello Largo 2</option>
            <option value="LongHairBun">Cabello Largo 3</option>
            <option value="LongHairCurly">Cabello Largo 4</option>
            <option value="LongHairDreads">Cabello Largo 5</option>
            <option value="LongHairFrida">Cabello Largo 6</option>
            <option value="LongHairFro">Cabello Largo 7</option>
            <option value="LongHairFroBand">Cabello Largo 8</option>
            <option value="LongHairNotTooLong">Cabello Largo 9</option>
            <option value="LongHairShavedSides">Cabello Largo 10</option>
            <option value="LongHairMiaWallace">Cabello Largo 11</option>
            <option value="LongHairStraight">Cabello Largo 12</option>
            <option value="LongHairStraight2">Cabello Largo 13</option>
            <option value="LongHairStraightStrand">Cabello Largo 14</option>
            <option value="ShortHairDreads01">Cabello Corto 1</option>
            <option value="ShortHairDreads02">Cabello Corto 2</option>
            <option value="ShortHairFrizzle">Cabello Corto 3</option>
            <option value="ShortHairShaggyMullet">Cabello Corto 4</option>
            <option value="ShortHairShortCurly">Cabello Corto 5</option>
            <option value="ShortHairShortFlat">Cabello Corto 6</option>
            <option value="ShortHairShortRound">Cabello Corto 7</option>
            <option value="ShortHairShortWaved">Cabello Corto 8</option>
            <option value="ShortHairSides">Cabello Corto 9</option>
            <option value="ShortHairTheCaesar">Cabello Corto 10</option>
            <option value="ShortHairTheCaesarSidePart">Cabello Corto 11</option>
      </select>
    </div>
  </div>


   <div className="row form-group">
    {
      // <label for="clotheType" className="col-sm-3 control-label">👔 Clothes</label>
    }
     <div className="col-sm-9">
       <select id="clotheType" className="form-control" onChange={this.onChangeSelect.bind(this)}>
        <option value="">Ropa</option>
         <option value="BlazerShirt">Blazer</option>
         <option value="GraphicShirt">Camisa casual</option>
         <option value="ShirtCrewNeck">Camisa cuello medio</option>
         <option value="ShirtScoopNeck">Camisa cuello bajo</option>
         <option value="ShirtVNeck">Camisa cuello V</option>
         <option value="Overall">Overol</option>
         <option value="CollarSweater">Suéter </option>
         <option value="BlazerSweater">Sueter Blazer</option>
         <option value="Hoodie">sudadera con capucha</option>
       </select>
    </div>
    </div>

    <div className="row form-group">
      {
        // <label for="accessoriesType" className="col-sm-3 control-label"> 👓 Accessories</label>
      }
     <div className="col-sm-9"><select id="accessoriesType" className="form-control" onChange={this.onChangeSelect.bind(this)}>
        <option value="">Anteojos</option>
        <option value="Blank">Sin accesorio</option>

        <option value="Kurt">Modelo 1</option>
        <option value="Prescription01">Modelo 2</option>
        <option value="Prescription02">Modelo 3</option>
        <option value="Round">Modelo 4</option>
        <option value="Sunglasses">Modelo 5</option>
        <option value="Wayfarers">Modelo 6</option>
        </select>
      </div>
     </div>

   <div className="row form-group">
      {
        // <label for="hairColor" className="col-sm-3 control-label"> 💈 Hair Color</label>
      }
      <div className="col-sm-9"><select id="hairColor" className="form-control" onChange={this.onChangeSelect.bind(this)}>
      <option value="Auburn">Color del cabello</option>
      <option value="Auburn">Castaño</option>
        <option value="Black">Negro</option>
        <option value="Blonde">Rubio</option>
        <option value="BlondeGolden">Rubio intenso</option>
        <option value="Brown">Café claro</option>
        <option value="BrownDark">Café oscuro</option>
        <option value="PastelPink">Rosado</option>
        <option value="Red">Rojizo</option>
        <option value="Platinum">Plateado 1</option>
        <option value="SilverGray">Plateado 2</option>
        </select>
      </div>
    </div>

    <div className="row form-group">
      {
        // <label for="facialHairType" className="col-sm-3 control-label">Facial Hair</label>
      }
     <div className="col-sm-9"><select id="facialHairType" className="form-control" onChange={this.onChangeSelect.bind(this)}>
        <option value="Blank">Cabello facial</option>
        <option value="Blank">Ninguno</option>
        <option value="BeardMedium">Barba </option>
        <option value="BeardLight">Barba 2</option>
        <option value="BeardMagestic">Barba 3</option>
        <option value="MoustacheFancy">Bigote</option>
        <option value="MoustacheMagnum">Bigote 2</option>
      </select>
      </div>
    </div>

     <div className="row form-group">
     {
       // <label for="eyeType" className="col-sm-3 control-label">👁 Eyes</label>
     }
     <div className="col-sm-9"><select id="eyeType" className="form-control" onChange={this.onChangeSelect.bind(this)}>
       <option value="">Ojos</option>
       <option value="Close">Cerrados</option>
       <option value="Cry">Llorando</option>
       <option value="Default">Default</option>
       <option value="Dizzy">Mareado</option>
       <option value="EyeRoll">Rollo</option>
       <option value="Happy">Feliz</option>
       <option value="Hearts">Corazón</option>
       <option value="Side">De lado</option>
       <option value="Squint">Bizcos</option>
       <option value="Surprised">Sorprendido</option>
       <option value="Wink">Guiño</option>
       <option value="WinkWacky">Guiño 2</option>
     </select>
     </div>
     </div>

     <div className="row form-group">
      {
        // <label for="eyebrowType" className="col-sm-3 control-label">✏️ Eyebrow</label>
      }
     <div className="col-sm-9"><select id="eyebrowType" className="form-control" onChange={this.onChangeSelect.bind(this)}>
         <option value="">Cejas</option>
         <option value="Default">Default</option>
         <option value="Angry">Furioso</option>
         <option value="AngryNatural">Furioso 2</option>
         <option value="DefaultNatural">Default Natural</option>
         <option value="FlatNatural">Alargadas</option>
         <option value="RaisedExcited">Sorprendido</option>
         <option value="RaisedExcitedNatural">Sorprendido 2</option>
         <option value="UnibrowNatural">Uniceja</option>
         <option value="UpDown">Sorprendido 3</option>
         <option value="UpDownNatural">Sorprendido 4</option>
         <option value="SadConcerned">Triste</option>
         <option value="SadConcernedNatural">Triste 2</option>
     </select>
     </div>
     </div>

     <div className="row form-group">
      {
        // <label for="mouthType" className="col-sm-3 control-label">👄 Mouth</label>
      }
      <div className="col-sm-9"><select id="mouthType" className="form-control" onChange={this.onChangeSelect.bind(this)}>
          <option value="">Boca</option>
          <option value="Default">Default</option>
          <option value="Concerned">Preocupado</option>
          <option value="Disbelief">Incredulo</option>
          <option value="Eating">Comiendo</option>
          <option value="Grimace">Mueca</option>
          <option value="ScreamOpen">Gritando</option>
          <option value="Tongue">Lengua</option>
          <option value="Serious">Serio</option>
          <option value="Smile">Sonriendo</option>
          <option value="Twinkle">Sonriendo 2</option>
          <option value="Sad">Triste</option>
          <option value="Vomit">Zombie</option>
      </select>
      </div>
     </div>

     <div className="row form-group">
      {
        // <label for="skinColor" className="col-sm-3 control-label">🎨 Skin</label>
      }
      <div className="col-sm-9">
        <select id="skinColor" className="form-control" onChange={this.onChangeSelect.bind(this)}>
            <option value="">Piel</option>
            <option value="Tanned">Bronceado</option>
            <option value="Yellow">Amarillo</option>
            <option value="Pale">Claro</option>
            <option value="Light">Caucásico</option>
            <option value="Brown">Latino</option>
            <option value="DarkBrown">Latino 2</option>
            <option value="Black">Etiópico</option>
            </select></div>
     </div>

     </form>
  </div>
   <button type="submit" className="save-button" onClick={this.saveAvatarData.bind(this)}>
      <label>
          Guardar
      </label>
  </button>
 </div>)
}

   render(){
     return(
       <div className={this.props.isVisibleModal ? "modal display-block" : "modal display-none"}>
         <div className="modal-content">
           <center>
            {this.avatar()}
           </center>
         </div>
       </div>
     )
   }
 }


 const mapStateToProps = state => ({
   utils : state.utils,
   auth  : state.auth
 });
 export default connect(
   mapStateToProps,
   {
     getAvatar,
     saveAvatar
   }
 )(AvatarModal);

// export default AvatarModal;
