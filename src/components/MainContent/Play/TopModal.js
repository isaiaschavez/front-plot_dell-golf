/**
 * @Author: memo
 * @Date:   2020-10-29T09:41:56-06:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-01T17:38:22-06:00
 */
 import React, { Component,  useState, useRef, useEffect } from "react";
 import {isMobile} from 'react-device-detect';
 import "./../../styles/modal.scss";
 import config from './../../../config/data.js'

 import Combobox from 'react-widgets/lib/Combobox'
 import TopTenItem from './TopTenItem'

 class TopModal extends Component {
   constructor(props) {
     super(props);
      this.state = {
        isLoadedVideo:false,
        isVisibleStep:false,
      }
    }

    componentDidMount(){
      // var url = 'http://192.168.1.76:5556/api/utilities/top/'
      const url =  config.protocol+config.host
             +config.port+config.getTop
      var data = []
      fetch(url)
        .then(response => response.json())
        .then(data => {
          if(data.status && data.status.code === "0000"){
            console.log("data.result.users:", data.result.users)
            if(data.result.users){
              this.setState({
                users: data.result.users
              })
            }
          }})
          .catch(e=>{
            console.log("error:", e)
          });
    }

    top8(){

        var items = []
        if(this.state.users && this.state.users.length>0){
          items = this.state.users.slice(0,8)
        }
        var item = []

        // for (var i = 0; i < 8; i++) {
        //   item[i] = <TopTenItem
        //                number={i+1}
        //                name={"name:" + i }
        //                points={"189"}
        //            />
        // }
        for (var i = 0; i < items.length; i++) {
           item[i] = <TopTenItem
                        number={i+1}
                        name={items[i].user}
                        points={items[i].scoreTotal}
                    />
        }

        return(<div>
                <label className="titleTop">
                    Ranking general
                </label>
                {item}
            </div>
        )
    }

    restTop(){
      var items = []
      if(this.state.users && this.state.users.length>0){
        items = this.state.users.slice(8,15)
      }
      var item = []
      // for (var i = 0; i < 8; i++) {
      //   item[i] = <TopTenItem
      //                number={i+1}
      //                name={"name:" + i }
      //                points={"189"}
      //            />
      // }

      for (var i = 0; i < items.length; i++) {
        item[i] = <TopTenItem
                    number={i+8}
                    name={items[i].user}
                    points={items[i].scoreTotal}
                  />
      }
      return(<div>
              {item}
          </div>
      )
    }

    onCloseModal(){
      this.props.onCloseModal()
    }

    renderWeb(){
      var display = isMobile ? "scoreContainerMobile " : "scoreContainer";
      return(  <div className={this.props.isVisibleTopModal ?
                          "modal display-block" : "modal display-none"}>
              <div className="modal-content-video">
                  <span className="close-top" onClick={this.onCloseModal.bind(this)}>&times;</span>
                  <center>
                  <>
                    <div className="topImage2">
                        <img src="/images/mainLogo.png" width="100%"/>
                    </div>
                    <div className={display}>
                      <div className="scoreRow">
                            {this.top8()}
                      </div>
                      <div className="scoreRow">
                            {this.restTop()}
                      </div>
                    </div>
                  </>
                  </center>
              </div>
            </div>)
      }

    renderMobile(){
      return(  <div className={this.props.isVisibleTopModal ?
                          "modal display-block" : "modal display-none"}>

                  <div className="center modal-content-top">
                  <span className="close-top" onClick={this.onCloseModal.bind(this)}>&times;</span>

                    <center>
                    <>
                      <div className="topImage">
                          <img src="/images/mainLogo.png" width="50%"/>
                      </div>
                      <div className="scoreContainerMobile">
                        <div className="scoreRow">
                              {this.top8()}
                        </div>
                        <div className="scoreRow">
                              {this.restTop()}
                        </div>
                      </div>
                    </>
                    </center>

                  </div>
                </div>)
    }

    render(){
      if(isMobile){
        return this.renderMobile()
      }else{
        return this.renderWeb()
      }
    }
  }


 export default TopModal;
