/**
 * @Author: memo
 * @Date:   2020-10-07T21:51:02-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-12T08:30:56-06:00
 */
import React, { Component,  useState, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import {isMobile} from 'react-device-detect';
import "./Play.scss";
import config from './../../../config/data.js'

import Quizz from './Quizz'
import VideoModal from './VideoModal';
import QuizModal from './QuizModal';
import TopModal from './TopModal';
import FinishModal from './FinishModal';

import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter
} from "react-router-dom";


const texts = []

texts[0] = {
  label1:"",
  label2:"",
  label3:""
}

texts[1] = {
  label1:"",
  label2:"Sabemos que no ha sido un año fácil y que muchos de nosotros hemos adoptado el trabajo remoto.",
  label3:"Te presentamos Work From Home de Dell Technologies."
}

texts[3] = {
  label1:"Ahora que ya conoces lo que podemos hacer, te contaré un poco de cómo lo haremos.",
  label2:"",
  label3:""
}


texts[4] = {
  label1:".",
  label2:"La transición hacia el mundo digital no sólo ha afectado nuestro estilo de vida, también las amenazas se han transformado, por lo que hoy más que nunca es clave estar protegido.",
  label3:""
}

texts[5] = {
  label1:"",
  label2:"La transición hacia el mundo digital no sólo ha afectado nuestro estilo de vida, también las amenazas se han transformado, por lo que hoy más que nunca es clave estar protegido.",
  label3:""
}


texts[6] = {
  label1:"La transición hacia el mundo digital no sólo ha afectado nuestro estilo de vida, también las amenazas se han transformado, por lo que hoy más que nunca es clave estar protegido.",
  label2:"",
  label3:""
}

texts[7] = {
  label1:"Como vimos en el video anterior, la seguridad es indispensable para una correcta operación, te presentamos las opciones que Dell Technologies ofrece.",
  label2:"",
  label3:""
}

texts[8] = {
  label1:"Como vimos en el video anterior, la seguridad es indispensable para una correcta operación, te presentamos las opciones que Dell Technologies ofrece.",
  label2:"",
  label3:""
}

texts[9] = {
  label1:"Las proyecciones en cuanto a transformación digital fueron rebasadas por mucho a partir de la crisis sanitaria.",
  label2:"",
  label3:""
}

texts[10] = {
  label1:"Las proyecciones en cuanto a transformación digital fueron rebasadas por mucho a partir de la crisis sanitaria.",
  label2:"",
  label3:""
}

texts[11] = {
  label1:"Platiquemos acerca de cómo los datos y las soluciones en la nube nos acercan cada vez más al futuro.",
  label2:"",
  label3:""
}


texts[12] = {
  label1:"Platiquemos acerca de cómo los datos y las soluciones en la nube nos acercan cada vez más al futuro.",
  label2:"",
  label3:""
}
texts[13] = {
  label1:"La transformación digital se permea cada día más a nuestra vida diaria, sin embargo aún hay un camino de transición que debemos recorrer, házlo de la mano de Dell Technologies. ",
  label2:"",
  label3:""
}

texts[14] = {
  label1:"La transformación digital se permea cada día más a nuestra vida diaria, sin embargo aún hay un camino de transición que debemos recorrer, házlo de la mano de Dell Technologies.  ",
  label2:"",
  label3:""
}

texts[15] = {
  label1:"La transformación digital se permea cada día más a nuestra vida diaria, sin embargo aún hay un camino de transición que debemos recorrer, házlo de la mano de Dell Technologies.  ",
  label2:"",
  label3:""
}






function Play ({auth}){
  const [openVideo, setOpenVideo] = useState(false)
  const [openQuiz, setOpenQuiz]   = useState(false)
  const [loadedScore, setLoadedScore]   = useState(false)
  const [storage, setStorage]   = useState([])
  const [openTop, setOpenTop]   = useState(false)
  const [score, setScore]       = useState([])
  const [openFinish, setOpenFinish]     = useState(false)
  const [level, setLevel]       = useState(0)
  const [isOver, setOver]       = useState(false)


  useEffect(() => {
    var levelAux
    function handleChangeLevel(e) {
      console.log("setting level:" + e)
      setLevel(e)
      levelAux = e

      if(e===17){

           // var url = 'http://192.168.1.76:5556/api/utilities/userFinishGame'
           const url =  config.protocol+config.host
                  +config.port+config.userFinishGame

           var data = {
             username:auth.user.name + " " + auth.user.lastname,
             userId:auth.user.id
           }
           fetch(url, {
               method: 'POST', // *GET, POST, PUT, DELETE, etc.
               cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
               headers: {
                 'Content-Type': 'application/json'
                 // 'Content-Type': 'application/x-www-form-urlencoded',
               },
               redirect: 'follow', // manual, *follow, error
               referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
               body: JSON.stringify(data) // body data type must match "Content-Type" header
           });
      }
    }


    function handleChangeScore(score) {
      console.log("handleChangeScore")
      console.log("handleChangeScore.levelAux:", levelAux)
      console.log("handleChangeScore.score:", score)
      console.log("handleChangeScore.level:" + level)
      console.log("handleChangeScore.storage:" + storage)

      // var url = 'http://192.168.1.76:5556/api/utilities/score/'
      const url =  config.protocol+config.host
             +config.port+config.setScore

      fetch(url, {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: 'follow', // manual, *follow, error
          referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify({
                  userId:auth.user.id,
                  level,
                  score
          }) // body data type must match "Content-Type" header
      });
    }

    // var url = 'http://192.168.1.76:5556/api/utilities/score/'+auth.user.id
    const url =  config.protocol+config.host
           +config.port+config.getScore + auth.user.id
    console.log("PlayHook-url:" + url)
    fetch(url)
      .then(response => response.json())
      .then(data => {
        console.log("PlayHook->data:", data)
        if(data.status && data.status.code === "0000"){
            try {
              console.log("PlayHook->data.result.score:"
                  + data.result.score)
              // console.log("CLocalStorage-url:" + url)



              window.localStorage.setItem(auth.user.id+"_score",
                    JSON.stringify(data.result.score)
              );

              setStorage(JSON.stringify(data.result.score))
              var data2 = window.localStorage.getItem(auth.user.id+"_score")

              // console.log("CLocalStorage-data2:", data2)
              // console.log("CLocalStorage-auth.user._id:", auth.user)
              // console.log("CLocalStorage...loading")
              setLoadedScore(true)
              // console.log("CLocalStorage...loaded")
            } catch (e) {
                console.log("e:", e)
            } finally {

            }

        }
      })
      .catch(e=>{
        console.log("error:", e)
      });



    window.addEventListener("message", function (event) {


      // console.log("receiving:", event)
      // console.log("event.action=>", event.action)
      // console.log("event.data=>", event.data)
      //
      // console.log("event.data.action=>", event.data.action)
      // console.log("level:" + level)
      // console.log("levelAux:" + levelAux)

      if(event && event.data && event.data.action
        && event.data.action === "ShowQuizzPopup"
      ){
          // console.log("levelAux:" + levelAux)
          // console.log("level>" + level)
          switch(levelAux){

            // case 0:
            //     setOpenQuiz(true)
            //     break;
            case 1:
                setOpenVideo(true)
              break;
            case 3:
            case 5:
            case 7:
            case 9:
            case 11:
            case 13:
            case 15:
                setOpenQuiz(true)
              break;
            case 17:
                setOpenFinish(true)
              break;
            case 8:
               setOpenTop(true)
               break;
          }
      }

      if(event && event.data && event.data.action
        && event.data.action === "SendData"
      ){
          // alert("event.data.iLevel:" + event.data.iLevel)
          handleChangeLevel(event.data.iLevel)
      }


      if(event && event.data && event.data.action
        && event.data.action === "SendScore"
      ){
        console.log("event.data.action:", event.data.action)
        handleChangeScore(event.data.score)
      }


    });
  });
  const handle = useFullScreenHandle();
  const pageContainer = isMobile? "page-content-mobile-play":"page-content"

  function createMarkup() {
    var url = "<iframe src='/gameOri/index.html?userId="+auth.user.id+"' width: 80, height: 100, ></iframe>'"
    return {__html: url};
  }

  function checkQuizFinish(){
    setOpenQuiz(false)
    switch (level) {
      case 3:
      case 5:
      case 7:
      case 9:
      case 11:
      case 13:
            setOpenVideo(true)
        break;
      default:

    }
  }

  return(<div className={pageContainer}>

            {
              loadedScore &&
              <FullScreen className="marginPlay" handle={handle}>
                <div id="game" className="marginPlay"
                     dangerouslySetInnerHTML={createMarkup()} />
                     {
                       // !isMobile &&
                       // <center>
                       //   <input type="submit"
                       //     className="button-blue"
                       //     value="Activar FullScreen"
                       //     onClick={handle.enter}
                       //   />
                       // </center>
                     }
               </FullScreen>
            }
            {
              // open &&
              // <Quizz/>
            }
            {
              openVideo &&
              <VideoModal
                  isVisibleModal={openVideo}
                  onCloseModal={()=>setOpenVideo(false)}
                  auth={auth}
                  level={level}
                  texts={texts[level]}
                  numberVideo={"/videos/"+(level)+".mp4"}
              />
            }
            {
              openQuiz &&
              <QuizModal
                isVisibleQuizModal={openQuiz}
                onCloseModal={()=>checkQuizFinish()}
                level={level}
                userId={auth.user.id}
              />
            }

            {
              openTop &&
              <TopModal
                isVisibleTopModal={openTop}
                onCloseModal={()=>setOpenTop(false)}
                level={level}
                userId={auth.user.id}
                score={score}
              />
            }
            {
              openFinish &&
              <FinishModal
                isVisibleModal={true}
                onCloseModal={()=>setOpenFinish(false)}
                level={level}
                userId={auth.user.id}
                score={score}
              />
            }
      </div>)
}

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(
    mapStateToProps
    // ,
    // {
    //   // getProjects
    //   // getMalls
    //  }
  )(Play);
// export default Play
