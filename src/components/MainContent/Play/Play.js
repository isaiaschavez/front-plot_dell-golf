/**
 * @Author: memo
 * @Date:   2020-10-04T20:46:26-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-10-26T14:31:28-06:00
 */



import React, { Component,  useState, useRef } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { FullScreen, useFullScreenHandle } from "react-full-screen";
const handle = useFullScreenHandle();
import VideoModal from './VideoModal';

class Play extends Component {
  constructor() {
    super();
    this.state = {
      open:false
    };

    window.addEventListener("message", function (event) {
      console.log("receiving:", event)
      console.log("event.action=>", event.action)
      console.log("event.data=>", event.data)

      console.log("event.data.action=>", event.data.action)

      if(event && event.data && event.data.action
        && event.data.action === "ShowQuizzPopup"
      ){
          this.setState({
            isVisibleModal:true
          })
      }
    });
  }

  createMarkup() {
    return {__html: '<iframe src="/gameOrisss/index.html" width: 100, height: 100, ></iframe>'};
  }

    render() {
      var pageContainer = isMobile? "page-mobile-content":"page-content"

      return(<div className={pageContainer}>
        {
          <FullScreen handle={handle}>
              <div id="game" style={{ width: '100%', height: '100%', display: 'row'}}
                 dangerouslySetInnerHTML={this.createMarkup()} />
           </FullScreen>
        }

        {
          this.state.isVisibleModal &&
          <VideoModal
              isVisibleModal={this.state.isVisibleModal}
              onCloseModal={()=>{
                this.setState({isVisibleModal:false})
              }}
          />
        }
      <button onClick={handle.enter}>
        First
      </button>

      </div>)
    }

}

Play.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  // projects: state.projects,
  // malls: state.malls
});

export default connect(
  mapStateToProps,
  {
    //getAllGamesScore
  }
)(Play);
