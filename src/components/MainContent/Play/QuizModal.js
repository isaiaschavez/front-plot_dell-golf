/**
 * @Author: memo
 * @Date:   2020-10-26T21:03:43-06:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-01T17:37:36-06:00
 */
 import React, { Component,  useState, useRef, useEffect } from "react";
 import {isMobile} from 'react-device-detect';
 import "./../../styles/modal.scss";
 import config from './../../../config/data.js'

 import Combobox from 'react-widgets/lib/Combobox'
 const quizzzes = require("./quizz01.json")
 const OK_ANSWER=100
 const OK_ANSWER_X_3=100
 const BONUS_3OK=50
 const quizzes = []
 // quizzes[0]={
 //               question:"¿Qué aspectos son clave al planear la transición al trabajo remoto?:",
 //               responses:[
 //                           "Hardware y seguridad",
 //                           "Hardware, software, seguridad y administración",
 //                           "Conectividad e infraestructura de red"
 //                         ],
 //               correct:1
 //            }

 class QuizModal extends Component {
    constructor(props) {
     super(props);
      console.log("this.props.level:" + this.props.level)
      console.log("quizzzes.questions[this.props.level]:" +  quizzzes.questions[""+this.props.level][0])
      // quizzes[this.props.level] = quizzzes.questions[""+this.props.level]
      var data = quizzzes.questions[""+this.props.level][0]
      quizzes[this.props.level] = {}
      quizzes[this.props.level].question = data.question
      quizzes[this.props.level].responses = data.responses
      quizzes[this.props.level].correct = data.correct

      this.state = {
          questionSelected:null,
          isAnswered:false,
          currentIndex:0,
          quiz:quizzes[this.props.level],
          isOver:false,
          score:0,
          counterOK:0,
          quizScore:0
      }

    }

    continueAction(){
      // alert("continueAction")
      console.log("this.state.currentIndex:" + this.state.currentIndex)
      if(this.state.currentIndex<2){
        var data = quizzzes.questions[""+this.props.level][this.state.currentIndex+1]
        console.log("data:" + JSON.stringify(data))
        quizzes[this.props.level] = {}
        quizzes[this.props.level].question = data.question
        quizzes[this.props.level].responses = data.responses
        quizzes[this.props.level].correct = data.correct

        document.getElementById("responseImgA").src="/images/null.png"
        document.getElementById("responseImgB").src="/images/null.png"
        document.getElementById("responseImgC").src="/images/null.png"

        document.getElementById('questionA').className = "responses"
        document.getElementById('questionB').className = "responses"
        document.getElementById('questionC').className = "responses"

        document.getElementById('circleA').className = "dot"
        document.getElementById('circleB').className = "dot"
        document.getElementById('circleC').className = "dot"


        this.setState({
          quiz:quizzes[this.props.level],
          currentIndex:this.state.currentIndex+1,
          isAnswered:false,
          questionSelected:null
        })
      }else{

        var score = this.state.counterOK * OK_ANSWER

        var quizScore = score
        if(this.state.counterOK === 3){
            quizScore = quizScore + BONUS_3OK
        }

        // var url = 'http://192.168.1.76:5556/api/utilities/scoreQuiz/'
        const url =  config.protocol+config.host
               +config.port+config.setScoreQuiz


        fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            headers: {
              'Content-Type': 'application/json'
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify({
                    userId:this.props.userId,
                    level:this.props.level,
                    score:quizScore
            }) // body data type must match "Content-Type" header
        });

        //

        this.setState({
          isOver:true,
          scoreFinal:quizScore
        })
      }
    }


    responseAction(){
      if(this.state.questionSelected === this.state.quiz.correct){
        // alert("OK")
          switch (this.state.questionSelected) {

            case 0:
                  document.getElementById("responseImgA").src="/images/palomita_blanca.png"
              break;

            case 1:
                  document.getElementById("responseImgB").src="/images/palomita_blanca.png"
              break;

              case 2:
                    document.getElementById("responseImgC").src="/images/palomita_blanca.png"
                break;
            default:

          }

          this.setState({
            counterOK:this.state.counterOK + 1
          })
      }else{
        // alert("BAD")

        switch (this.state.questionSelected) {
          case 0:
                document.getElementById("responseImgA").src="/images/tache_blanco.png"
            break;

          case 1:
                document.getElementById("responseImgB").src="/images/tache_blanco.png"
            break;

            case 2:
                  document.getElementById("responseImgC").src="/images/tache_blanco.png"
              break;
          default:
        }

        switch (this.state.quiz.correct) {
          case 0:
                document.getElementById("responseImgA").src="/images/palomita_azul.png"
            break;

          case 1:
                document.getElementById("responseImgB").src="/images/palomita_azul.png"
            break;

            case 2:
                  document.getElementById("responseImgC").src="/images/palomita_azul.png"
              break;
          default:
        }
      }

      this.setState({
        isAnswered:true
      })
    }

    exitAction(){
      this.props.onCloseModal()
    }

    renderQuiz(){
      var imageTop=""
      switch (this.props.level) {
        case 0:
            imageTop="/images/avataaars1.png"
          break;

        default:

      }

      return(
        <div className={this.props.isVisibleQuizModal ?
                          "modal display-block" : "modal display-none"}>

              <div className="modal-content-quiz">

              <center>
              <>
                <div className="topImage">
                    <img src={imageTop} width="50%"/>
                </div>

                <div className="questionTitle">
                    {
                      // quizzes[this.props.level].question
                    }
                    {
                      this.state.quiz.question
                    }
                </div>

                <div  className="rowPlay" onClick={()=>{
                                            document.getElementById('questionA')
                                                .className = "responsesA"
                                                document.getElementById('questionB')
                                                    .className = "responses"
                                                    document.getElementById('questionC')
                                                        .className = "responses"

                                                        document.getElementById('circleA')
                                                            .className = "dotA"
                                                            document.getElementById('circleB')
                                                                .className = "dot"
                                                                document.getElementById('circleC')
                                                                    .className = "dot"
                                            this.setState({
                                                questionSelected:0
                                            })
                                            }}>
                  <div id="circleA" className="dot">
                      <label className="letterInCircle">
                          A
                      </label>
                  </div>
                  <div id="questionA" className="responses">
                        <div className="responsesSection1">
                          <label>
                              {this.state.quiz.responses[0]}
                          </label>
                        </div>

                        <div id="responsesSection2A" className="responsesSection2A">
                            <img id="responseImgA" width="80%" src="/images/null.png" />
                        </div>
                  </div>
                </div>



                <div className="rowPlay" onClick={()=>{

                                          document.getElementById('questionA')
                                              .className = "responses"
                                              document.getElementById('questionB')
                                                  .className = "responsesA"
                                                  document.getElementById('questionC')
                                                      .className = "responses"

                                                      document.getElementById('circleA')
                                                          .className = "dot"
                                                          document.getElementById('circleB')
                                                              .className = "dotA"
                                                              document.getElementById('circleC')
                                                                  .className = "dot"

                                                  this.setState({
                                                      questionSelected:1
                                                  })
                                            }}>
                  <div  id="circleB" className="dot">
                      <label  className="letterInCircle">
                          B
                      </label>
                  </div>

                  <div id="questionB" className="responses">
                        <div className="responsesSection1">
                          <label>
                              {this.state.quiz.responses[1]}
                          </label>
                        </div>

                        <div id="responsesSection2B" className="responsesSection2B">
                            <img  id="responseImgB" width="80%" src="/images/null.png" />
                        </div>
                  </div>

                </div>



                <div className="rowPlay" onClick={()=>{

                                          document.getElementById('questionA')
                                              .className = "responses"
                                              document.getElementById('questionB')
                                                  .className = "responses"
                                                  document.getElementById('questionC')
                                                      .className = "responsesA"

                                                      document.getElementById('circleA')
                                                          .className = "dot"
                                                          document.getElementById('circleB')
                                                              .className = "dot"
                                                              document.getElementById('circleC')
                                                                  .className = "dotA"

                                                  this.setState({
                                                      questionSelected:2
                                                  })
                                            }}>
                  <div id="circleC" className="dot">
                      <label  className="letterInCircle">
                          C
                      </label>
                  </div>

                  <div id="questionC" className="responses">
                        <div className="responsesSection1">
                          <label>
                              {this.state.quiz.responses[2]}
                          </label>
                        </div>

                        <div id="responsesSection2C" className="responsesSection2C">
                            <img id="responseImgC" width="80%" src="/images/null.png" />
                        </div>
                  </div>

                </div>
                  {
                    this.state.questionSelected !== null &&
                    this.state.isAnswered === false &&
                    <>
                      <div>
                        <button className="send-button-welcome" onClick={this.responseAction.bind(this)}>
                            Responder
                        </button>
                      </div>
                    </>
                  }

                  {
                    this.state.isAnswered === true &&
                    <>
                      <div>
                        <button className="send-button-welcome" onClick={this.continueAction.bind(this)}>
                            Continuar
                        </button>
                      </div>
                    </>
                  }
              </>
              </center>
              </div>
        </div>
      )
    }


    renderScore(){

      var imageTop=""
      switch (this.props.level) {
        case 0:
            imageTop="/images/avataaars1.png"
          break;

        default:

      }

      return (<div className={this.props.isVisibleQuizModal ?
                                "modal display-block" : "modal display-none"}>

                    <div className="modal-content-quiz">

                    <center>
                    <>
                      <div className="topImage">
                          <img src={imageTop} width="50%"/>
                      </div>

                      <div className="scoreTitle">
                        <label>Gracias por tus respuestas</label>
                      </div>


                      <div className="scoreText01">
                        <label>Tus puntos obtenidos al responder los quizzes son los siguientes:</label>
                      </div>

                      <button className="points-button-quiz">
                        <label>
                          {this.state.scoreFinal}
                        </label>
                      </button>

                      <div className="scoreText02">
                        <label>Nota: Éstos se verán reflejados en tu score final al terminar el juego.</label>
                      </div>

                      <div>
                        <button className="button-exit-quiz" onClick={this.exitAction.bind(this)}>
                            Continuar
                        </button>
                      </div>

                    </>
                    </center>
                  </div>
              </div>)
    }

    render(){
      if (this.state.isOver) {
          return this.renderScore()
      }else{
          return this.renderQuiz()
      }
    }
  }


 export default QuizModal;
