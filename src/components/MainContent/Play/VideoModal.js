/**
 * @Author: memo
 * @Date:   2020-10-25T19:01:43-06:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-18T14:00:58-06:00
 */
 import React, { Component,  useState, useRef, useEffect } from "react";
 import {isMobile} from 'react-device-detect';
 import "./../../styles/modal.scss";
 import Combobox from 'react-widgets/lib/Combobox'
import { withOrientationChange } from 'react-device-detect'

 class VideoModal extends Component {
   constructor(props) {
     super(props);
      this.state = {
        isLoadedVideo:false,
        isVisibleStep:false,
      }
    }

    playVideo(){
        this.setState({isLoadedVideo:true})
    }

    onCloseModal(){
      if(this.props.level == 1 && this.props.onMenu != true){
        this.setState({
          label1 : "Muy bien, ya conociste un poco de lo que Dell ofrece con la tecnología de Work from Home.",
          label2 : "Ahora sigue avanzando en el juego, más adelante encontrarás más información al respecto y quizzes.",
          label3 : "Contéstalos correctamente para aumentar tu score.",
          isVisibleStep:true,
          isLoadedVideo:false
        })
      }else{
        this.setState({
          isLoadedVideo:false
        },()=>{
          this.props.onCloseModal()
        })
      }
    }

    close(){

        this.props.onCloseModal()

    }

    closeVideoPopUp(){
      this.setState({ isLoadedVideo:false })
    }

    render(){

      var imageTop=""
      switch (this.props.level) {
        case 0:
        case 1:
        case 3:
            imageTop="/images/avataaars1.png"
          break;
          case 5:
          case 7:

              imageTop="/images/avataaars2.png"
            break;
            case 9:
            case 11:
            case 13:
            case 15:
            case 17:
                imageTop="/images/avataaars3.png"
              break;
        default:

      }

      const { isLandscape, isPortrait } = this.props
      // console.log("this.props.isLandscape:", this.props.isLandscape)
      // console.log("this.props.isPortrait:", this.props.isPortrait)
      // console.log("isMobile:", isMobile)
      // alert("isLandscape:" +  isLandscape)
      var validationScene = isMobile && isLandscape
      // alert("validationScene:" + validationScene)

      var modalType = validationScene ?
          "modal-content-video-mobile":"modal-content-video"
      // alert("modalType:" + modalType)

      // var modalType = "modal-content-video"
      return(
        <div className={this.props.isVisibleModal ?
                          "modal display-block" : "modal display-none"}>
                    {
                      this.state.isVisibleStep &&
                      <div className={modalType}>
                          <center>
                          <>
                            <div className="topImage">
                                <img src={imageTop} width="10%"/>
                            </div>

                            <div>
                              <label>{this.state.label1}</label>
                            </div>

                            <div>
                              <label> {this.state.label2}</label>
                            </div>
                            <div className="titleVideo">
                              <label >{this.state.label3}
                              </label>
                            </div>
                            <div>
                              <button type="submit" className="auth-button"
                                onClick={this.close.bind(this)}
                              >
                                Seguir jugando
                              </button>
                            </div>
                        </>
                      </center>
                  </div>
                    }
                    { !this.state.isLoadedVideo &&
                      !this.state.isVisibleStep &&
                      <div className={modalType}>
                          <center>
                    <>
                      <div className="topImage">
                          <img src={imageTop} width="50%"/>
                      </div>
                      {
                        !isMobile &&
                        <div className="fixSpace">
                          .
                        </div>
                      }
                      <div>
                        {
                          this.props.level === 1 &&
                          <label
                            className={isMobile?"text-video-mobile":"text-video"}
                          >Hola {this.props.auth?this.props.auth.user.name:""}
                                , bienvenido/a.  </label>

                        }
                        {
                          this.props.level !== 0 &&
                          <label
                            className={isMobile?"text-video-mobile":"text-video"}
                          >{this.props.texts.label1}</label>
                        }
                      </div>

                      <div className={isMobile?"text-video-mobile":"text-video"} >
                        <label> {this.props.texts.label2} </label>
                      </div>

                      <div>
                        <label className={isMobile?"text-video-mobile":"text-video"}>
                            {this.props.texts.label3}
                        </label>
                      </div>

                      <div className="boxVideo">
                            <label className={isMobile?"textBoxVideo-mobile":"textBoxVideo"}>
                              Ver video   </label>
                            <img className="playButton" src="/images/boton_play.png"
                                width={isMobile?"10%":"10%"}

                                 onClick={this.playVideo.bind(this)}/>
                      </div>
                      </>
                      </center>
                  </div>
                    }

                    {
                      this.state.isLoadedVideo &&
                      <div className="playVideoPopUp">
                        <span className="close-white" onClick={this.onCloseModal.bind(this)}>&times;</span>
                        <video id="video" autoPlay controls className="videoAdd">
                          <source  src={this.props.numberVideo}
                              type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
                        </video>
                      </div>
                    }

        </div>
      )
    }
  }


 export default withOrientationChange(VideoModal);
