/**
 * @Author: memo
 * @Date:   2020-10-29T20:24:08-06:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-03T23:44:39-06:00
 */
 import React, { Component,  useState, useRef, useEffect } from "react";
 import {isMobile} from 'react-device-detect';
 import { connect } from "react-redux";
 import Avatar from 'avataaars'
 import config from './../../../config/data.js'
 import "./../../styles/modal.scss";
 import { getAvatar, getScoreTotal } from "../../../actions/authActions";

 import Combobox from 'react-widgets/lib/Combobox'

 class FinishModal extends Component {
   constructor(props) {
     super(props);
      this.state = {
        currentState:0
      }

   }

   componentDidUpdate(prevProps) {
     if(
       this.props.score
           && prevProps.score
           && this.props.score != prevProps.score
     ){
       console.log("in")
         this.setState({
           scoreTotal:this.props.score.score.scoreTotal
         })
     }
   }

   sendSurvey(){
     // $('input:radio[name=response01]:checked').val(),
     const url =  config.protocol+config.host
            +config.port+config.setSuvey

     // var url = 'http://192.168.1.76:5556/api/utilities/survey'
     var response05=""
     if(this.state.response05_1)
       response05 = response05 + " '" +this.state.response05_1 + "'"
     if(this.state.response05_2)
       response05 = response05 + " '" +this.state.response05_2 + "'"
     if(this.state.response05_3)
         response05 = response05 + " '" +this.state.response05_3 + "'"
     if(this.state.response05_4)
        response05 = response05 + " '" +this.state.response05_4 + "'"


     var data = {
             user:this.props.userId,
             response01:this.state.response01,
             response02:this.state.response02,
             response03:this.state.response03,
             response04:this.state.response04,
             response04_1:this.state.response4_1,
             response05,
             response06:this.state.response06,

             response06_1:this.state.response6_1,
     }

     console.log("data:", data)
     console.log("url:", url)
     fetch(url, {
         method: 'POST', // *GET, POST, PUT, DELETE, etc.
         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
         headers: {
           'Content-Type': 'application/json'
           // 'Content-Type': 'application/x-www-form-urlencoded',
         },
         redirect: 'follow', // manual, *follow, error
         body: JSON.stringify(data) // body data type must match "Content-Type" header
     });
   }

   responseAction(){
     if(this.state.currentState===1){
       this.sendSurvey()
     }
     this.setState({
       currentState : this.state.currentState + 1
     })
   }

   setResponse1(event){
     console.log(event.target.value);
     this.setState({
       response01:event.target.value
     })
   }

   setResponse2(event){
     console.log(event.target.value);
     this.setState({
       response02:event.target.value
     })
   }

   setResponse3(event){
     console.log(event.target.value);
     this.setState({
       response03:event.target.value
     })
   }

   setResponse4(event){
     console.log(event.target.value);
     this.setState({
       response04:event.target.value
     })
   }

   setResponse5(event){
     console.log(event.target.value);
     this.setState({
       response05:event.target.value
     })
   }

   setResponse6(event){
     console.log(event.target.value);
     this.setState({
       response06:event.target.value
     })
   }

   setResponse51(event){
     console.log(event.target.value);
     console.log("response05_1:", this.state.response05_1)


     if(!this.state.response05_1){
       this.setState({
         response05_1:event.target.value
       })
     }else{
       this.setState({
         response05_1:null
       })
       document.getElementById('radios2Survey1').checked = false
    }
   }


   setResponse51(event){
     console.log(event.target.value);
     console.log("response05_1:", this.state.response05_1)


     if(!this.state.response05_1){
       this.setState({
         response05_1:event.target.value
       })
     }else{
       this.setState({
         response05_1:null
       })
       document.getElementById('radios2Survey1').checked = false
    }
   }


   setResponse52(event){
     console.log(event.target.value);
     console.log("response05_2:", this.state.response05_2)


     if(!this.state.response05_2){
       this.setState({
         response05_2:event.target.value
       })
     }else{
       this.setState({
         response05_2:null
       })
       document.getElementById('radios2Survey2').checked = false
    }
   }


   setResponse53(event){
     console.log(event.target.value);
     console.log("response05_3:", this.state.response05_3)


     if(!this.state.response05_3){
       this.setState({
         response05_3:event.target.value
       })
     }else{
       this.setState({
         response05_3:null
       })
       document.getElementById('radios2Survey3').checked = false
    }
   }


   setResponse54(event){
     console.log(event.target.value);
     console.log("response05_4:", this.state.response05_4)


     if(!this.state.response05_4){
       this.setState({
         response05_4:event.target.value
       })
     }else{
       this.setState({
         response05_4:null
       })
       document.getElementById('radios2Survey4').checked = false
    }
   }

   setResponse4_1(event){
     console.log("setResponse4_1:"+event.target.value);
     this.setState({
       response4_1:event.target.value
     })
   }

   setResponse6_1(event){
     console.log(event.target.value);
     this.setState({
       response6_1:event.target.value
     })
   }

   renderSurvey(){
      return(<div className={this.props.isVisibleModal ?
                          "modal display-block" : "modal display-none"}>
          <div className="modal-content-survey">
          <scrollbar>
            <center>
              <div className="containerMessage">
                  <div className="titleSurvey">
                      Queremos saber tu opinión
                  </div>

                  <div className="questionSurvey">
                    1. ¿Qué te pareció el torneo Virtual de Golf?
                  </div>
                  <div className="radios1q" onChange={this.setResponse1.bind(this)}>
                      <div className="radio1q" >No me gustó</div>
                      1<input className="radio1q" type="radio" id="male" name="response01" value="1"/>
                      2<input className="radio1q" type="radio" id="male" name="response01" value="2"/>
                      3<input className="radio1q" type="radio" id="male" name="response01" value="3"/>
                      4<input className="radio1q" type="radio" id="male" name="response01" value="4"/>
                      5<input className="radio1q" type="radio" id="male" name="response01" value="5"/>
                      <div className="radio1q">Me encantó</div>
                  </div>


                  <div className="questionSurvey">
                    2. ¿Qué áreas de oportunidad encuentras dentro de la dinámica?
                  </div>
                  <div className="textAreaSurvey">
                    <textarea rows="5" cols="90" placeholder="Comentarios"
                      onChange={this.setResponse2.bind(this)}>
                    </textarea>
                  </div>


                  <div className="questionSurvey">
                    3. ¿Estás en un punto de renovación digital dentro de tu empresa?
                  </div>
                  <div className="questionSurvey" >
                    ¿De qué producto?
                  </div>
                  <div className="textAreaSurvey">
                    <textarea rows="5" cols="90" placeholder="Comentarios"
                        onChange={this.setResponse3.bind(this)}>
                    </textarea>
                  </div>


                  <div className="questionSurvey">
                      4. ¿Consideras de valor la información que recibiste dentro del juego?
                  </div>
                  <div className="radios1q" >
                      <div className="radio1q" >Si, es de valor.</div>
                        <input className="radio1q" type="radio" id="response04_1_1" name="response04_1" value="Si, es de valor." onClick={this.setResponse4_1.bind(this)}/>
                        <input className="radio1q" type="radio" id="response04_1_2" name="response04_1" value="No, no es de valor" onClick={this.setResponse4_1.bind(this)}/>
                      <div className="radio1q">No, no es de valor</div>
                  </div>

                  <div className="textAreaSurvey">
                    <textarea rows="5" cols="90" placeholder="Comentarios"
                      onChange={this.setResponse4.bind(this)}>
                    </textarea>
                  </div>

                  <div className="questionSurvey">
                      5. ¿Cuál de nuestras soluciones te puede apoyar dentro de tu transformación digital? (Puedes seleccionar más de una)
                  </div>
                  <div className="radios2Survey"  >
                    <div className="radios2SurveyItem">
                      <input className="radio1q" type="radio" id="radios2Survey1" name="radios2Survey1"
                             onClick={this.setResponse51.bind(this)}
                       value="Cyber Recovery"/>Cyber Recovery
                    </div>

                    <div className="radios2SurveyItem">
                      <input className="radio1q" type="radio" id="radios2Survey2" name="radios2Survey2"
                           onClick={this.setResponse52.bind(this)}
                          value="DFS"/>DFS</div>


                    <div className="radios2SurveyItem">
                        <input className="radio1q" type="radio" id="radios2Survey3" name="radios2Survey3"
                          onClick={this.setResponse53.bind(this)}
                          value="WFH"/>WFH</div>
                    <div className="radios2SurveyItem">
                        <input className="radio1q" type="radio" id="radios2Survey4" name="radios2Survey4"
                          onClick={this.setResponse54.bind(this)}
                          value="Power Store"/>Power Store</div>
                  </div>


                  <div className="questionSurvey">
                      6. Si ya tuviste una cita con tu asesor, ¿Cómo calificas la experiencia?
                  </div>
                  <div className="radios1q" onChange={this.setResponse6_1.bind(this)}>
                      <div className="radio1q" >No he tenido mi cita.</div>
                      <input className="radio1q" type="radio" id="response06_1" name="response06_1" value="No he tenido mi cita."/>
                      <input className="radio1q" type="radio" id="response06_2" name="response06_1" value="Sí, ya tuve mi cita."/>
                      <div className="radio1q" >Si, ya tuve mi cita.</div>

                  </div>
                  {
                    document.getElementById('response06_2')&&
                    document.getElementById('response06_2').checked === true &&
                    <div className="radios1q" onChange={this.setResponse6.bind(this)}>
                        <div className="radio1q">No me gustó</div>
                        1<input className="radio1q" type="radio" id="male" name="asesor1" value="1"/>
                        2<input className="radio1q" type="radio" id="male" name="asesor1" value="2"/>
                        3<input className="radio1q" type="radio" id="male" name="asesor1" value="3"/>
                        4<input className="radio1q" type="radio" id="male" name="asesor1" value="4"/>
                        5<input className="radio1q" type="radio" id="male" name="asesor1" value="5"/>
                        <div className="radio1q">Me encantó</div>
                    </div>
                  }
                    <button className="button-finish-survey" onClick={()=>{this.responseAction()}}>
                        Responder
                    </button>

              </div>
            </center>
            </scrollbar>
        </div>
      </div>
      )
   }

   renderMessage(){
      return (<div className={this.props.isVisibleModal ?
                          "modal display-block" : "modal display-none"}>
              <div className="modal-content-exit-survey">
                <center>
                <div className="containerMessage-exit01">
                    <label>
                    !Felicidades!
                    </label>
                    <label>
                      Has completado el juego
                    </label>

                    <div>
                      <button className="button-next-finish" onClick={()=>{this.responseAction()}}>
                          Continuar
                      </button>
                    </div>

                  </div>


                </center>
              </div>
              </div>)
   }

   renderMessageExit(){

      return (<div className={this.props.isVisibleModal ?
                          "modal display-block" : "modal display-none"}>
              <div className="modal-content-exit-survey">
              <span className="close-white-exit-survey" onClick={()=>{
                  window.location.href = "/Scheduler"
                }}>&times;</span>

                <center>
                <div className="containerExitSurvey">
                    <div className="containerExitSurvey">
                      <label >
                      !Felicidades!
                      </label>
                    </div>
                    <label>
                    Has completado el juego y lo has hecho increíble.
                    </label>
                    <label className="questionSurvey">
                      Tu score final
                    </label>
                    <button className="points-button-exit-survey">
                      {
                        this.props.auth.score &&
                        <label>
                          {this.props.auth.score.scoreTotal} puntos
                        </label>
                      }
                    </button>
                    <button className="button-next-finish" onClick={()=>{
                        window.location.href = "/Scheduler"
                      }}>
                        Agendar
                    </button>

                    <label className="note-survey">
                      Nota: Recibirás un mail con la liga para la premiación, la cual se llevará a cabo online.
                      También no olvides agendar tu cita de seguimiento con uno de nuestro asesores
                    </label>
                </div>
              </center>
            </div>
          </div>)
   }


   render(){

     if(this.state.currentState === 2 && this.props.auth.score === undefined){
       this.props.getScoreTotal(this.props.auth.user.id)
     }
      return (
        <>
        {
          this.state.currentState === 0 &&
          <>
           {this.renderMessage()}
          </>
        }
        {
          this.state.currentState === 1 &&
          <>
           {this.renderSurvey()}
          </>
        }
        {
          this.state.currentState === 2 &&
          this.props.auth &&
          this.props.auth.score &&
          this.props.auth.score.scoreTotal !== null &&
          <>
           {this.renderMessageExit()}
          </>
        }
        </>
      )
   }
 }

 const mapStateToProps = state => ({
   utils : state.utils,
   auth  : state.auth
 });
 export default connect(
   mapStateToProps,
   {
     getAvatar,
     getScoreTotal
   }
 )(FinishModal);
 // export default FinishModal;
