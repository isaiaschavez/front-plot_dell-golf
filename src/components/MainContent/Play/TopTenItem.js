/**
 * @Author: memo
 * @Date:   2020-10-29T10:59:41-06:00
 * @Last modified by:   memo
 * @Last modified time: 2020-10-29T12:55:37-06:00
 */
 import React, { Component } from 'react';

 export default class TopTenItem extends Component {

   render() {
     return(
       <div className="topTenItem">
          <div className="topTenItemA">
              {this.props.number}
          </div>
          <div className="topTenItemB">
              <div className="colorA">
                {this.props.name}:
              </div>

              <div className="colorB">
                {this.props.points} puntos
              </div>
          </div>
       </div>
     )
   }
 }
