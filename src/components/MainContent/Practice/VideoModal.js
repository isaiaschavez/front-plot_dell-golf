/**
 * @Author: memo
 * @Date:   2020-10-25T19:01:43-06:00
 * @Last modified by:   memo
 * @Last modified time: 2020-10-25T19:04:58-06:00
 */
 import React, { Component,  useState, useRef, useEffect } from "react";
 import {isMobile} from 'react-device-detect';
 import "./../../styles/modal.scss";
 import Combobox from 'react-widgets/lib/Combobox'


 class VideoModal extends Component {
   constructor(props) {
     super(props);
      this.state = {

      }
    }

    render(){
      return(
        <div className={this.props.isVisibleModal ? "modal display-block" : "modal display-none"}>
          <div className="modal-content">
            <center>
                <span className="close" onClick={this.props.onCloseModal}>&times;</span>

            </center>
          </div>
        </div>
      )
    }
  }


 export default VideoModal;
