/**
 * @Author: memo
 * @Date:   2020-10-07T21:51:02-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-12T08:30:18-06:00
 */
import React, { Component,  useState, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import {isMobile} from 'react-device-detect';
import VideoModal from './VideoModal';

import "./Practice.scss";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter
} from "react-router-dom";
function Practice ({auth}){
  const [open, setOpen] = useState(false)
  const [fullscreen, setFullscreen] = useState(false)

  useEffect(() => {
    window.addEventListener("message", function (event) {
      console.log("receiving:", event)
      console.log("event.action=>", event.action)
      console.log("event.data=>", event.data)

      console.log("event.data.action=>", event.data.action)

      if(event && event.data && event.data.action
        && event.data.action === "ShowQuizzPopup"
      ){
          setOpen(true)
      }
    });
  });

  // window.addEventListener('message', handleMessage, false);
  // function handleMessage(e) {
  //   // Get reference to display div
  //   alert(e)
  // }

  const handle = useFullScreenHandle();
  const pageContainer = isMobile? "page-content-mobile-practice":"page-content"

  function createMarkup() {
    var url = "<center><iframe src='/gamePractice/index.html?userId="+auth.user.id+"' width: 100, height: 100, ></iframe></center>'"
    return {__html: url};
  }

  return(<div class={pageContainer}>
            {
              // !open &&
              <FullScreen handle={handle}>
                <div id="game"
                     dangerouslySetInnerHTML={createMarkup()} />
                     {
                       // !isMobile && !fullscreen &&
                       // <center>
                       //   <input type="submit"
                       //     className="button-blue"
                       //     value="Activar FullScreen"
                       //     onClick={()=>{
                       //       handle.enter()
                       //       setFullscreen(true)
                       //     }}
                       //   />
                       // </center>
                     }
               </FullScreen>
            }
            {
              open &&
              <VideoModal
                  isVisibleModal={true}
                  onCloseModal={()=>setOpen(false)}
              />
            }
      </div>)
}

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(
    mapStateToProps
    // ,
    // {
    //   // getProjects
    //   // getMalls
    //  }
  )(Practice);
// export default Practice
