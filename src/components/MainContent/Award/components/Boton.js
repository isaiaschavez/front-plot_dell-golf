import React from 'react';
import styled from 'styled-components'
import GlobalFonts from '../global';

const BotonStyle = styled.button`
  font-family:Roboto, arial;
  flex:1;
  text-align:center;
  opacity:${props => props.disabled ? "0.50" : "1"};
  margin-top: 1rem;
   color: #ffffff;
   background-color: #41B6E6;
   border-color: #41B6E6;
   border-radius: 7pt;
   width: 80%;
   max-height:3rem;
   padding: 0.8em 0;
   margin-bottom: 0.3rem;
   margin-left:auto;
   margin-right:auto;
   font-size: 1rem;
   font-family: $code;
   text-shadow: 1px 1px 0 rgba(0,0,0,0.5);
   transition: all 0.25s ease 0s;
 -webkit-box-shadow: 10px 8px 22px -13px rgba(0,0,0,0.39); 
box-shadow: 10px 8px 22px -13px rgba(0,0,0,0.39);
border-top:solid 1px rgba(0,0,0,0.5);
border-left:solid 1px rgba(0,0,0,0.5);
-webkit-box-shadow: inset 8px 12px 14px -14px rgba(0,0,0,0.27);
-moz-box-shadow: inset 8px 12px 14px -14px rgba(0,0,0,0.27);
box-shadow: inset 8px 12px 14px -14px rgba(0,0,0,0.27);
   
   &:hover {
     background-color: ${props => props.disabled ? "#41B6E6":"#0076CE"};  ;
     border-color: #0076CE;
     transform: ${props => props.disabled ? "none":"translateY(-2.5px)"}; 
     cursor: pointer;
     transition: all 0.25s ease 0s;
    };
    @media (min-width: 768px) {
      width: 100%;
      padding:0.6rem 0;
       font-size: 1.1em;
   }
` 
const Boton = ({buttonFunction,message,disabled}) => {
 return (
 <>
  <GlobalFonts/>
  <BotonStyle disabled={disabled} onClick={buttonFunction}>
         {message}
  </BotonStyle>
 </>
 );
}
 
export default Boton;