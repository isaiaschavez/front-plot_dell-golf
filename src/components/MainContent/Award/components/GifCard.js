import React from 'react';
import styled from 'styled-components'
import GlobalFonts from '../global';

const GiftImage = styled.img`
  transform-style: preserve-3d;
   transform: ${props => props.selected ? "translate3d(0px, -13%, 18em) scale(1.2);" : "none"};
   transition:0.09s;
   min-width:5rem;
   width:70%;
`;
const GiftCard = styled.div`
flex-grow:1;
flex-shrink:1;
  transition:0.09s;
  transform-style: preserve-3d;
  background: ${props => props.selected ? "#41B6E6" : "transparent"};
  transform: ${props => props.selected ? "scale(0.95)" : "none"};
  -webkit-box-shadow:  ${props => props.selected ? "none" : " 5px 7px 22px -13px rgba(0,0,0,0.23);" }; 
  box-shadow:  ${props => props.selected ? "none" : " 5px 7px 22px -13px rgba(0,0,0,0.23);"}; 
  position:relative;
  border: 1px solid  #41B6E6;
   max-width:15rem;
   width: 50%;
   padding:2rem 0;
   margin: 0 0.5rem;
   border-radius:6pt;
   color:white;
   display:flex;
   margin-left: 0.22rem;
   flex-direction:column;
   align-items:center;
   justify-content:space-around;
   font-family: $code;
   cursor: pointer; 
    max-height:10rem;
   
   @media( min-width: 768px){
     height:12rem;
    max-height:12rem;
   }
   @media( min-width: 1200px){
    height:14rem;
    max-height:14rem;
    margin: 0 0.5rem;
   }
`;
const ButtonCheck = styled.img`
  transform: ${props => props.selected ? "none": "translate3d(0px, -13%, 18em) scale(1.4);" };
  opacity: ${props => props.selected ? "1":"0"  };
   transition:0.14s;
   width:1.7rem;
   position:absolute;
   top:0.6rem;
   right:0.6rem;
  @media( min-width: 1200px){
     height:1.9rem;
     width:1.9rem;
   }
`;
const GifText = styled.p`
  font-size:0.8rem;
  text-shadow: 1px 1px 0 rgba(0,0,0,0.2);
   margin-bottom:1rem;
   font-family:Roboto, arial;
      -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none; 
    margin-top:1rem;
    @media( min-width: 768px){
  font-size:1rem;
   }
`;



const GifCard = ({data,selected,onPushCard}) => {
 const { name, urlImage,id} = data
    
 return (  
  <GiftCard selected={selected} onClick={()=>{onPushCard(id) }}>
    <GlobalFonts/>
   {  <ButtonCheck selected={selected} src="/images/ok.png" alt=""/>}
   <GiftImage selected={selected} src={urlImage} alt={ name}/>
   <GifText className="mt-1"> {name} </GifText>
  </GiftCard>
 );
}
 
export default GifCard;