import React from "react";
import { Route, Redirect } from "react-router-dom";
import Boton from './Boton'
import styled from 'styled-components'

import "./Modal.scss";

const ButtonFooter = styled.div`
  margin-top:1rem;
  width:100%;
` 

const ModalAward = ( {buttonFunction, onCloseModal, message, message2 ,buttonMessage="Agendar cita"} ) => {
  
return(
        <div className={"modal display-block"}>
          <div className="modal-content-award">
            <div className="p-relative expand center-flex flex flex-column " style={{  padding: "3rem"}}>

            <span className="modal-close-award" onClick={onCloseModal}>&times;</span>
            <p className="modal-text-award">{message}</p>
            <p className="modal-text-award">{message2}</p>
            {buttonFunction &&
              <ButtonFooter>
                <Boton buttonFunction={buttonFunction} message={buttonMessage}/>
              </ButtonFooter>
             }
            </div>
          </div>
        </div>
      )
}
 
export default ModalAward;