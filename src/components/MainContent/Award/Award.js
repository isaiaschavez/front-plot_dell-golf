/**
 * @Author: memo
 * @Date:   2020-10-18T16:25:20-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-22T22:06:55-06:00
 */
import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import GlobalFonts from './global'

import { connect } from 'react-redux'
import AwardSuccess from './Subviews/AwardSuccess'
import AwardGifSelected from './Subviews/AwardGifSelected'
import AwardUnfinish from './Subviews/AwardUnfinish'
import Modal from './components/Modal'

import {
  getScoreTotal,
  getScoreTop,
  getDatesOfUser,
  getHasAGift
} from '../../../actions/authActions'

import './Award.scss'
const Contenedor = styled.div`
  display: flex;
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  min-height: 100vh;
  right: 0;
  align-items: center;
  margin-top:0.8rem;
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: $bold;
  background: linear-gradient(to bottom, #0076ce, #00447c);

  @media (min-width: 768px) {
    padding-top: 0rem;
    background: transparent;    
  }

`
const BodyContainer = styled.div`
  ${'' /* overflow-y: scroll; */}
  font-family:Roboto, arial;
  border-radius: 20pt;
  display: flex;
  margin-top: 0.5rem;
  box-sizing: border-box;
  height: 100%;
  

  @media (min-height: 1024px) {
    height: 80%;
  }
  @media (min-height: 1440px) {
    height: 65%;
  }

  @media (min-width: 1024px) {
    height: 80%;
  }
  @media (min-width: 2560px) {
    height: 65%;
  }

  overflow-y: scroll;
  flex-direction: column;
  justify-content: center;
  @media (min-width: 768px) {
  overflow-y: hidden;
    background: linear-gradient(to bottom, #0076ce, #00447c);
    -webkit-box-shadow: 8px 12px 75px -27px rgba(0,0,0,0.41);
-moz-box-shadow: 8px 12px 75px -27px rgba(0,0,0,0.41);
box-shadow: 8px 12px 75px -27px rgba(0,0,0,0.41);
border-bottom:solid 2px rgba(0,0,0,0.3);
border-right:solid 3px rgba(0,0,0,0.3);
    min-width: 80%;
    width: 80%;
  }
  @media (min-width: 1024px) {
    min-width: 60%;
    width: 60%;
    margin-top: 2rem;
  }
`

const Award = ({
  utils,
  getScoreTotal,
  getScoreTop,
  getDatesOfUser,
  getHasAGift
}) => {
  //STATES
  const [views, setViews] = useState(initialStateViews())
  const { redeemGifView, selectGifView, giftSendedView } = views


  const [dataUser, setdataUser] = useState({
    idUser: '',
    nameUser: '',
    lastNameUser: '',
    emailUser: '',
    scoreUser: 200,
    hasPoints: false,
    allscores: null,
    hasFinishedTheGame:false
  })
  const {
    idUser,
    nameUser,
    lastNameUser,
    emailUser,
    scoreUser,
    hasPoints,
    allscores,
    hasFinishedTheGame
  } = dataUser

  useEffect(() => {
    const { id, email, name, lastname, giftSended } = utils.user
    //Validación extra

      setdataUser({
        ...dataUser,
        idUser: id,
        emailUser: email,
        nameUser: name,
        lastNameUser: lastname,
        giftSended:false
      })
      // setViews({ ...initialStateViews(), redeemGifView: true })
    // else setViews( { ...initialStateViews(), giftSendedView: true } )
  }, [])

  useEffect(() => {
    if (utils.score) {
      const scoreUser = utils.score.scoreTotal
      const hasFinishedTheGame = false
      const hasPoints = scoreUser > 0 ? true : false
      setdataUser({ ...dataUser, scoreUser, hasPoints,hasFinishedTheGame })
    }
  }, [utils.score])
  
  useEffect(() => {
    if (utils.scoretop) {
      const allscores = utils.scoretop.users
      setdataUser({ ...dataUser, allscores })
    }
  }, [ utils.scoretop ] )
  useEffect( () => {
    if (utils.hasagift) {
      if (utils.hasagift === true) {
        setViews({ ...initialStateViews(),giftSendedView: false,redeemGifView:false })
      }
    }
  }, [utils.hasagift])
  
  
  const seeGifsFunction = () => {
    setViews({ ...initialStateViews(),selectGifView: true,redeemGifView:false })
  }

  const setSendedView = () => {  
    setViews({ ...initialStateViews(),giftSendedView: true,redeemGifView:false })
  }

  return (
    <Contenedor>
      <GlobalFonts />
      <BodyContainer>
        {redeemGifView && (
          <AwardUnfinish 
            buttonFunction={seeGifsFunction}
            hasPoints={hasPoints}
            dataUser={dataUser}
            hasFinishedTheGame={hasFinishedTheGame}
          />
        )}
        {selectGifView && (
          <AwardSuccess
            dataUser={dataUser}
            setSendedView={setSendedView}
          />
        )}
        {giftSendedView && (
          <AwardGifSelected
            message='Tu premio va en camino.'
            message2='¡Gracias por ser parte del torneo!'
          />
        )}
      </BodyContainer>
    </Contenedor>
  )
}

const initialStateViews = () => ({
  redeemGifView: true,
  selectGifView: false,
  giftSendedView: false
})

const mapStateToProps = state => ({
  utils: state.auth
})

export default connect(mapStateToProps, {
  getHasAGift,
  getScoreTotal,
  getScoreTop,
  getDatesOfUser
})(Award)
