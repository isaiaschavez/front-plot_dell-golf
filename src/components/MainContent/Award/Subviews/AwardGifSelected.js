 import React,{useState} from "react";
 import styled from 'styled-components'
import GlobalFonts from '../global';

 const Title = styled.h2`
    font-family:Roboto, arial;
    margin: 2rem;
 `

 const AwardSuccessContainer = styled.div`
  
   flex-direction:column;
   justify-content:start;
   text-align:center;
   margin:0.6rem;
   color:white;
   font-family:Roboto;
`;
const AwardSuccessHeader = styled.div`
font-family:Roboto, arial;
  text-shadow: 1px 1px 0 rgba(0,0,0,0.5);

    line-height:2rem;
    font-size:1.3rem;    
    margin-top:1rem;
`;

const AwardSuccessMesage = styled.div`

    line-height:1.8rem;
    padding:0 2rem;
`;

 const AwardGifSelected = ({message,message2}) => {
   return (
     <AwardSuccessContainer>
     <GlobalFonts/>
       <AwardSuccessHeader>
      <Title>Premios</Title>
      <AwardSuccessMesage >{message}</AwardSuccessMesage>
      <AwardSuccessMesage >{message2}</AwardSuccessMesage>
      </AwardSuccessHeader>
     </AwardSuccessContainer> )
}


 export default AwardGifSelected