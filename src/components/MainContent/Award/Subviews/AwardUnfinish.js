import React,{useState,useEffect} from 'react'
import styled from 'styled-components'
import GlobalFonts from '../global';
import Boton from '../components/Boton'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'

import {
  getScoreTotal,
  getScoreTop,
  getSendMailGift,
  postSendMailGiftInternal,
  getDatesOfUser,
  getHasAGift
} from '../../../../actions/authActions'
import Modal from '../components/Modal'



const ImagenPrincipal = styled.div`
  flex-shrink:1;
  flex-grow:2;
  text-align: center;
  max-width: 75%;
  display:flex;
  justify-content:center;
  aligin-items:center;
  height:25%;
  &>img{
    max-height:100%;
    align-self: center;
    @media (min-height: 1000px) {
    max-height:80%;
    }
  }
  @media (min-width: 720px) {
  height:30%;
    margin-top: 0%;
    flex-grow:4;
    max-width: 23rem;
  }
  @media (min-width: 1200px) {
    width:50%;
    max-width:50%;
  }
  @media (min-width: 1600px) {
    max-width:45%;
    &>img{
    max-width:1200px;
    align-self: center;
    }
  }
  
`
const AwardUnfinishContainer = styled.div`
  height:100%;
  font-family:Roboto, arial;
  color: white;
  display: flex;
  flex-direction: column;
  justify-items: center;
  align-items: center;
  @media ( min-width:720px){
  padding-top: 0rem;
  }
  

`
const AwardUnfinishText = styled.div`
text-shadow: 1px 1px 0 rgba(0,0,0,0.5);
  flex-shrink:3;
  flex-grow:1;  
  height:15%;
  width:auto;
  max-width:100vw;
  padding:0 2rem;
  text-align: center;
  margin-top: 3rem ;
  line-height: 1.4rem;
  padding: 0 0.5rem;
  display:flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  
  text-wrap: break-word;
  @media ( min-width:1200px){
  padding: 0 13rem;
  font-size: 1.2rem;
  }
  @media ( min-width:1600px){
  padding: 0 13rem;
  font-size: 1.6rem;
  line-height: 2rem;
  }
`

const CardFooter = styled.div`
  flex:1;
  display:flex;
  flex-grow:2;
  justify-content:center;
  align-items:start;
  width:80%;
  height:20%;
  padding:0 1rem;
  max-width:100vw;
     @media (min-width: 768px) {
      width: 35%;
   }
   
` 
const AwardUnfinish = ({
  utils,
  buttonFunction,
  dataUser,
  hasFinishedTheGame
}) => {


   let history = useHistory()
  const isFinishedTournament = isTournamentFinished()

  const [modalVisible, setmodalVisible] = useState(false)
  const [hasPointsUser, setHasPointsUser] = useState(null)
  const [hasAppointment, setHasAppointment] = useState(null)
  
  useEffect( () => {
    utils.score && setHasPointsUser(utils.score.scoreTotal > 0)
  }, [utils.score])
  
  useEffect(() => {
      utils.datesuser && setHasAppointment(utils.datesuser.hasADate)
  }, [ utils.datesuser ] )

  const onCloseModal = () => {
    setmodalVisible(false)
  }
  const redirectToGetADate = () => {
    setmodalVisible(false)
    history.push('/Scheduler')
  }
  

  const onAwardButtonPress = ()=>{
    
    buttonFunction()
  } 

  return (
    <AwardUnfinishContainer>
      {modalVisible && <Modal 
          onCloseModal={onCloseModal}
          buttonFunction={redirectToGetADate}
          message={
            '¡Ooops! No has agendado tu cita. Para recibir tu premio recuerda agendar una cita con un asesor.'
          }
        /> }
        <GlobalFonts/>
      <AwardUnfinishText>
        <h1>Premios</h1>
        <p className="mt-1">
         Cuando haya concluido el torneo aquí podras consultar los premios entre
        los cuales podrás elegir. ¡Suerte!
        </p>
      </AwardUnfinishText>
      <ImagenPrincipal>
        <img src="/images/premiosmovil.png" alt=""/>
      </ImagenPrincipal>
       { (
        <CardFooter>
        <Boton buttonFunction={onAwardButtonPress} message={"Canjear premio"} />
        </CardFooter>
      )} 
    </AwardUnfinishContainer>
  )
}

const isTournamentFinished = () => {
    return true
  }

const mapStateToProps = state => ({
  utils: state.auth
})


export default connect(mapStateToProps, {
  getHasAGift,
  getScoreTotal,
  getScoreTop,
  getSendMailGift,
  postSendMailGiftInternal,
  getDatesOfUser
})(AwardUnfinish)

