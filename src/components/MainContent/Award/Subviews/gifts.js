export default 
 [
  {
   "position":1,
   "principal":{
     "id":0,
       "name":"Mouse + teclado KM717 + Headset UC150 + Router",
       "urlImage":"/images/premios/IMG_1erPremio.png"
    },
    "secundario":{
      "id":1,
       "name":"Gift card con valor equivalente",
       "urlImage":"/images/premios/IMG_Amzn.png"
    }
  },
  { 
   "position":2,
   "principal":{
     "id":0,
       "name":"Monitor DELL P24 + MouseWM122",
       "urlImage":"/images/premios/IMG_2ndoPremio.png"
    },
    "secundario":{
      "id":1,
       "name":"Gift card con valor equivalente",
       "urlImage":"/images/premios/IMG_Amzn.png"
    }
  },
  {
   "position":3,
   "principal":{
     "id":0,
       "name":"Headset UC150",
       "urlImage":"/images/premios/IMG_3erPremio.png"
    },
    "secundario":{
      "id":1,
       "name":"Gift card con valor equivalente",
       "urlImage":"/images/premios/IMG_Amzn.png"
    }
  },
  {
   "position":4,
   "principal":{
     "id":0,
       "name":"DockingDA300 + MouseWM122",
       "urlImage":"/images/premios/IMG_4toPremio_1.png"
    },
    "secundario":{
      "id":1,
       "name":"Gift card con valor equivalente",
       "urlImage":"/images/premios/IMG_Amzn.png"
    }
  },
  {
   "position":5,
   "principal":{
     "id":0,
       "name":"Boina estilo golf + pelota",
       "urlImage":"/images/premios/IMG_5toPremio.png"
    },
    "secundario":{
      "id":1,
       "name":"Gift card con valor equivalente",
       "urlImage":"/images/premios/IMG_Amzn.png"
    }
  },
  {
   "position":6,
   "principal":{
     "id":0,
       "name":"Pelota personalizada",
       "urlImage":"/images/premios/IMG_6toPremio.png"
    },
    "secundario":{
      "id":1,
       "name":"Gift card con valor equivalente",
       "urlImage":"/images/premios/IMG_Amzn.png"
    }
  },
  {
   "position":7,
    "principal":{
      "id":0,
       "name":"Kit de pelotas Title ist velocity",
       "urlImage":"/images/premios/IMG_7toPremio.png"
    },
    "secundario":{
      "id":1,
       "name":"Gift card con valor equivalente",
       "urlImage":"/images/premios/IMG_Amzn.png"
    }
  },
  {
   "position":8,
    "principal":{
      "id":0,
       "name":"Forro de madera + pelota",
       "urlImage":"/images/premios/IMG_8toPremio.png"
    },
    "secundario":{
      "id":1,
       "name":"Gift card con valor equivalente",
       "urlImage":"/images/premios/IMG_Amzn.png"
    }
  },
  {
    "position":9,
    "principal":{
       "id":0,
       "name":"Guante de Driver + pelotaa",
       "urlImage":"/images/premios/IMG_9toPremio.png"
    },
    "secundario":{
      "id":1,
       "name":"Gift card con valor equivalente",
       "urlImage":"/images/premios/IMG_Amzn.png"
    }
  },
  {
   "position":10,
   "principal":{
     "id":0,
       "name":"Mouse + teclado KM717",
       "urlImage":"/images/premios/IMG_10moPremio.png"
    },
    "secundario":{
      "id":1,
       "name":"Gift card con valor equivalente",
       "urlImage":"/images/premios/IMG_Amzn.png"
    }
  }]
