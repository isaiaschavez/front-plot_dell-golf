import styled,{ keyframes }  from 'styled-components'


export const AwardSuccessContainer = styled.div`
  font-family: Roboto, arial;
  overflow-y:scroll;
  display: flex;
  height:100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  color: white;
  font-family: Roboto;
  @media (max-width: 768px) {
    margin-top: 1rem;
  }
  @media (min-width: 768px) {
    margin-top: 0rem;
  }
`
export const AwardSuccessHeader = styled.div`
text-shadow: 1px 1px 0 rgba(0,0,0,0.5);
  line-height: 1.6rem;
  padding-top:3rem;
  flex-grow: 1;
  flex-shrink: 3;
  font-family: Roboto-light, arial;
  font-weight: lighter;
  margin: 0;
  width: 60%;
  font-size: 0.8rem;
  &>h1{
  font-family: Roboto, arial;
  }
  

  @media (min-width: 768px) {
    line-height: 1.8rem;
  font-size: 1rem;
  }
  @media (min-width: 1280px) {
    line-height: 1.8rem;
    font-size: 1rem;
      &>h2{
       margin-bottom:0.5rem;
        font-size: 1.3rem;

    }
   
  }
  @media (min-width: 1600px) {
    line-height: 2.4rem;
    font-size: 1.2rem;
    margin-bottom:2rem;
     &>h2{
  margin-bottom:0.5rem;

    }
    &>h2{
  margin-bottom:0.5rem;
  }
  &>h3{
    font-size:1.8rem;
    
  }
  }
`

export const BodyCard = styled.div`
  flex-grow: 1;
  display: flex;
  flex-shrink:3;
`



export const Hrstyled = styled.div`

  height: 2px;
  background: rgb(0, 118, 206);
  margin: 0.5em 0;
  padding: 0 3rem;
background: linear-gradient(
    90deg,
    rgba(0, 118, 206, 1) 0%,
    rgba(200, 200, 221, 1) 50%,
    rgba(0, 118, 206, 1) 100%
  );




`
export const UserPosition = styled.span`
  transform:scale(1.2);
`
export const CardFooter = styled.div`
  flex-grow: 2;
flex-shrink:1;
  align-items: center;
  display: flex;
  flex-direction: column;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: start;
  width: 80%;
  height:20%;
  @media (min-width: 768px) {
    width: 35%;
  }
`