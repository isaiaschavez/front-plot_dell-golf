import React, { useState, useEffect } from 'react'
import GifCard from '../components/GifCard'
import Modal from '../components/Modal'
import Boton from '../components/Boton'
import GlobalFonts from '../global'
import ListaDeRegalos from './gifts'
import { connect } from 'react-redux'

import {AwardSuccessContainer,AwardSuccessHeader,BodyCard,Hrstyled,CardFooter} from './estilos'


import {  
  getSendMailGift,
  postSendMailGiftInternal,
} from '../../../../actions/authActions'


const AwardSuccess = ({utils,setSendedView, dataUser,getSendMailGift,postSendMailGiftInternal, }) => {


  const {
    idUser,
    nameUser,
    lastNameUser,
    emailUser,
    scoreUser,
    hasPoints,
    allscores
  } = dataUser

  const [gifts, setGifts] = useState(ListaDeRegalos)
  const [isGiftSended, setIsGiftSended] = useState(false)
  const [userData, setuserData] = useState({
    userPosition: null,
    userGifts: null
  })
  const { userPosition, userGifts } = userData
  
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [gifSelected, setgifSelected] = useState(null)

  const onPushCard = id => {
    if ( gifSelected && id === gifSelected.id) {
      setgifSelected(null)
      return
    }
    id === 0
      ? setgifSelected(userGifts.principal)
      : setgifSelected(userGifts.secundario)
  }

  const onSendGift = () => {
    if (!gifSelected) {
      return
    }
    const dataUserToSend = {
      name: nameUser,
      lastname: lastNameUser,
      email: emailUser,
      gift: gifSelected
    }

    // getSendMailGift(dataUserToSend)
    // postSendMailGiftInternal(dataUserToSend)
    setIsModalVisible( true )
    setIsGiftSended(true)
    
  }
  const onCloseModal = () => {
    setIsModalVisible( false )
    setSendedView()
  }

  useEffect(() => {
    // const { "userPosition", positionIndex } = getPositionInTable(dataUser)
    const userPosition = "1er lugar."
    const positionIndex = 2
    const userGifts = getGiftsForThisUser(gifts, positionIndex)
    setuserData({ ...userData, userPosition, userGifts })
  }, [])


  if (!userGifts) {
    return <h3>  </h3>
  }
  
  return (
    <AwardSuccessContainer>
      <GlobalFonts />
      {isModalVisible && (
        <Modal
          onCloseModal={onCloseModal}
          message={'Listo. Tu premio va en camino.'}
        />
      )}
      <AwardSuccessHeader>
        <h1>Premios</h1>
        <h3 className=''>¡Felicidades!</h3>
        <h2>
          Fuiste acreedor del:
          <br />
          <span> {userPosition} </span>
          <p>Con {dataUser.scoreUser} puntos</p>
        </h2>
        <Hrstyled></Hrstyled>
        <p className='mt-1'>Elije que premio deseas recibir</p>
      </AwardSuccessHeader>
      <BodyCard>
        <GifCard
          onPushCard={onPushCard}
          data={userGifts.principal}
          selected={gifSelected && gifSelected.id === 0}
        />
        <GifCard
          onPushCard={onPushCard}
          data={userGifts.secundario}
          selected={gifSelected && gifSelected.id === 1}
        />
      </BodyCard>
      <CardFooter>
        {
          <Boton
            disabled={gifSelected === null}
            buttonFunction={onSendGift}
            message={'Canjear premio'}
            // class={`send-button-award tipografia-light ${
            //   gifSelected === null ? 'opacity-75' : ''
            // }`}
          />
        }
      </CardFooter>
    </AwardSuccessContainer>
  )
}

const mapStateToProps = state => ({
  utils: state.auth
})
export default connect(mapStateToProps, {
  getSendMailGift,
  postSendMailGiftInternal,
})(AwardSuccess)



const getPositionInTable = dataUser => {
  
  // let position = null
  // let positionIndex = null
  // const userName = dataUser.nameUser + ' ' + dataUser.lastNameUser
  // console.log( "userName", userName );
  // console.log(dataUser);
  // dataUser.allscores.map( ( user, index ) => {
  //   if (user.user === userName && dataUser.scoreUser === user.scoreTotal) {
  //     position = getPositionString(index + 1)
  //     positionIndex = index + 1
  //     return 
  //   }
  // })
  // return { userPosition: position, positionIndex }
}
const getGiftsForThisUser = (gifts, positionIndex) => {
  console.log("positionIndex:",positionIndex);
  let giftToSend = null
  gifts.map(gift => {
    console.log("gift.position:",gift.position,"positionIndex:",positionIndex);
    if (gift.position === positionIndex) giftToSend = gift
  })
  return giftToSend
}

const getPositionString = id => {
  switch (id) {
    case 1:
      return '1er lugar.'
    case 2:
      return '2do lugar.'
    case 3:
      return '3er lugar.'
    case 4:
      return '4to lugar.'
    case 5:
      return '5to lugar.'
    case 6:
      return '6to lugar.'
    case 7:
      return '7mo lugar.'
    case 8:
      return '8vo lugar.'
    case 9:
      return '9no lugar.'
    case 10:
      return '10mo lugar.'
    default:
      return 'No has podido alzanzar un lugar.'
  }
}


