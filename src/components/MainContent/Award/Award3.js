/**
 * @Author: memo
 * @Date:   2020-10-18T16:25:20-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-22T22:06:55-06:00
 */
 import React, { Component } from "react";
 import { connect } from "react-redux";
 import './Award.scss'
 import {isMobile} from 'react-device-detect';
 const isEmpty = require("is-empty");

 class Award extends Component {
   constructor(props){
     super(props)
     this.state = {
       index:1,
       initialSection:<center>
                          <div>
                            <label className="text-award">
                              Aquí podrás canjear tus puntos por los premios que tenemos para ti
                            </label>
                          </div>
                           <div>
                             <button className="send-button-award"
                                //onClick={this.validateForm.bind(this)}
                                >
                               <center>
                                 Canjear
                               </center>
                             </button>
                           </div>
                         </center>,

         initialAwards: <center>
                         <img width="100%" src="/images/Img_premios.png"/>
                       </center>,
         awardSelected:null,
         firstAward: {
           image:"/images/question.png",
           name:"Premio 1"
         },
         secondAward: {
           image:"/images/question.png",
           name:"Premio 2"
         },

     }
   }

   onClickSelected(e){
     console.log("e:", e)
     this.setState({
       nameAward:e.name
     })
     this.forceUpdate()
   }

   validateForm(){
     switch (this.state.index) {
       case 1:
              this.setState({
                initialSection:<center>
                                      <label>
                                        1er lugar
                                      </label>
                                      <div>
                                        <button className="score-award" onClick={this.validateForm.bind(this)}>
                                          <center>
                                          <label>
                                            13000
                                          </label>
                                          </center>
                                        </button>
                                      </div>

                                      <div>
                                         {this.state.awardSelected == null &&
                                           <div className="awardNull">
                                               <label>
                                                   (da clic en el premio que quieras canjear)
                                               </label>
                                           </div>
                                         }
                                         {
                                           //this.state.awardSelected != null &&
                                           <label>
                                            {this.state.nameAward}
                                           </label>
                                         }
                                      </div>

                               </center>,

                initialAwards:<center>
                                  <div onClick={this.onClickSelected.bind(this, this.state.firstAward)}>
                                    <img width="100%" src={this.state.firstAward.image}/>
                                  </div>
                                  <div onClick={()=>{
                                    alert("2")
                                    this.setState({
                                        nameAward:this.state.secondAward.name,
                                        awardSelected:this.state.secondAward
                                    })
                                  }}>
                                    <img width="100%" src={this.state.secondAward.image}/>
                                  </div>
                              </center>
              })
         break;
       default:

     }
   }

   initialSection(){
     return (<center>
               <div  className="text-award">
                 <label>
                   Aquí podrás canjear tus puntos por los premios que tenemos para ti
                 </label>
               </div>
               <div>
                 <button className="send-button-award"
                    //onClick={this.validateForm.bind(this)}
                    >
                   <center>
                   <label>
                     Canjear
                   </label>
                   </center>
                 </button>
               </div>

           </center>);
   }

   renderWeb(){

     return(<div className="page-content-award">
         <center>
         <div className="rootDivScheduler">

            <div className="mainWrapperLayoutScheduler">
                <div className ="mainWrapperScheduler">
                  <div className ="schedulerTitle">
                      <label>Agendar cita</label>
                  </div>
                  <div className ="schedulerTitle">
                      <label>Agendar cita</label>
                  </div>
                  <div className ="schedulerTitle">
                      <label>Agendar cita</label>
                  </div>
                </div>
            </div>

         </div>
         </center>
       </div>)

   }

   render(){
      var pageContainer = isMobile? "page-mobile-content-award":"page-content-award"
      var position = isMobile ? "mainAwardLayout": "mainAwardLayout margin-web"
      return(<div className={pageContainer}>
        <center>
        <div className="rootDiv-award">
              <div className={position}>
                  <div className="mainWrapperAward">
                      <div className="awardLeft">
                          {this.state.initialSection}
                      </div>
                      <div className="awardRight">
                          {this.state.initialAwards}
                      </div>
                  </div>
              </div>
        </div>
        </center>
      </div>)
   }

   render2(){
     if(isMobile){
        return this.renderMobile()
     }else {
        return this.renderWeb()
     }
   }

 }

 const mapStateToProps = state => ({
   utils : state.utils,
 });

 export default connect(
   mapStateToProps,
   {
     //getAllGamesScore
   }
 )(Award);
