import { createGlobalStyle } from 'styled-components';
import fontbold from './Roboto-Regular.ttf';
import fontlight from './Roboto-Light.ttf';

export default createGlobalStyle`
  @font-face {
     font-family: 'Roboto';
     src: local('Roboto-Regular') url('/fonts/Roboto-Regular.ttf') format("truetype");
     url(${fontbold}) format('ttf'),
  }
   @font-face {
     font-family: 'Roboto-light';
     src: local('Roboto-light') url('/fonts/Roboto-Light.ttf') format("truetype");
     url(${fontlight}) format('ttf'),
  }
`;
