/**
 * @Author: memo
 * @Date:   2020-11-01T14:06:17-06:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-22T21:14:57-06:00
 */
 import React, { Component } from "react";
 import { connect } from "react-redux";
 import { withOrientationChange } from 'react-device-detect'

 import './Videos.scss'
 import { getAvatar, getScoreTotal } from "../../../actions/authActions";
 import VideoModal from './../Play/VideoModal'
 import {isMobile} from 'react-device-detect';
 const isEmpty = require("is-empty");
 const texts = []

 texts[0] = {
   label1:"",
   label2:"",
   label3:""
 }

 texts[1] = {
   label1:"",
   label2:"Sabemos que no ha sido un año fácil y que muchos de nosotros hemos adoptado el trabajo remoto.",
   label3:"Te presentamos Work From Home de Dell Technologies."
 }

 texts[3] = {
   label1:"",
   label2:"La transición hacia el mundo digital no sólo ha afectado nuestro estilo de vida, también las amenazas se han transformado, por lo que hoy más que nunca es clave estar protegido.",
   label3:""
 }


 texts[3] = {
   label1:"Ahora que ya conoces lo que podemos hacer, te contaré un poco de cómo lo haremos.",
   label2:"",
   label3:""
 }

 texts[4] = {
   label1:"Ahora que ya conoces lo que podemos hacer, te contaré un poco de cómo lo haremos.",
   label2:"",
   label3:""
 }


 texts[5] = {
   label1:"La transición hacia el mundo digital no sólo ha afectado nuestro estilo de vida, también las amenazas se han transformado, por lo que hoy más que nunca es clave estar protegido.",
   label2:"",
   label3:""
 }

 texts[6] = {
   label1:"La transición hacia el mundo digital no sólo ha afectado nuestro estilo de vida, también las amenazas se han transformado, por lo que hoy más que nunca es clave estar protegido.",
   label2:"",
   label3:""
 }

 texts[7] = {
   label1:"Como vimos en el video anterior, la seguridad es indispensable para una correcta operación, te presentamos las opciones que Dell Technologies ofrece.",
   label2:"",
   label3:""
 }

 texts[8] = {
   label1:"Como vimos en el video anterior, la seguridad es indispensable para una correcta operación, te presentamos las opciones que Dell Technologies ofrece.",
   label2:"",
   label3:""
 }

 texts[9] = {
   label1:"Las proyecciones en cuanto a transformación digital fueron rebasadas por mucho a partir de la crisis sanitaria. Hoy, en la era de los datos, éstos son más valiosos que nunca.",
   label2:"Hoy, en la era de los datos, éstos son más valiosos que nunca.",
   label3:""
 }

 texts[10] = {
   label1:"Las proyecciones en cuanto a transformación digital fueron rebasadas por mucho a partir de la crisis sanitaria.",
   label2:"",
   label3:""
 }


 texts[11] = {
   label1:"Platiquemos acerca de cómo los datos y las soluciones en la nube nos acercan cada vez más al futuro.",
   label2:"",
   label3:""
 }
 texts[12] = {
   label1:"Platiquemos acerca de cómo los datos y las soluciones en la nube nos acercan cada vez más al futuro.",
   label2:"",
   label3:""
 }

 texts[13] = {
   label1:"La transformación digital se permea cada día más a nuestra vida diaria, sin embargo aún hay un camino de transición que debemos recorrer, házlo de la mano de Dell Technologies.  ",
   label2:"",
   label3:""
 }

 texts[14] = {
   label1:"La transformación digital se permea cada día más a nuestra vida diaria, sin embargo aún hay un camino de transición que debemos recorrer, házlo de la mano de Dell Technologies.  ",
   label2:"",
   label3:""
 }

 class Videos extends Component {
   constructor(props){
     super(props)
     this.state = {
        nameVideo:"Work From Home",
        isVisibleModal:false,
        level:1
     }
   }
   UNSAFE_componentWillMount(){
     this.props.getScoreTotal(this.props.auth.user.id)
   }
   renderWeb(){

     var score = []
     if(this.props.auth && this.props.auth.score &&
        this.props.auth.score.score){
          score = this.props.auth.score.score.score
          console.log("score[0]:", score[0])
          console.log("score[1]:", score[1])
        }
        console.log("score[1]:", score[1])
     return(<div className="page-content-video">
           <div className="mainWrapperLayoutVideo">
              <div>
                  <img src="/images/avataaars1.png"
                      width={isMobile?"10%":"100%"}/>
              </div>

              <div>
                    <div className="rightVideo">
                         {
                           this.props.auth &&
                           <div className="welcomeName">
                               ¡Hola {this.props.auth.user.name}!
                           </div>
                         }
                    </div>

                    <div className="rightVideo">
                          <label className="fix-text-video">En esta sección podrás ver los videos informativos
                              sobre nuestra tecnología {this.state.nameVideo} una vez que los hayas desbloqueado en el juego.
                          </label>
                    </div>
              </div>
           </div>
           <div className="videoButtons">
                 { score && score[1]!=0 && score[1]!=null &&
                   <div className="videoButton-activated" onClick={()=>{
                     this.setState({
                       level:1,
                       isVisibleModal:true
                     })
                   }}>
                         Video 1
                   </div>
                 }
                 { (!score || score[1] == 0 || score[1] == undefined )&&
                   <div className="videoButton">
                         Video 1
                   </div>
                 }

                 { score && score[3]!=0 &&  score[3]!= null &&
                   <div className="videoButton-activated" onClick={()=>{
                     this.setState({
                       level:3,
                       isVisibleModal:true
                     })
                   }}>
                         Video 2
                   </div>
                 }
                 { (!score || score[3] == 0 || score[3] == undefined)&&
                   <div className="videoButton">
                         Video 2
                   </div>
                 }

           </div>

           {
             this.state.isVisibleModal &&
             <VideoModal
                 isVisibleModal={this.state.isVisibleModal}
                 onCloseModal={()=>{
                   this.setState({isVisibleModal:false})
                 }}
                 auth={this.props.auth}
                 level={this.state.level}
                 texts={texts[this.state.level]}
                 numberVideo={"/videos/"+(this.state.level)+".mp4"}
                 onMenu={true}
             />
           }
       </div>
     )
   }


   renderMobile(){
     var pageContainer = "page-content-video"

     if(this.props.isLandscape){
       // alert("entro")
        pageContainer = "page-content-video fix-page-content-video-mobile"
     }

     var score = []
     if(this.props.auth && this.props.auth.score &&
        this.props.auth.score.score){
          score = this.props.auth.score.score.score
          console.log("score[0]:", score[0])
          console.log("score[1]:", score[1])
        }
     return(<div className={pageContainer}>
         <div className="mainWrapperLayoutVideo-mobile video-mobile">
            <div>
                <center>
                    <img src="/images/avataaars1.png" width="30%"/>
                </center>
            </div>

            <div>
                  <div className="rightVideo-mobile-title">
                       {
                         this.props.auth &&
                         <div className="welcomeName">Hola {this.props.auth.user.name}
                         </div>
                       }
                  </div>

                  <div className="rightVideo-mobile">
                        <label>En esta sección podrás ver los videos informativos
                            sobre nuestra tecnología {this.state.nameVideo} una vez que los hayas desbloqueado en el juego.
                        </label>
                  </div>
            </div>
         </div>
         <div className="videoButtons">
               { score && score[1]!=0 &&
                 <div className="videoButton-activated" onClick={()=>{
                   this.setState({
                     level:1,
                     isVisibleModal:true
                   })
                 }}>
                       Video 1
                 </div>
               }
               { !score || score[1] ==0 &&
                 <div className="videoButton">
                       Video 1
                 </div>
               }

               { score && score[3]!=0 &&
                 <div className="videoButton-activated" onClick={()=>{
                   this.setState({
                     level:3,
                     isVisibleModal:true
                   })
                 }}>
                       Video 2
                 </div>
               }
               { !score || score[3] ==0 &&
                 <div className="videoButton">
                       Video 2
                 </div>
               }

         </div>

         {
           this.state.isVisibleModal &&
           <VideoModal
               isVisibleModal={this.state.isVisibleModal}
               onCloseModal={()=>{
                 this.setState({isVisibleModal:false})
               }}
               auth={this.props.auth}
               level={this.state.level}
               texts={texts[this.state.level]}
               numberVideo={"/videos/"+(this.state.level)+".mp4"}
               onMenu={true}
           />
         }
       </div>)
   }


   render(){
     if (isMobile) {
       return this.renderMobile()
     }else{
       return this.renderWeb()
     }
   }

 }

 const mapStateToProps = state => ({
   auth  : state.auth,
   utils : state.utils,
 });

 export default connect(
   mapStateToProps,
   {
     getScoreTotal
   }
 )(withOrientationChange(Videos));
