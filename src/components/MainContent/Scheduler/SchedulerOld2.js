//
// // import { connect } from "react-redux";
// // import './Scheduler.scss'
// import mobiscroll from '@mobiscroll/react';
// import '@mobiscroll/react/dist/css/mobiscroll.min.css';
// import React, { Component } from "react";
// import { connect } from "react-redux";
//
// let API_KEY = "3831100934-aifu3rlik77n0p3d55po18d8vmb2jgvq.Schedulers.googleusercontent.com";
// let CLIENT_ID = 'AIzaSyDUJmgeSeURuhTxE30-6OfsaBaUm_0wsUA';
//
// var CALENDAR_ID = 'theacidmedia.net_8l6v679q5j2f7q8lpmcjr4mm3k@group.calendar.google.com';
// var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
// var SCOPES = "https://www.googleapis.com/auth/calendar.readonly";
// const now = new Date();
// let calApiLoaded;
// let firstDay = new Date(now.getFullYear(), now.getMonth() - 1, -7);
// let lastDay = new Date(now.getFullYear(), now.getMonth() + 2, 14);
//
// class Scheduler extends React.Component {
// 	constructor(props) {
//         super(props);
//
//         this.state = {
//             events: []
//         };
//     }
//
//     componentDidMount() {
//     // Load the Google API Client
//     		// window.onGoogleLoad = () => {
//     		// 	window.gapi.load('client', this.initClient);
//     		// }
//         this.loadGoogleSDK();
//     }
//
//     // Load the SDK asynchronously
//     loadGoogleSDK = () => {
//         (function (d, s, id) {
//             var js, fjs = d.getElementsByTagName(s)[0];
//             if (d.getElementById(id)) {
//                 onGoogleLoad();
//                 return;
//             }
//             js = d.createElement(s);
//             js.id = id;
//             js.src = "https://apis.google.com/js/api.js?onload=onGoogleLoad";
//             js.onload = "onGoogleLoad";
//             fjs.parentNode.insertBefore(js, fjs);
//         }(document, 'script', 'google-jssdk'));
//     };
//
//     // Init the Google API client
//     initClient = () => {
//         window.gapi.client.init({
//             apiKey: API_KEY,
//             clientId: CLIENT_ID,
//             discoveryDocs: DISCOVERY_DOCS,
//             scope: SCOPES
//         }).then(() => {
//             calApiLoaded = true;
//             this.loadEvents(firstDay, lastDay);
//         });
//     }
//
//     // Load events from Google Calendar between 2 dates
//     loadEvents = (firstDay, lastDay) => {
//         // Only load events if the Google API finished loading
//         if (calApiLoaded) {
//             window.gapi.client.calendar.events.list({
//                 'calendarId': CALENDAR_ID,
//                 'timeMin': firstDay.toISOString(),
//                 'timeMax': lastDay.toISOString(),
//                 'showDeleted': false,
//                 'singleEvents': true,
//                 'maxResults': 100,
//                 'orderBy': 'startTime'
//             }).then((response) => {
//                 let event;
//                 let events = response.result.items;
//                 let eventList = [];
//                 // Process event list
//                 for (var i = 0; i < events.length; ++i) {
//                     event = events[i];
//                     eventList.push({
//                         start: new Date(event.start.date || event.start.dateTime),
//                         end: new Date(event.end.date || event.end.dateTime),
//                         text: event.summary || 'No Title'
//                     });
//                 }
//                 // Pass the processed events to the calendar
//                 this.setState({ events: eventList });
//             });
//         }
//     }
//
//     onPageLoading = (event, inst) => {
//         const year = event.firstDay.getFullYear();
//         const month = event.firstDay.getMonth();
//
//          // Calculate dates
//         // (pre-load events for previous and next months as well)
//         firstDay = new Date(year, month - 1, -7);
//         lastDay = new Date(year, month + 2, 14);
//
//         this.loadEvents(firstDay, lastDay);
//     }
//
//     render() {
//         return (
//             <mobiscroll.Eventcalendar
//                 theme="ios"
//                 themeVariant="light"
//                 display="inline"
//                 view={{
//                     calendar: {
//                         labels: true
//                     }
//                 }}
//                 data={this.state.events}
//                 onPageLoading={this.onPageLoading}
//             />
//         );
//     }
// }
//
// const mapStateToProps = state => ({
//   utils : state.utils,
// });
// export default connect(
//   mapStateToProps,
//   {
//     //getAllGamesScore
//   }
// )(Scheduler);

import React, { Component } from "react";
import { connect } from "react-redux";
import './Scheduler.scss'
import {isMobile} from 'react-device-detect';

const API_KEY = "AIzaSyDUJmgeSeURuhTxE30-6OfsaBaUm_0wsUA"
const CLIENT_ID = "3831100934-aifu3rlik77n0p3d55po18d8vmb2jgvq.apps.googleusercontent.com"
const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"]
const SCOPES = "https://www.googleapis.com/auth/calendar.events"
class Scheduler extends Component {
  onClick(){
    var gapi = window.gapi

    gapi.load('client:auth2', () => {
     console.log('loaded client')

     gapi.client.init({
       apiKey: API_KEY,
       clientId: CLIENT_ID,
       discoveryDocs: DISCOVERY_DOCS,
       scope: SCOPES,
     })

     gapi.client.load('calendar', 'v3', () => console.log('bam!'))

     gapi.auth2.getAuthInstance().signIn()
     .then(() => {

       var event = {
         'summary': 'Awesome Event!',
         'location': '800 Howard St., San Francisco, CA 94103',
         'description': 'Really great refreshments',
         'start': {
           'dateTime': '2020-06-28T09:00:00-07:00',
           'timeZone': 'America/Los_Angeles'
         },
         'end': {
           'dateTime': '2020-06-28T17:00:00-07:00',
           'timeZone': 'America/Los_Angeles'
         },
         'recurrence': [
           'RRULE:FREQ=DAILY;COUNT=2'
         ],
         'attendees': [
           {'email': 'lpage@example.com'},
           {'email': 'sbrin@example.com'}
         ],
         'reminders': {
           'useDefault': false,
           'overrides': [
             {'method': 'email', 'minutes': 24 * 60},
             {'method': 'popup', 'minutes': 10}
           ]
         }
       }

       var request = gapi.client.calendar.events.insert({
         'calendarId': 'primary',
         'resource': event,
       })

       request.execute(event => {
         console.log(event)
         window.open(event.htmlLink)
       })
     })
   })}


  render(){
    var pageContainer = isMobile? "page-mobile-content":"page-content"

    return(<div className={pageContainer}>
      Scheduler

      Client ID :  3831100934-aifu3rlik77n0p3d55po18d8vmb2jgvq.apps.googleusercontent.com

      Client Secret : _dpWcbieciVchdjLJWGM0or3

     api key  AIzaSyDUJmgeSeURuhTxE30-6OfsaBaUm_0wsUA


       <button style={{width: 100, height: 50, top:300}} onClick={this.onClick.bind(this)}>Add Event</button>
      </div>
    )
  }

}

const mapStateToProps = state => ({
  utils : state.utils,
});
export default connect(
  mapStateToProps,
  {
    //getAllGamesScore
  }
)(Scheduler);
