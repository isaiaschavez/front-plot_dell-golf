/**
 * @Author: memo
 * @Date:   2020-10-18T15:09:48-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-10-18T15:10:22-05:00
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import './Scheduler.scss'
import {isMobile} from 'react-device-detect';

class Scheduler extends Component {
  onClick(){

  }

  render(){
    var pageContainer = isMobile? "page-mobile-content":"page-content"

    return(<div className={pageContainer}>


       <button style={{width: 100, height: 50, top:300}} onClick={this.onClick.bind(this)}>Add Event</button>
      </div>
    )
  }

}

const mapStateToProps = state => ({
  utils : state.utils,
});
export default connect(
  mapStateToProps,
  {
    //getAllGamesScore
  }
)(Scheduler);
