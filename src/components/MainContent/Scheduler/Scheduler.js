/**
 * @Author: memo
 * @Date:   2020-10-18T15:09:48-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-18T13:07:07-06:00
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import "react-datepicker/dist/react-datepicker.css"

import './Scheduler.scss'
import {isMobile, isSafari} from 'react-device-detect';
import { sendScheduler } from "../../../actions/authActions";
import DatePicker from 'react-datepicker';
const isEmpty = require("is-empty");
const CustomInput = ({ value, onClick }) => (
  <div className="custom-input-date" onClick={onClick}>
    {value}
  </div>
);


const CustomInputTime = ({ value, onClick }) => (
  <div className="custom-input-time" onClick={onClick}>
    {value}
  </div>
);

class Scheduler extends Component {
  constructor(props){
    super(props)
    this.state = {
      isSended:false,
      errorMessageDate:null,
      errorMessageTime:null,
      errorMessageComment:null,
      startDate:null,
      timeDate:null
    }
  }

  validateForm(){
    if(isSafari){
      // console.log("this.state.startDate:", this.state.startDate)
      if(isEmpty(this.state.startDate) || this.state.startDate == null){
          this.setState({
            errorMessageDate:"Ingrese el día de su cita."
          })
      }else{
        this.setState({
          errorMessageDate:null
        })
      }

      console.log("this.state.timeDate:", this.state.timeDate)


      if(isEmpty(this.state.timeDate)){
        this.setState({
          errorMessageTime:"Ingrese la hora de su cita."
        })
      }else{
        this.setState({
          errorMessageTime:null
        })
      }
      var commentInput = document.getElementById('commentInput')

      if(isEmpty(commentInput.value)){
        this.setState({
          errorMessageComment:"Ingrese su comentario."
        })
      }else{
        this.setState({
          errorMessageComment:null
        })
      }


      if(
        this.state.startDate != null &&
        this.state.timeDate  != null &&
        !isEmpty(commentInput.value)
      ){
        var month = this.state.startDate.getMonth()
        month = month + 1
        var day = this.state.startDate.getDate()
        var year = this.state.startDate.getFullYear()
        var min = this.state.timeDate.getMinutes()
        var hour = this.state.timeDate.getHours()
        // console.log("month:", month)
        // console.log("day:", day)
        // console.log("year:", year)
        // console.log("min:", min)
        // console.log("hour:", hour)
        this.setState({
          isSended:true
        },()=>{

          var dateString = year + "-" + month + "-" + day
          var timeString = hour + ":" + min
          console.log("dateString:", dateString)
          console.log("timeString:", timeString)

          this.props.sendScheduler({
            dateInput:dateString,
            timeInput:timeString,
            commentInput:commentInput.value,
            user:this.props.auth.user,
            email:this.props.auth.user.email
          })
        })

      }

    }else{
      var dateInput = document.getElementById('dateInput')
      var timeInput = document.getElementById('timeInput')
      var commentInput = document.getElementById('commentInput')
      // console.log("dateInput:" + dateInput.value)
      // console.log("timeInput:" + timeInput.value)
      // console.log("commentInput:" + commentInput.value)
      // console.log("isEmpty(dateInput.value):" + isEmpty(dateInput.value))
      if(isEmpty(dateInput.value)){
          this.setState({
            errorMessageDate:"Ingrese el día de su cita."
          })
      }else{
        this.setState({
          errorMessageDate:null
        })
      }
      if(isEmpty(timeInput.value)){
        this.setState({
          errorMessageTime:"Ingrese la hora de su cita."
        })
      }else{
        this.setState({
          errorMessageTime:null
        })
      }
      if(isEmpty(commentInput.value)){
        this.setState({
          errorMessageComment:"Ingrese su comentario."
        })
      }else{
        this.setState({
          errorMessageComment:null
        })
      }
      if(
        !isEmpty(dateInput.value) &&
        !isEmpty(timeInput.value) &&
        !isEmpty(commentInput.value)
        // this.state.errorMessageDate == null &&
        // this.state.errorMessageComment == null &&
        // this.state.errorMessageTime == null
      ){
        this.setState({
          isSended:true
        },()=>{
          this.props.sendScheduler({
            dateInput:dateInput.value,
            timeInput:timeInput.value,
            commentInput:commentInput.value,
            user:this.props.auth.user,
            email:this.props.auth.user.email
          })
        })
      }
    }

  }

  renderMobile(){
    var pageContainer = "page-mobile-content"

    return(<div className={pageContainer}>
      <center>
          <div className="mobile-content">

          { !this.state.isSended &&
            <>
            <div className ="schedulerTitle">
                <label>Agendar cita</label>
            </div>
            <div>
                <div className="dialog001">
                Selecciona el día en que te gustaría tener una conversación con alguno de nuestros asesores
                </div>
                { isSafari &&
                  <div className="inputsDate">
                    <center>
                      <input id="dateInput" className="dateInput-mobile" type="date"/>
                      <input id="timeInput" className="dateInput2-mobile" type="time"/>
                      <div className="auth-error">
                        {this.state.errorMessageDate}
                      </div>
                      <div className="auth-error">
                        {this.state.errorMessageTime}
                      </div>
                    </center>
                  </div>
                }

                <div className="commentContainer">
                  <textarea
                      id="commentInput"
                      placeholder="Comentarios"
                      rows="10"
                      columns="20"
                  />
                  <div className="auth-error">
                    {this.state.errorMessageComment}
                  </div>
                  <button className="scheduler-button" onClick={this.validateForm.bind(this)}>
                    <label>
                      Agendar
                    </label>
                  </button>
                </div>
            </div>
            </>
          }

          {
            this.state.isSended &&
            <>
              <div className="confirm">
                  <img src="./images/ok.png" width="20%"/>
                  <label>¡Gracias por agendar tu cita!</label>
                    <label>Pronto uno de nuestros asesores</label>
                      <label>estará comunicándose contigo.</label>
              </div>
            </>
          }

          </div>
      </center>
      </div>)
  }




  renderWeb(){
    var pageContainer = "page-content-scheduler"

    if(this.state.isSended){
      return (
                <div className="mainWrapperLayoutScheduler-confirm rootDivScheduler-confirm  confirmTxt">
                  <center>
                    <div>
                        <img className="fixImageConfirm" src="./images/ok.png" width="40%"/>
                    </div>
                    <div>
                        <label>¡Gracias por agendar tu cita!</label>
                    </div>
                    <div>
                      <label>Pronto uno de nuestros asesores</label>
                    </div>
                    <div>
                      <label>estará comunicándose contigo.</label>
                    </div>
                  </center>
                </div>
              )
    }
    else {
      return(<div className={pageContainer}>
        <center>
        <div className="rootDivScheduler">
              <div className="mainWrapperLayoutScheduler">
              {
                !this.state.isSended &&
                <div className ="mainWrapperScheduler">
                <div className ="schedulerTitle">
                    <label>Agendar cita</label>
                </div>


                <div className="wrapperScheduler">
                  <div className="wrapperSchedulerDate01">
                    <div className="dialog001">
                        Selecciona el día en que te gustaría tener una conversación con alguno de nuestros asesores
                    </div>
                    {
                      !isSafari &&
                      <div className="inputsDate">
                        <center>
                          <input id="dateInput" className="dateInput" type="date"/>
                          <input id="timeInput" className="dateInput2" type="time"/>
                          <div className="auth-error">
                            {this.state.errorMessageDate}
                          </div>
                          <div className="auth-error">
                            {this.state.errorMessageTime}
                          </div>

                        </center>
                      </div>
                    }
                    {
                      isSafari &&
                      <div className="row-datepicker-safari">
                      <DatePicker
                            selected={this.state.startDate}
                            onChange={startDate => this.setState({startDate})}
                            customInput={<CustomInput />}
                          />
                          <DatePicker
                            selected={this.state.timeDate}
                            onChange={timeDate => this.setState({timeDate})}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            timeCaption="Time"
                            dateFormat="h:mm"
                            customInput={<CustomInputTime />}
                          />
                      </div>
                    }
                    {
                      isSafari &&
                      <>
                        <div className="auth-error">
                          {this.state.errorMessageDate}
                        </div>
                        <div className="auth-error">
                          {this.state.errorMessageTime}
                        </div>
                      </>
                    }
                  </div>
                  <div className="wrapperSchedulerDate02">
                      <center>
                        <textarea
                            id="commentInput"
                            placeholder="Comentarios"
                            rows="6"
                            cols="40"
                        />
                        <div className="auth-error">
                          {this.state.errorMessageComment}
                        </div>
                        <button className="scheduler-button" onClick={this.validateForm.bind(this)}>
                          <label>
                            Agendar
                          </label>
                        </button>
                      </center>
                  </div>
                </div>
                </div>
              }
              </div>
          </div>
        </center>
       </div>)
    }
  }

  render(){
    if(isMobile){
        return this.renderMobile()
    }else{
        return this.renderWeb()
    }
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  {
    sendScheduler
  }
)(Scheduler);
