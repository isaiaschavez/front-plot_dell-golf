/**
 * @Author: memo
 * @Date:   2020-10-07T21:49:30-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-18T17:36:20-06:00
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import {isMobile} from 'react-device-detect';
import Avatar from 'avataaars'

class Dashboard extends Component {


  renderMobile(){
    return(<div className="page-mobile-content-dashboard">
    {
      // <img
      //     className="centerImage"
      // width="20%"
      //       src="images/mainLogo.png"/>
    }
      <center>
          <div className="mobile-content-dashboard">

            <div>
                <div>
                  <img  id="topImage"
                        width="45%"
                        src="images/mainLogo.png"/>
                </div>

                <div>
                  <img  id="topImage"
                        width="45%"
                        src="images/logos_vertical.png"/>
                </div>

                <div className="headerDashBoard-mobile">
                    <label className="header-text-mobile">
                    ¡Bienvenido al Torneo de Golf Digital!
                    </label>
                    <label className="header-text-mobile2">
                      Demuestra tus habilidades de golfista, y gana increíbles premios.
                    </label>
                </div>
                <div className="headerDashBoard2-mobile">
                  <label className="welcomeHome2">
                   El torneo está abierto del
                   23 de Noviembre al 7 de Diciembre, completa todos los niveles y lo antes posible, pues en caso de empate por puntos, se tomará en cuenta la fecha en la que se haya terminado el juego.
                  </label>
                </div>
            </div>
            <div className="scroller">

            <div class="card">

            <div class="card">
              <center>
                <img width="25%" src="./../../../../images/icono_golf.png"
                 onClick={()=>window.location.href = '/Practice'}
                 />
             </center>
            </div>

              <div className="lowerButtonsTitle">
                <label className="labelTitle">
                1. Practicar y jugar golf

                </label>

                <label className="labelText">
                Practica tus tiros antes de iniciar la partida. Recuerda, una vez que inicies el juego, no se podrán repetir los niveles ni volver a empezar.
                </label>
              </div>
            </div>




            <div class="card">
              <center>
                <img width="25%" src="./../../../../images/icono_laptop.png"
                 //onClick={()=>window.location.href = '/Play'}
                 />
             </center>
            </div>
            <div class="card">
              <div className="lowerButtonsTitle">
                <label className="labelTitle">
                2. Conoce las soluciones Dell
                </label>

                <label className="labelText">
                Mira breves videos y aprende las nuevas soluciones Dell Technologies.
                </label>
              </div>
            </div>


            <div class="card">
                <center>
                  <img width="25%" src="./../../../../images/icono_quiz.png"
                   //onClick={()=>window.location.href = '/Play'}
                   />
               </center>
            </div>
            <div class="card">
              <div className="lowerButtonsTitle">
                <label className="labelTitle">
                    3. Contesta correctamente  y gana puntos.
                </label>

                <label className="labelText">
                    Responde breves quizzes para seguir sumando puntos y subir el ranking.
                </label>
              </div>
            </div>



            <div class="card">
                <center>
                  <img width="25%" src="./../../../../images/icono_calendario.png"
                   onClick={()=>window.location.href = '/Scheduler'}
                   />
               </center>
            </div>
            <div class="card">
              <div className="lowerButtonsTitle">
                <label className="labelTitle">
                  4. Agenda una cita con el asesor.
                </label>
              </div>
            </div>
            <div class="card">
              <center>
                <img width="30%" src="./../../../../images/icono_premios.png"
                 onClick={()=>window.location.href = '/Awards'}/>
             </center>
            </div>
            <div class="card">
                <div className="lowerButtonsTitle">
                  <label className="labelTitle">
                    5. Redime tu premio y ¡a disfrutar!.
                  </label>
                </div>
            </div>


            </div>

            <div class="cards">
              <div class="card">
                  <button className="send-button-welcome" onClick={()=>window.location.href = '/Award'}>
                      Redimir premio
                  </button>
                  {/* <button className="send-button-welcome" onClick={()=>window.location.href = '/Play'}>
                      Jugar ahora
                  </button> */}
              </div>
            </div>

          </div>
      </center>
      </div>)
  }


  renderWeb(){
    var pageContainer = "page-content-dashboard"

    return(<div className={pageContainer}>
      <center>
      <div className="rootDivScheduler">
            <div className="mainWrapperLayoutDashboard">
              <img  id="topImage"
                    src="images/mainLogo.png"/>
              <div className="headerDashBoard">
                  <label>
                  ¡Bienvenido al Torneo de Golf Digital!
                  </label>
                  <label>
                    Demuestra tus habilidades de golfista, y gana increíbles premios.
                  </label>
              </div>
              <div className="headerDashBoard2">
                <label className="welcomeHome2">
                 El torneo está abierto del 23 de Noviembre al 7 de Diciembre, completa todos los niveles y lo antes posible, pues en caso de empate por puntos, se tomará en cuenta la fecha en la que se haya terminado el juego.
                </label>
              </div>

              <div className="row-dashboard">
                  <div className="leftBox">
                    <center>
                      <img width="30%"
                          src="./../../../../images/icono_golf.png"
                          onClick={()=>window.location.href = '/Practice'}/>
                    </center>
                  </div>
                  <div className="rightBox">
                      <div className="rightBoxTxt">
                          <div>
                            <label className="labelTitle">
                              1. Practicar y jugar golf
                            </label>
                          </div>
                          <div>
                            <label className="labelText">
                              Practica tus tiros antes de iniciar la partida. Recuerda, una vez que inicies el juego,
                            </label>
                          </div>
                          <div>
                            <label className="labelTextA">
                              no se podrán repetir los niveles ni volver a empezar.
                            </label>
                          </div>
                      </div>

                  </div>
              </div>

              <div className="row-dashboard">
                  <div className="leftBox">
                    <center>
                      <img width="30%"
                          src="./../../../../images/icono_laptop.png"
                          //onClick={()=>window.location.href = '/Play'}
                          />
                    </center>
                  </div>
                  <div className="rightBox">
                      <div className="rightBoxTxt">
                          <div>
                            <label className="labelTitle">
                            2. Conoce las soluciones Dell
                            </label>
                          </div>
                          <div>
                            <label className="labelText">
                            Mira breves videos y aprende las nuevas soluciones Dell Technologies.
                            </label>
                          </div>
                      </div>

                  </div>
              </div>

              <div className="row-dashboard">
                  <div className="leftBox">
                    <center>
                      <img width="30%"
                          src="./../../../../images/icono_quiz.png"
                          //onClick={()=>window.location.href = '/Play'}
                          />
                    </center>
                  </div>
                  <div className="rightBox">
                      <div className="rightBoxTxt">
                          <div>
                            <label className="labelTitle">
                            3. Contesta correctamente  y gana puntos.
                            </label>
                          </div>
                          <div>
                            <label className="labelText">
                            Responde breves quizzes para seguir sumando puntos y subir el ranking.
                            </label>
                          </div>
                      </div>

                  </div>
              </div>

              <div className="row-dashboard">
                  <div className="leftBox">
                    <center>
                      <img width="30%"
                          src="./../../../../images/icono_calendario.png"
                          onClick={()=>window.location.href = '/Scheduler'}/>
                    </center>
                  </div>
                  <div className="rightBox">
                      <div className="rightBoxTxt">
                          <div>
                            <label className="labelTitle">
                            4. Agenda una cita con el asesor.
                            </label>
                          </div>
                      </div>
                  </div>
              </div>

              <div className="row-dashboard">
                  <div className="leftBox">
                    <center>
                      <img width="30%"
                          src="./../../../../images/icono_premios.png"
                          onClick={()=>window.location.href = '/Award'}/>
                    </center>
                  </div>
                  <div className="rightBox">
                      <div className="rightBoxTxt">
                          <div>
                            <label className="labelTitle">
                            5. Redime tu premio y ¡a disfrutar!.
                            </label>
                          </div>
                      </div>
                  </div>
              </div>

              <div className="cardsWeb">
                <div className="cardWeb">
                    <button className="send-button-welcomeWeb fixButtonLeft" onClick={()=>window.location.href = '/Award'}>
                        Redimir premio
                    </button>
                    {/* <button className="send-button-welcomeWeb" onClick={()=>window.location.href = '/Play'}>
                        Jugar ahora
                    </button> */}
                </div>
              </div>

            </div>
      </div>
      </center>
      </div>)
  }


  render(){
    if(isMobile){
      return this.renderMobile()
    }else {
      return this.renderWeb()
    }
  }
}

const mapStateToProps = state => ({
  utils : state.utils,
});
export default connect(
  mapStateToProps,
  {
    //getAllGamesScore
  }
)(Dashboard);
