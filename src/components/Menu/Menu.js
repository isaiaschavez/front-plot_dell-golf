import React, { Component } from "react";
import { bool } from 'prop-types';
import { StyledMenu } from './Menu.styled';
import {ReactNavbar} from "react-responsive-animate-navbar";
const Menu = ({ open, ...props }) => {

  const isHidden = open ? true : false;
  const tabIndex = isHidden ? 0 : -1;

  return (
    <StyledMenu open={open} aria-hidden={!isHidden} {...props}>
      <a href="/about" tabIndex={tabIndex}>
        <span aria-hidden="true">💁🏻‍♂️</span>
          About us
      </a>
         <hr/>
      <a href="/" tabIndex={tabIndex}>
        <span aria-hidden="true">💸</span>
        Pricing
        </a>
      <a href="/Contact" tabIndex={tabIndex}>
        <span aria-hidden="true">📩</span>
        Contact
        </a>
    </StyledMenu>
  )

}

// class Menu extends Component {
//   render() {
//     return (
//       <ReactNavbar
//         color="rgb(25, 25, 25)"
//         logo="https://svgshare.com/i/KHh.svg"
//         menu={[
//           { name: "HOME", to: "/" },
//           { name: "ARTICLES", to: "/articles" },
//           { name: "ABOUT ME", to: "/about" },
//           { name: "CONTACT", to: "/contact" },
//         ]}
//         social={[
//           {
//             name: "Linkedin",
//             url: "https://www.linkedin.com/in/nazeh-taha/",
//             icon: ["fab", "linkedin-in"],
//           },
//           {
//             name: "Facebook",
//             url: "https://www.facebook.com/nazeh200/",
//             icon: ["fab", "facebook-f"],
//           },
//           {
//             name: "Instagram",
//             url: "https://www.instagram.com/nazeh_taha/",
//             icon: ["fab", "instagram"],
//           },
//           {
//             name: "Twitter",
//             url: "http://nazehtaha.herokuapp.com/",
//             icon: ["fab", "twitter"],
//           },
//         ]}
//       />
//     );
//   }
// }

Menu.propTypes = {
  open: bool.isRequired,
}

export default Menu;
