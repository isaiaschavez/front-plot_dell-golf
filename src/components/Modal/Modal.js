/**
 * @Author: memo
 * @Date:   2020-10-15T21:55:01-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-12T21:26:59-06:00
 */
 import React, { Component,  useState, useRef, useEffect } from "react";
 import {isMobile} from 'react-device-detect';
 import "./Modal.scss";

 class Modal extends Component {
   constructor(props) {
     super(props);
      this.state = {

      };
   }
   render(){
      return(
        <div className={this.props.isVisibleModal ? "modal display-block" : "modal display-none"}>
          <div class={isMobile?"modal-content-mobile":"modal-content"}>
            <span class={isMobile?"close-mobile":"close-login"} onClick={this.props.onCloseModal}>&times;</span>
            <p className="modal-text">{this.props.message}</p>
            <p className="modal-text">{this.props.message2}</p>
          </div>
        </div>
      )
   }
 }

 export default Modal;
