/**
 * @Author: memo
 * @Date:   2020-10-03T00:48:40-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-11-18T11:16:03-06:00
 */



import React from 'react';
import styled from 'styled-components';
import setAuthToken from "./../../components/Auth/setAuthToken";
import {isMobile} from 'react-device-detect';

const Ul = styled.ul`
  list-style: none;
  display: flex;
  cursor: pointer;
  align-items: center;
  flex-flow: row nowrap;
  li {
    padding:5px 15px;
  }

  @media (max-width: 768px) {
    flex-flow: column nowrap;
    /* background-color: #0D2538; */
    overflow-y: scroll;

     background: linear-gradient(
       to right,
       #0076CE,
       #00447C
     );
    position: fixed;
    transform: ${({ open }) => open ? 'translateX(0)' : 'translateX(100%)'};
    top: 0;
    right: 0;
    height: 100vh;
    width: 100%;
    padding-top: 3.5rem;
    transition: transform 0.3s ease-in-out;
    /* z-index:10000; */
    li {
      color: #fff;
    }
    ;
    a {
      color: #fff;
    }
  }
`;
const ItemBlocked = styled.li`
  opacity:0.5;

`




const RightNav = ({ open, exit }) => {
  return (
    <Ul open={open}>
    {
      isMobile&&
      <li>
        <center>
          <img width="15%" src="/images/mainLogo.png"
          onClick={()=>{
              window.location.href = "/Dashboard"
            }}
          />
        </center>
      </li>
    }

    {
      isMobile&&
      <li>
      <div onClick={()=>{
          window.location.href = "/Avatar"
        }}>
          Mi perfil
      </div>
      </li>
    }

    {
      isMobile&&
      <li>
        <center>
          <img width="60%" src="/images/pleca.png"/>
        </center>
      </li>
    }

    <li>
    <div onClick={()=>{
        window.location.href = "/VideosWork"
      }}>
        Work from Home
    </div>
    </li>


    <li>
    <div onClick={()=>{
        window.location.href = "/VideosCyber"
      }}>
        Cyber Security
    </div>
    </li>


    <li>
    <div onClick={()=>{
        window.location.href = "/VideosPowerstore"
      }}>
        Powerstore
    </div>
    </li>

    {
      isMobile&&
      <li>
        <center>
          <img width="60%" src="/images/pleca.png"/>
        </center>
      </li>
    }

    {
      !isMobile&&
      <div className="itemMenu">
        |
      </div>
    }



    <li>
    <div onClick={()=>{
        window.location.href = "/Practice"
      }}>
        Practicar 
    </div>
    {/* <div>
        Practicar 
    </div> */}
    </li>

          <li>
          <div onClick={()=>{
              window.location.href = "/Play"
            }}>
              Jugar
          </div>
          </li>
          

          {
            isMobile&&
            <li>
              <center>
                <img width="60%" src="/images/pleca.png"/>
              </center>
            </li>
          }

          <li>
          <div onClick={()=>{
              window.location.href = "/Scheduler"
            }}>
              Agendar una cita
          </div>
          </li>


          {
            isMobile&&
            <li>
              <center>
                <img width="60%" src="/images/pleca.png"/>
              </center>
            </li>
          }


          <li>
          <div onClick={()=>{
              window.location.href = "/Award"
            }}>
              Premios
          </div>
          </li>


          {
            isMobile&&
            <li>
              <center>
                <img width="60%" src="/images/pleca.png"/>
              </center>
            </li>
          }

          <li>
            <div onClick={()=>{
                localStorage.removeItem("jwtTokenApp");
                setAuthToken(false);
                window.location.href = "/"
              }}>
              Salir
            </div>
          </li>
    </Ul>
  )
}




export default RightNav
