/**
 * @Author: memo
 * @Date:   2020-10-04T23:01:20-05:00
 * @Last modified by:   memo
 * @Last modified time: 2020-10-30T13:14:20-06:00
 */
import React from 'react';
import styled from 'styled-components';
import Burger from './Burger';
import MyAvatar from './NavAvatar';
import {isMobile} from 'react-device-detect';

const Top = styled.nav`
  position:absolute;
  right:0;
  top:0px;
  z-index:1000;
  width: 100%;
  height: 4rem;
  border-bottom: 2px solid #f1f1f1;
  padding: 0 20px;
  display: flex;
  justify-content: space-between;
  /* background-color: #00447C; */
  /* background: '/images/menu.png'; */
  background: linear-gradient(
    to right,
    #41B6E6,
    #00447C
  );

  color:white;
  /* position: fixed; */
  .logo {
    display: flex;
    left:2rem;
    /* justify-content: space-between; */
    /* background-color:red; */
    width:45%
  };
`

const Topbar = (exit, avatar) => {
  var sizeAvatar = isMobile ? "50%":"60%"
  var sizeLogo = isMobile ? "50%":"50%"
  console.log("MyAvatar2:", avatar)
  // var image = "https://avataaars.io/?avatarStyle=Circle&topType="+this.props.user.avatar.topType+
  //       +"LongHairBun&accessoriesType=Blank&hairColor=Red&facialHairType=Blank&clotheType=BlazerShirt&eyeType=Default&eyebrowType=Default&mouthType=Default&skinColor=Light"
  var image ="https://avataaars.io/?avatarStyle=Circle"
  return (
    <Top>
      <div className="logo">
        <div className="avatarBox" >
          {
            // <img
            //   onClick={()=>{
            //     window.location.href = "/Avatar"
            //   }}
            //   height={sizeAvatar}
            //   // width={sizeAvatar}
            //   src={image}
            // />
          }
          {
            <div className="avatarNavBar">
            <MyAvatar

            />
            </div>
          }
          {
            !isMobile &&
            <div className="profileBox">
            <label>Mi perfil</label>
            </div>
          }

          {
           !isMobile &&
             <div className="leftBox" onClick={()=>document.location.href="/"}>
               <img
                   width="60%"
                   src="images/logoMenu.png"
                 />
             </div>
           }

           {
            isMobile &&
              <div className="leftBox" onClick={()=>document.location.href="/"}>
                <img
                    width="100%"
                    src="images/logoMenu.png"
                  />
              </div>
            }
        </div>

      </div>

      <Burger exit={exit}/>
    </Top>
  )
}

export default Topbar
