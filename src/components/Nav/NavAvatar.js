/**
 * @Author: memo
 * @Date:   2020-10-30T01:44:25-06:00
 * @Last modified by:   memo
 * @Last modified time: 2020-10-30T13:13:28-06:00
 */

 import React, { Component } from "react";
 import { connect } from "react-redux";
 import {Piece} from 'avataaars';
 import 'react-widgets/dist/css/react-widgets.css'
 import {isMobile} from 'react-device-detect';
 import Avatar from 'avataaars'
 import { getAvatar } from "../../actions/authActions";


 class NavAvatar extends Component {
   constructor(props) {
     super(props);
      this.state = {
        isVisibleModal:false,
        topType:"None",
        hairColor:'BrownDark',
        facialHairType:'Blank',
        clotheColor:'PastelBlue',
        eyeType:'Happy',
        eyebrowType:'Default',
        mouthType:'Smile',
        skinColor:'Light'
      };
   }

   UNSAFE_componentWillMount(){
     this.props.getAvatar(this.props.auth.user.id)
   }
   componentDidUpdate(prevProps) {

     console.log("USER_AVATAR NavAvatar this.props:", this.props.auth)
     if(
       this.props.auth.avatar
           && prevProps.auth.avatar
           && this.props.auth.avatar != prevProps.auth.avatar
     ){
       alert("a")

       this.setState({
          topType:this.props.auth.avatar.topType,
          hairColor:this.props.auth.avatar.hairColor,
          facialHairType:this.props.auth.avatar.facialHairType,
          clotheColor:this.props.auth.avatar.clotheType,
          eyeType:this.props.auth.avatar.eyeType,
          eyebrowType:this.props.auth.avatar.eyebrowType,
          mouthType:this.props.auth.avatar.mouthType,
          skinColor:this.props.auth.avatar.skinColor
       })
     }
   }


   componentDidUpdate(prevProps) {
     console.log("NavAvatar this.props:", this.props.auth)
     if(
       this.props.auth.avatar
           && prevProps.auth.avatar
           && this.props.auth.avatar != prevProps.auth.avatar
     ){
       console.log("NavAvatar:entró")
       this.setState({
          topType:this.props.auth.avatar.topType,
          hairColor:this.props.auth.avatar.hairColor,
          facialHairType:this.props.auth.avatar.facialHairType,
          clotheColor:this.props.auth.avatar.clotheType,
          eyeType:this.props.auth.avatar.eyeType,
          eyebrowType:this.props.auth.avatar.eyebrowType,
          mouthType:this.props.auth.avatar.mouthType,
          skinColor:this.props.auth.avatar.skinColor
       })
     }
   }


   render(){
     return (<div className="avatarItem">
       <Avatar
         avatarStyle='Circle'
         topType={this.props.auth.avatar.topType?this.props.auth.avatar.topType:this.state.topType}
         accessoriesType={this.props.auth.avatar.accessoriesType?this.props.auth.avatar.accessoriesType:this.state.accessoriesType}
         hairColor={this.props.auth.avatar.hairColor?this.props.auth.avatar.hairColor:this.state.hairColor}
         facialHairType={this.props.auth.avatar.facialHairType?this.props.auth.avatar.facialHairType:this.state.facialHairType}
         clotheType={this.props.auth.avatar.clotheType?this.props.auth.avatar.clotheType:this.state.clotheType}
         clotheColor={this.props.auth.avatar.clotheColor?this.props.auth.avatar.clotheColor:this.state.clotheColor}
         eyeType={this.state.eyeType}
         eyebrowType={this.state.eyebrowType}
         mouthType={this.state.mouthType}
         skinColor={this.state.skinColor}
         />
     </div>)
   }
 }

 const mapStateToProps = state => ({
   utils : state.utils,
   auth  : state.auth,
   avatar: state.avatar
 });
 export default connect(
   mapStateToProps,
   {
     getAvatar
   }
 )(NavAvatar);
